package ua.org.oa.s.alex.MovieHall.web;


import ua.org.oa.s.alex.MovieHall.dao.api.Dao;
import ua.org.oa.s.alex.MovieHall.dao.impl.CrudDao;
import ua.org.oa.s.alex.MovieHall.dao.impl.HallDaoImpl;
import ua.org.oa.s.alex.MovieHall.dao.impl.MovieDaoImpl;
import ua.org.oa.s.alex.MovieHall.dao.impl.MovieHouseDaoImpl;
import ua.org.oa.s.alex.MovieHall.datasource.DataSource;
import ua.org.oa.s.alex.MovieHall.dto.MovieDTO;
import ua.org.oa.s.alex.MovieHall.dto.MovieHouseDTO;
import ua.org.oa.s.alex.MovieHall.model.Hall;
import ua.org.oa.s.alex.MovieHall.model.Movie;
import ua.org.oa.s.alex.MovieHall.model.MovieHouse;
import ua.org.oa.s.alex.MovieHall.model.User;
import ua.org.oa.s.alex.MovieHall.service.impl.MovieHouseServiceImpl;
import ua.org.oa.s.alex.MovieHall.service.impl.MovieServiceImpl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;


/**
 * Created by A Sosnovskyi on 14.03.2017.
 */
public class App {
    public static void main(String[] args) {

//        String url = "jdbc:mysql://localhost:3306/MovieHall_test";
//        // String url="jdbc:mysql://localhost:3306/MovieHall_test?useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
//        String user = "admin";
//        String password = "123";
////        String sql = "Select * from hall";
//        String sql = "Select * from movie";
//        DataSource dataSource = DataSource.getInstance();
//
//        try (Connection connection = dataSource.getConnection();
//             PreparedStatement preparedStatement = connection.prepareStatement(sql);
//             ResultSet resultSet = preparedStatement.executeQuery()) {
//            resultSet.last();
//            if (resultSet.getRow() == 0) {
//                System.out.println("Table is empty");
//            } else {
//                resultSet.beforeFirst();
//            }
//            while (resultSet.next()) {
////                System.out.println(resultSet.getInt("hall_id"));
////                System.out.println(resultSet.getString("hall_name"));
//                System.out.println(resultSet.getInt("movie_id"));
//                System.out.println(resultSet.getString("movie_title"));
////                System.out.println(resultSet.getString("movie_description"));
////                System.out.println(resultSet.getInt("movie_rating"));
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        //==========================================================================================
//        Dao movieDao = MovieDaoImpl.getInstance();
//        movieDao.getAll();
        //==========================================================================================


        //   String name, String genre, String producedBy, int duration, String description, int rating, String country, String icon, String actors
        // String name, String genre, String producedBy, int duration, String description, int rating, String country, String icon, String actors

//        Movie movie=new Movie("Аватар", "научно-фантастический фильм", "Джеймс Кэмерон", 162,
//                    "Действие фильма происходит в 2154 году, когда человечество добывает ценный минерал анобтаниум на Пандоре,"+
//                            " обитаемом спутнике газовой планеты в звёздной системе Альфы Центавра. "+
//                            "По сюжету ресурсодобывающая корпорация угрожает существованию местного племени человекоподобных"+
//                            " разумных существ — на’ви. Название фильма — название генетически спроектированных тел, "+
//                            "гибридов на’ви и людей, используемых командой исследователей для изучения планеты и взаимодействия"+
//                            " с туземными жителями Пандоры", 2124, "USA", "icons/icon2.jpg",
//                    "Сэм Уортингтон\n" +
//                            "Зои Салдана\n" +
//                            "Сигурни Уивер\n" +
//                            "Стивен Лэнг\n" +
//                            "Мишель Родригес\n" +
//                            "Джованни Рибизи\n" +
//                            "Джоэль Мур");
//            MovieDTO movieDTO=new MovieDTO(movie.getName(), movie.getGenre(), movie.getProducedBy(), movie.getDuration(), movie.getDescription(),
//                    movie.getRating(), movie.getCountry(), movie.getIcon(), movie.getActors());
//
//          MovieServiceImpl movieService=MovieServiceImpl.getInstance();
//        movieService.save(movieDTO);
//=======================================================================================================================================

//        Calendar currentDate = new GregorianCalendar();
//        System.out.println(currentDate.getTimeInMillis());
//
//        Date date = new Date();
//        System.out.println(date.getTime());
        //===============================Тест MovieHouseDao работает===================================================================

//        MovieHouseDaoImpl movieHouseDao = MovieHouseDaoImpl.getInstance();
//        List<MovieHouse> movieHouseList = movieHouseDao.getAll();
//        for (MovieHouse movieHouse : movieHouseList) {
//            HallDaoImpl hallDao = HallDaoImpl.getInstance();
//            List<Hall> halls = hallDao.readAllByMovieHouseId(movieHouse.getId());
//            Hall[] hallsArray = halls.toArray(new Hall[halls.size()]);
//            movieHouse.setHalls(hallsArray);
//
//        }
//        for (MovieHouse movieHouse : movieHouseList) {
//            System.out.println(movieHouse);
//        }
        //=================================================================================================
        MovieHouseServiceImpl movieHouseService = MovieHouseServiceImpl.getInstance();
        List<MovieHouseDTO> movieHouseList = movieHouseService.getAll();

//        for (MovieHouseDTO movieHouseDTO : movieHouseList) {
//            System.out.println(movieHouseDTO);
//        }
    }
}
