package ua.org.oa.s.alex.task4_2.part2;

import java.util.Iterator;
import java.util.Objects;

/**
 * Created by A Sosnovskyi on 17.02.2017.
 */
public class MyDequeImpl<E> implements MyDeque<E> {
    public int size;
    private Node<E> head = null;
    private Node<E> tail = null;

    public MyDequeImpl() {
        this.size = 0;
        this.head = null;
        this.tail = null;
    }

    /**
     * addFirst
     *
     * @param e E element
     */
    @Override
    public void addFirst(E e) {
        if (this.head == null && this.tail == null) {
            this.head = new Node<>(e, null, null);
            this.tail = this.head; //первый помещенный элемент есть и начало и конец

        } else {
            Node<E> old = this.head;
            Node<E> newNode = new Node<>(e, old, null);
            this.head = newNode; //elem, next, prev
            old.prev = newNode;
//----------------------------------------------------------------------
//            System.out.println("old.element="+old.element);
//            if(old.prev!=null){
//                System.out.println(" old.prev="+old.prev.element);
//            }else{System.out.println(" old.prev=null");}
//            if (old.next!=null){
//                System.out.println(" old.next="+old.next.element);
//            }else{
//                System.out.println(" old.next=null");
//            }
//            System.out.println();
//            System.out.println("new.element="+newNode.element);
//            if (newNode.next!=null){
//                System.out.println(" new.next="+newNode.next.element);
//            }else{
//                System.out.println(" new.next=null");
//            }
//            if (newNode.prev!=null){
//                System.out.println(" new.prev="+newNode.prev.element);
//            }else{
//                System.out.println(" new.prev=null");
//            }
//            System.out.println("================================");
//--------------------------------------------------------------------------
        }
        this.size += 1;
    }

    /**
     * addLast
     *
     * @param e E element
     */
    @Override
    public void addLast(E e) {
        if (this.tail == null && this.head == null) {
            this.tail = new Node<>(e, null, null);
            this.head = this.tail;

        } else {
            Node<E> old = this.tail;
            this.tail = new Node<>(e, null, old); //elem, next, prev
            old.next = this.tail;

        }
        this.size += 1;
    }

    /**
     * removeFirst
     *
     * @return E removed element
     */
    @Override
    public E removeFirst() {
        Node<E> tmpNode = this.head;
        this.head = this.head.next; //Убрать ссылку на первый элемент
        System.out.println("this.head.next" + this.head.next.element);
        return tmpNode.element;
    }

    /**
     * removeLast
     *
     * @return E removed element
     */
    @Override
    public E removeLast() {
        Node<E> tmpNode = this.tail;
        this.tail = this.tail.prev; //Убрать ссылку на последний элемент
        return tmpNode.element;
    }


    /**
     * getFirst
     *
     * @return E element
     */
    @Override
    public E getFirst() {
        if (this.head == null) {
            throw new NullPointerException("В очереди нет элементов");
        }
        return this.head.element;
    }

    /**
     * getLast
     *
     * @return E element
     */
    @Override
    public E getLast() {
        if (this.tail == null) {
            throw new NullPointerException("В очереди нет элементов");
        }
        return this.tail.element;
    }


    /**
     * contains
     *
     * @param o Object
     * @return boolean
     */
    @Override
    public boolean contains(Object o) {
        if (o == null) {
            throw new IllegalArgumentException(o + "=null");
        }

        Node node = this.head;
        int i = 0;
        while (i++ < size) {
            if (node.element.equals(o)) {
                return true;
            }
            node = node.next;
        }
        return false;
    }

    /**
     * clear
     */
    @Override
    public void clear() {
        this.head = null;
        this.tail = null;
        this.size = 0;
    }


    /**
     * toArray
     *
     * @return Object[] of elements
     */
    @Override
    public Object[] toArray() {
        Node node = this.head;
        Object[] result = new Object[size];
        int iter = 0;
        int i=0;
        while (i++<size) {
            result[iter++] = node.element;
            node = node.next;

        }
        return result;
    }


    /**
     * size
     *
     * @return int size
     */
    @Override
    public int size() {

        return  this.size;
    }


    /**
     * @param deque
     * @return boolean
     */
    @Override
    public boolean containsAll(MyDeque<? extends E> deque) {
        if (deque.size() == 0) {
            throw new IllegalArgumentException("не может быть равной " + deque.size());
        }
        Object[] dequeArray = deque.toArray();
        for (int index = 0; index < dequeArray.length; index++) {
            if (!this.contains(dequeArray[index])) {
                return false;
            }
        }
        return true;
    }

    /**
     * @return
     */
    @Override
    public Iterator<E> iterator() {
        return new IteratorImpl();
    }

    /**
     * toString
     *
     * @return String
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        Node node = this.head;

        while (node.next != null) {
            sb.append(node.toString()).append(", ");
            node = node.next;
        }


        return sb.toString();
    }

    /**
     * @return
     */
    public Node<E> getHead() {
        return head;
    }

    /**
     * @return
     */
    public Node<E> getTail() {
        return tail;
    }


    private static class Node<E> {
        E element;
        Node<E> next;
        Node<E> prev;

        public Node(E element, Node<E> next, Node<E> prev) {
            this.element = element;
            this.next = next;
            this.prev = prev;
        }


        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Node<?> node = (Node<?>) o;
            return Objects.equals(element, node.element);
        }

        @Override
        public int hashCode() {
            return Objects.hash(element);
        }


        @Override
        public String toString() {
            if (element == null) {
                return "null";
            }
            return "Node{" +
                    "element=" + element +
                    '}';
        }
    }

    private class IteratorImpl implements Iterator<E> {
        Node<E> currentNode;
        int current;
        int flag;

        public IteratorImpl() {
            this.currentNode = getHead();
            this.current = 0;
            this.flag = 0;
        }

        @Override
        public boolean hasNext() {
            if (current < size) {
                return true;
            }
            return false;
        }

        @Override
        public E next() {
            Node<E> result = currentNode;
            currentNode = currentNode.next;
            current++;
            flag = 1;
            return result.element;
        }

        @Override
        public void remove() {
            if (flag == 0) { // не вызывался метод next()
                throw new IllegalStateException("Метод remove() так не используется");
            }
            Node prevNode = currentNode.prev;
            Node nextNode = currentNode.next;
            prevNode.next = nextNode;
            nextNode.prev = prevNode;
            size -= 1; //уменьшить размер коллекции на 1
            this.flag = 0;
        }


    }

}
