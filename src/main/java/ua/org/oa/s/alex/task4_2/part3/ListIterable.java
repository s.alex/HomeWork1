package ua.org.oa.s.alex.task4_2.part3;

/**
 * Created by A Sosnovskyi on 17.02.2017.
 */
public interface ListIterable<E> {
    ListIterator<E> listIterator();
}
