package ua.org.oa.s.alex.MovieHall.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * Created by Admin on 17.03.2017.
 */
@Getter
@Setter
public class Ticket extends Entity<Integer> {
    private  int movieHouseName; //
    private  Date timeOfSell;  //
    private  int userId;   //
    private  int hall;   //
    private  int seanceId;  //
    private  int seatRow; //
    private   int seatPosition;
    private  int price;

    public Ticket(int movieHouseName,  Date timeOfSell,
                  int userId, int hall, int seanceId, int seatRow, int seatPosition, int price) {
        this.movieHouseName = movieHouseName;
        this.timeOfSell = timeOfSell;
        this.userId = userId;
        this.hall = hall;
        this.seanceId = seanceId;
        this.seatRow = seatRow;
        this.seatPosition = seatPosition;
        this.price = price;
    }

    public Ticket() {
        this.movieHouseName = 0;
        this.timeOfSell = null;
        this.userId = 0;
        this.hall = 0;
        this.seanceId = 0;
        this.seatRow = 0;
        this.seatPosition = 0;
        this.price = 0;
    }
}
