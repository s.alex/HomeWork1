package ua.org.oa.s.alex.MovieHall.dto;

import lombok.Getter;
import lombok.Setter;
import ua.org.oa.s.alex.MovieHall.model.Entity;
import ua.org.oa.s.alex.MovieHall.model.Hall;

import java.util.Arrays;

/**
 * Created by Admin on 21.03.2017.
 */
@Getter
@Setter
public class MovieHouseDTO extends Entity<Integer> {
    private String name;
    private int hallsQuantity;
    private Hall[] movieHalls;

    public MovieHouseDTO() {

    }
    public MovieHouseDTO(String name, int hallsQuantity) {
        this.name = name;
        this.hallsQuantity = hallsQuantity;
        this.movieHalls = new Hall[hallsQuantity];
    }

    @Override
    public String toString() {
        return "MovieHouseDTO{" +
                "name='" + name + '\'' +
                ", halls=" + Arrays.toString(movieHalls) +
                ", hallsQuantity=" + hallsQuantity +
                '}';
    }
}
