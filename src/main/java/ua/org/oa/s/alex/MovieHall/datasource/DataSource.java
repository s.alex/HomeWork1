package ua.org.oa.s.alex.MovieHall.datasource;


import com.mchange.v2.c3p0.ComboPooledDataSource;
import ua.org.oa.s.alex.MovieHall.helper.PropertyHolder;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by Admin on 14.03.2017.
 */
public final class DataSource {
    private static DataSource dataSource;
    private static ComboPooledDataSource poolConnections;

    private DataSource(){
        initPoolConnections();
    }

    public static synchronized DataSource getInstance(){
        if(dataSource==null){
            dataSource=new DataSource();
        }
        return dataSource;
    }

    public Connection getConnection(){
        Connection connection=null;
        try {
            connection=poolConnections.getConnection();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return connection;
    }

    private static void initPoolConnections(){
        poolConnections=new ComboPooledDataSource();
        PropertyHolder propertyHolder=PropertyHolder.getInstance();

        try {
            poolConnections.setDriverClass(propertyHolder.getDbDriver());
            poolConnections.setJdbcUrl(propertyHolder.getDbUrl());
            poolConnections.setUser(propertyHolder.getDbUserLogin());
            poolConnections.setPassword(propertyHolder.getDbUserPassword());
            poolConnections.setMinPoolSize(5);
            poolConnections.setAcquireIncrement(1);
            poolConnections.setMaxPoolSize(100);

        } catch (PropertyVetoException e) {
            e.printStackTrace();
        }

    }
}
