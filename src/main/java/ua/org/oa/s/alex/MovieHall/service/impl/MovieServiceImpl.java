package ua.org.oa.s.alex.MovieHall.service.impl;

import ua.org.oa.s.alex.MovieHall.dao.DaoFactory;
import ua.org.oa.s.alex.MovieHall.dao.api.Dao;
import ua.org.oa.s.alex.MovieHall.dao.impl.MovieDaoImpl;
import ua.org.oa.s.alex.MovieHall.dto.MovieDTO;
import ua.org.oa.s.alex.MovieHall.mapper.BeanMapper;
import ua.org.oa.s.alex.MovieHall.model.Movie;
import ua.org.oa.s.alex.MovieHall.service.api.Service;

import java.util.List;

/**
 * Created by Admin on 14.03.2017.
 */
public class MovieServiceImpl implements Service<Integer, MovieDTO> {
    private static MovieServiceImpl service;
    private Dao<Integer, Movie> movieDao;
    private BeanMapper beanMapper;

    private MovieServiceImpl() {
        movieDao= DaoFactory.getInstance().getMovieDao();
        beanMapper=BeanMapper.getInstance();
    }
    public static synchronized MovieServiceImpl getInstance(){
        if(service==null){
            service=new MovieServiceImpl();
        }

        return service;
    }

    @Override
    public List<MovieDTO> getAll() {
        List<Movie> movies = movieDao.getAll();
        List<MovieDTO> movieDTOs = beanMapper.listMapToList(movies, MovieDTO.class);
        return movieDTOs;
    }

    @Override
    public MovieDTO getById(Integer key) {
       Movie movie= movieDao.getById(key);
        MovieDTO movieDTO=BeanMapper.singleMapper(movie, MovieDTO.class);
        return movieDTO;
    }

    @Override
    public void save(MovieDTO movieDto) {
        Movie movie = beanMapper.singleMapper(movieDto, Movie.class);
        movieDao.save(movie);
    }

    @Override
    public void delete(Integer key) {
        movieDao.delete(key);
    }

    @Override
    public void update(MovieDTO entity) {
        Movie movie = beanMapper.singleMapper(entity, Movie.class);
        movieDao.update(movie);
    }
}
