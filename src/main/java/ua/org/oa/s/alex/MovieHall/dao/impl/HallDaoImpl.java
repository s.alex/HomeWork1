package ua.org.oa.s.alex.MovieHall.dao.impl;

import ua.org.oa.s.alex.MovieHall.datasource.DataSource;
import ua.org.oa.s.alex.MovieHall.model.Hall;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 19.03.2017.
 */
public class HallDaoImpl extends CrudDao<Hall> {
    private final String INSERT = "Insert into hall (hall_name, hall_rows, hall_row_seats" +
            "hall_seats_total, movie_house) values(?, ?, ?, ?, ?)";
    private final String UPDATE = "Update hall Set hall_name=?, hall_rows=?, hall_row_seats=?," +
            "hall_seats_total=?  where hall_id=?";
    private final String READ_ALL_BY_MOVIE_HOUSE_ID = "Select * from hall where movie_house=?";
    private static HallDaoImpl crudDao;

    private HallDaoImpl(Class type) {
        super(type);
    }



    public static HallDaoImpl getInstance() {
        if (crudDao == null) {
            crudDao = new HallDaoImpl(Hall.class);
        }
        return crudDao;
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Hall entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
        preparedStatement.setString(1, entity.getName());
        preparedStatement.setInt(2, entity.getRows());
        preparedStatement.setInt(3, entity.getRowSeats());
        preparedStatement.setInt(4, entity.getSeatsQuantity());
        preparedStatement.setInt(5, entity.getId());
        return preparedStatement;
    }

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Hall entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1, entity.getName());
        preparedStatement.setInt(2, entity.getRows());
        preparedStatement.setInt(3, entity.getRowSeats());
        preparedStatement.setInt(4, entity.getSeatsQuantity());
        preparedStatement.setInt(5, entity.getMovieHouseId());
        return preparedStatement;
    }

    @Override
    protected List<Hall> readAll(ResultSet resultSet) throws SQLException {
        List<Hall> listHalls = new ArrayList<>();
        Hall hallTmp = null;
        if (!resultSet.next()) {
            System.out.println("Table of Halls is empty");

        } else {
            resultSet.beforeFirst();
        }
        //hall_name, hall_rows, hall_row_seats, hall_seats_total, movie_house

        while (resultSet.next()) {
            hallTmp = new Hall(resultSet.getString("hall_name"), resultSet.getInt("hall_rows"),
                    resultSet.getInt("hall_row_seats"), resultSet.getInt("hall_seats_total"),
                    resultSet.getInt("movie_house"));
            hallTmp.setId(resultSet.getInt("hall_id"));
            listHalls.add(hallTmp);
        }
        return listHalls;
    }

    public List<Hall> readAllByMovieHouseId(int key) {

        List<Hall> result = null;
        try (Connection connection = DataSource.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(READ_ALL_BY_MOVIE_HOUSE_ID)
             ) {
            preparedStatement.setInt(1, key);
            ResultSet resultSet = preparedStatement.executeQuery();
            result = readAll(resultSet);
        } catch (SQLException e) {
        }

        return result;
    }
}
