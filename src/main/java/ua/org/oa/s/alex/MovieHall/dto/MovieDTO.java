package ua.org.oa.s.alex.MovieHall.dto;

import lombok.Getter;
import lombok.Setter;
import ua.org.oa.s.alex.MovieHall.model.Entity;

/**
 * Created by Admin on 14.03.2017.
 */
@Setter
@Getter
public class MovieDTO extends Entity<Integer> {
    private String name;
    private String genre; //жанр
    private String producedBy; //производство
    private int duration; //длительность
    private String description; //
    private int rating;
    private String country;
    private String icon;
    private String actors;


    public MovieDTO(){

    }

    public MovieDTO(String name, String genre, String producedBy, int duration, String description, int rating, String country, String icon, String actors) {
        this.name = name;
        this.genre = genre;
        this.producedBy = producedBy;
        this.duration = duration;
        this.description = description;
        this.rating = rating;
        this.country = country;
        this.icon = icon;
        this.actors = actors;
    }


    @Override
    public String toString() {
        return "MovieDTO{" +
                "title='" + name + '\'' +
                ", description='" + description + '\'' +
                ", duration=" + duration +
                "} ";
    }
}
