package ua.org.oa.s.alex.MovieHall.dto;

import lombok.Getter;
import lombok.Setter;
import ua.org.oa.s.alex.MovieHall.model.Entity;
import ua.org.oa.s.alex.MovieHall.model.Seat;

import java.util.Date;
import java.util.List;

/**
 * Created by Admin on 21.03.2017.
 */
@Getter
@Setter
public class SeanceDTO extends Entity<Integer> {
    private Date dayBegin;
    private Date dayEnd;
    private Date currentSeanceTime;
    //  private List<Date> timesOfShowTable;
    private List<Seat> seats;
    private int movieId;
    private int hallId;
    private int movieHouseId;
    private int price;

    public SeanceDTO(Date dayBegin, Date dayEnd , Date currentSeanceTime,  int movieId, int hallId, int movieHouseId, int price) {
        this.dayBegin = dayBegin;
        this.dayEnd = dayEnd;
        this.currentSeanceTime=currentSeanceTime;
        this.movieId = movieId;
        this.hallId = hallId;
        this.movieHouseId=movieHouseId;
        this.price = price;
    }

    public SeanceDTO() {
    }
}
