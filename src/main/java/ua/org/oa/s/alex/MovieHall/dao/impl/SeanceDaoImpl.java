package ua.org.oa.s.alex.MovieHall.dao.impl;

import ua.org.oa.s.alex.MovieHall.datasource.DataSource;
import ua.org.oa.s.alex.MovieHall.model.Seance;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 20.03.2017.
 */
public class SeanceDaoImpl extends CrudDao<Seance> {
    private final String INSERT = "Insert into seance (seance_day_begin, seance_day_end, seance_time, seance_movie," +
            " seance_hall, seance_sinema, seance_price) values(?, ?, ?, ?, ?, ?, ?)";
    private final String UPDATE = "Update seance Set seance_day_begin=?, seance_day_end=?, seance_time=?, seance_movie=?," +
            " seance_hall=?, seance_sinema=?, seance_price=? where seance_id=?";
   //SELECT * from seance WHERE seance_hall='1' and seance_date_begin<='2017-04-27' and seance_date_end>'2017-04-27'
    private final  String READ_ALL_BY_MOVIE_HALL_ID="SELECT * from seance WHERE seance_hall=? and seance_date_begin<=? and seance_date_end>?";
    private static SeanceDaoImpl seanceDao;

    private SeanceDaoImpl(Class type) {
        super(type);
    }

    public static SeanceDaoImpl getInstance() {
        if (seanceDao == null) {
            seanceDao = new SeanceDaoImpl(Seance.class);
        }
        return seanceDao;
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Seance entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
        return preparedStatement;
    }

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Seance entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setDate(1, (Date) entity.getDayBegin());
        preparedStatement.setDate(2, (Date) entity.getDayEnd());
        preparedStatement.setInt(3, entity.getCurrentSeanceTime());
        preparedStatement.setInt(4, entity.getMovieId());
        preparedStatement.setInt(5, entity.getHallId());
        preparedStatement.setInt(6, entity.getMovieHouseId());
        preparedStatement.setInt(7, entity.getPrice());
        return preparedStatement;
    }

    @Override
    protected List<Seance> readAll(ResultSet resultSet) throws SQLException {

        List<Seance> seanceList=new ArrayList<>();
        Seance seanceTmp=null;
        if(!resultSet.next()){
            System.out.println("В таблице Seance нет записей");
        }else{

            resultSet.beforeFirst();
        }
        //Date dayBegin, Date dayEnd , seance_time,  int movieId, int hallId, int movieHouseId, int price
        while (resultSet.next()){


            seanceTmp=new Seance(resultSet.getDate("seance_date_begin"), resultSet.getDate("seance_date_end"),
                    resultSet.getInt("seance_time"), resultSet.getInt("seance_movie"), resultSet.getInt("seance_hall"),
                    resultSet.getInt("seance_sinema"), resultSet.getInt("seance_price"));
            seanceTmp.setId(resultSet.getInt("seance_id"));
            seanceList.add(seanceTmp);

        }

        return seanceList;
    }
    public List<Seance> readAllByHallId(int key){
       Date date= new Date(System.currentTimeMillis());

        List<Seance> seanceList=null;
        try (Connection connection = DataSource.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(READ_ALL_BY_MOVIE_HALL_ID);
        ) {
            preparedStatement.setInt(1, key);
            preparedStatement.setDate(2, date);
            preparedStatement.setDate(3, date);
            ResultSet resultSet = preparedStatement.executeQuery();

            seanceList = readAll(resultSet);
        } catch (SQLException e) {
        }
//for (Seance seance: seanceList ){
//    System.out.println(seance);
//}
        return seanceList;
    }

}
