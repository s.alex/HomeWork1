package ua.org.oa.s.alex.MovieHall.model;

import lombok.Getter;
import lombok.Setter;


import java.util.Arrays;
import java.util.List;


/**
 * Created by Admin on 16.03.2017.
 */
@Getter
@Setter
public class Hall extends Entity<Integer>{
    private String name;
    private int seatsQuantity;
    private int rows;
    private int rowSeats;
    private int seance;
    private int movieHouseId;
    private Seat[] seats;
    private List<Seance> seances;


    public Hall(String name,  int rows, int rowSeats, int seatsQuantity, int movieHouseId) {
        this.name = name;
        this.seatsQuantity = seatsQuantity;
        this.rows = rows;
        this.rowSeats = rowSeats;
        this.movieHouseId = movieHouseId;
        this.seats=new Seat[ seatsQuantity];

    }

    public Hall() {
    }


    @Override
    public String toString() {
        return "Hall{" +
                "name='" + name + '\'' +
                ", seatsQuantity=" + seatsQuantity +
                ", rows=" + rows +
                ", rowSeats=" + rowSeats +
                ", seance=" + seance +
                ", movieHouseId=" + movieHouseId +
                ", seats=" + Arrays.toString(seats) +
                ", seances=" + seances +
                '}';
    }
}
