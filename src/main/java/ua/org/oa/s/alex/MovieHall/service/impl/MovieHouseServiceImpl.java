package ua.org.oa.s.alex.MovieHall.service.impl;

import ua.org.oa.s.alex.MovieHall.dao.DaoFactory;
import ua.org.oa.s.alex.MovieHall.dao.api.Dao;
import ua.org.oa.s.alex.MovieHall.dao.impl.HallDaoImpl;

import ua.org.oa.s.alex.MovieHall.dto.MovieHouseDTO;
import ua.org.oa.s.alex.MovieHall.mapper.BeanMapper;

import ua.org.oa.s.alex.MovieHall.model.Hall;
import ua.org.oa.s.alex.MovieHall.model.MovieHouse;
import ua.org.oa.s.alex.MovieHall.service.api.Service;


import java.util.List;

/**
 * Created by Admin on 21.03.2017.
 */
public class MovieHouseServiceImpl implements Service<Integer, MovieHouseDTO> {
    private static MovieHouseServiceImpl service;
    private Dao<Integer, MovieHouse> movieHouceDao;
    private BeanMapper beanMapper;

    private MovieHouseServiceImpl() {
        movieHouceDao = DaoFactory.getInstance().getMovieHouseDao();
        beanMapper = BeanMapper.getInstance();
    }

    public static synchronized MovieHouseServiceImpl getInstance() {
        if (service == null) {
            service = new MovieHouseServiceImpl();
        }

        return service;
    }

    @Override
    public List<MovieHouseDTO> getAll() {
        List <MovieHouse> movieHouseList=movieHouceDao.getAll();
       // -------------------------------------test good-----------------------------------------------------------------------
        /*for(MovieHouse movieHouse: movieHouseList){
            System.out.println(movieHouse);
        }*/
        //-----------------------------------------------------------------------------------------------------------
        for (MovieHouse movieHouse: movieHouseList){
           List<Hall> hallList= HallServiceImpl.getInstance().getAllByMovieHouseId(movieHouse.getId());


            int i=0;
            Hall[] halls=new Hall[movieHouse.getHallsQuantity()];

            //++++++++++++++++++++++++++++работает если закомментировать+++++++++++++++++++++++++++++++++++++++++++++++++++++
            for(Hall hall:hallList){

                halls[i]=hall;
                i++;
            }
            //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            movieHouse.setMovieHalls(halls);

        }

        List<MovieHouseDTO> movieHouseDTOList=null;// = beanMapper.listMapToList(movieHouseList, MovieHouseDTO.class);
       //----------------------------test-------------------------------------------
        for (MovieHouse movieHouse: movieHouseList){
            System.out.println(movieHouse);
        }
        //---------------------------------------------------------------------------------------
        return movieHouseDTOList;
    }

    @Override
    public MovieHouseDTO getById(Integer key) {
        MovieHouse movieHouse=movieHouceDao.getById(key);
        MovieHouseDTO movieHouseDTO=BeanMapper.singleMapper(movieHouse, MovieHouseDTO.class);
        return movieHouseDTO;
    }

    @Override
    public void save(MovieHouseDTO entity) {
        MovieHouse movieHouse=BeanMapper.singleMapper(entity, MovieHouse.class);
        movieHouceDao.save(movieHouse);
    }

    @Override
    public void delete(Integer key) {
        movieHouceDao.delete(key);

    }

    @Override
    public void update(MovieHouseDTO entity) {
        MovieHouse movieHouse=BeanMapper.singleMapper(entity, MovieHouse.class);
        movieHouceDao.update(movieHouse);
    }
}
