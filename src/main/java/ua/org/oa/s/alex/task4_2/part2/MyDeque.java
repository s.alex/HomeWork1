package ua.org.oa.s.alex.task4_2.part2;



/**
 * Created by A Sosnovskyi on 16.02.2017.
 */
 interface MyDeque<E> extends Iterable<E>{

    /**
     * add element to head
     *
     * @param e
     */
    void addFirst(E e);

    /**
     * add last element
     *
     * @param e
     */
    void addLast(E e);

    /**
     * First element it was deleting
     * @return
     */
    E removeFirst();
    /**
     * Last element it was deleting
     * @return
     */
    E removeLast();
    /**
     * First element
     * @return
     */
    E getFirst();
    /**
     * First element
     * @return
     */
    E getLast();

    /**
     *
     * @param o
     * @return
     */
    boolean contains(Object o);

    /**
     *
     */
    void clear();

    /**
     *
     * @return
     */
    Object[] toArray();

    /**
     *
     * @return
     */
    int size();
    boolean containsAll(MyDeque<? extends E> deque);


}
