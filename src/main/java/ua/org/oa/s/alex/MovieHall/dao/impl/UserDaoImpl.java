package ua.org.oa.s.alex.MovieHall.dao.impl;

import ua.org.oa.s.alex.MovieHall.model.User;

import java.sql.*;
import java.util.List;

/**
 * Created by Admin on 20.03.2017.
 */
public class UserDaoImpl extends CrudDao<User> {
    private final  String INSERT="Insert into user (user_name, user_surname, user_login, user_password, " +
            "user_email, user_discount,  permissions) values(?, ?, ?, ?, ?, ?, ?)";
    private final  String UPDATE="Update user Set user_name=?, user_surname=?, user_login=?, user_password=?," +
            "user_email=?, user_discount=?,  permissions=? where user_id=?";
    private static UserDaoImpl userDao;

    private UserDaoImpl(Class type) {
        super(type);
    }

    public static UserDaoImpl getInstance() {
        if (userDao == null) {
            userDao = new UserDaoImpl(User.class);
        }
        return userDao;
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, User entity) throws SQLException {
        PreparedStatement preparedStatement=connection.prepareStatement(UPDATE);
        preparedStatement.setString(1, entity.getUserName());
        preparedStatement.setString(2, entity.getUserSurname());
        preparedStatement.setString(3, entity.getUserSurname());
        preparedStatement.setString(4, entity.getUserSurname());
        preparedStatement.setString(5, entity.getUserSurname());
        preparedStatement.setInt(6, entity.getUserDiscount());
        preparedStatement.setInt(7, entity.getUserDiscount());
        preparedStatement.setInt(8, entity.getId());
        return preparedStatement;
    }

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, User entity) throws SQLException {
        PreparedStatement preparedStatement=connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1, entity.getUserName());
        preparedStatement.setString(2, entity.getUserSurname());
        preparedStatement.setString(3, entity.getUserSurname());
        preparedStatement.setString(4, entity.getUserSurname());
        preparedStatement.setString(5, entity.getUserSurname());
        preparedStatement.setInt(6, entity.getUserDiscount());
        preparedStatement.setInt(7, entity.getPermissions());

        return preparedStatement;
    }

    @Override
    protected List<User> readAll(ResultSet resultSet) throws SQLException {
        return null;
    }
}
