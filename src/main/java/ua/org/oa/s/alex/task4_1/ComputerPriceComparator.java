package ua.org.oa.s.alex.task4_1;

import java.util.Comparator;

/**
 * Created by Admin on 14.02.2017.
 */
public class ComputerPriceComparator implements Comparator {
    @Override
    public int compare(Object o1, Object o2) {
        if(o1==null || o2==null){
            throw new IllegalArgumentException("Objects of compare is null");
        }
        if(o1.getClass()!=Computer.class){
            throw new IllegalArgumentException(o1+" Object is not a Computer class");
        }
        if(o2.getClass()!=Computer.class){
            throw new IllegalArgumentException(o2+" Object is not a Computer class");
        }
        Computer first=(Computer)o1;
        Computer second=(Computer)o2;

        return first.getPrice()-second.getPrice();
    }
}
