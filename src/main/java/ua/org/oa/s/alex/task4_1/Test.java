package ua.org.oa.s.alex.task4_1;

import java.util.Arrays;
import java.util.Comparator;
import java.util.StringTokenizer;

/**
 * Created by Admin on 13.02.2017.
 */
public class Test<T> {

    public static void main(String[] args) {
        Integer[] intArray = new Integer[120];

        for (int i = 0; i < intArray.length; i++) {
            intArray[i] = Integer.valueOf(String.valueOf(System.nanoTime() % 1_000_000));
        }
        for (int i = 0; i < intArray.length; i++) {
            System.out.println(intArray[i]);
        }
        System.out.println();
        System.out.println();
        System.out.println(Test.max(intArray, null));
        //========================================================================
        System.out.println("=================================================================");
        String testStr = "Каждый веб-разработчик знает, что такое текст-«рыба». Текст этот, несмотря на название, не имеет никакого отношения к обитателям водоемов. Используется он веб-дизайнерами для вставки на интернет-страницы и демонстрации внешнего вида контента, просмотра шрифтов, абзацев, отступов и т.д. Так как цель применения такого текста исключительно демонстрационная, то и смысловую нагрузку ему нести совсем необязательно. Более того, нечитабельность текста сыграет на руку при оценке качества восприятия макета." +

                "Самым известным «рыбным» текстом является знаменитый Lorem ipsum. Считается, что впервые его применили в книгопечатании еще в XVI веке. Своим появлением Lorem ipsum обязан древнеримскому философу Цицерону, ведь именно из его трактата «О пределах добра и зла» средневековый книгопечатник вырвал отдельные фразы и слова, получив текст-«рыбу», широко используемый и по сей день. Конечно, возникают некоторые вопросы, связанные с использованием Lorem ipsum на сайтах и проектах, ориентированных на кириллический контент – написание символов на латыни и на кириллице значительно различается." +

                "И даже с языками, использующими латинский алфавит, могут возникнуть небольшие проблемы: в различных языках те или иные буквы встречаются с разной частотой, имеется разница в длине наиболее распространенных слов. Отсюда напрашивается вывод, что все же лучше использовать в качестве «рыбы» текст на том языке, который планируется использовать при запуске проекта. Сегодня существует несколько вариантов Lorem ipsum, кроме того, есть специальные генераторы, создающие собственные варианты текста на основе оригинального трактата, благодаря чему появляется возможность получить более длинный неповторяющийся набор слов.";
        StringTokenizer stringTokenizer = new StringTokenizer(testStr, " ,.");
        String[] strArray = new String[100];
        int i = 0;
        while (stringTokenizer.hasMoreTokens() && i < strArray.length) {
            strArray[i] = stringTokenizer.nextToken();
            System.out.println(strArray[i]);
            i++;
        }
        System.out.println();
        System.out.println();
        System.out.println(Test.max(strArray, null));
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        Computer[] compArray = new  Computer[10];
        compArray[0] = new Computer("AMD Sempron", 2000, 4096, 512);
        compArray[1] = new Computer("AMD Atlon", 2500, 4096, 768);
        compArray[2] = new Computer("AMD Sempron", 2000, 4096, 512);
        compArray[3] = new Computer("AMD Atlon", 2500, 4096, 768);
        compArray[4] = new Computer("AMD Phenom", 2700, 4096, 512);
        compArray[5] = new Computer("Intel Core2", 2500, 2048, 512);
        compArray[6] = new Computer("Intel Core2 Duo", 2200, 4096, 512);
        compArray[7] = new Computer("Intel Core i3", 2700, 4096, 512);
        compArray[8] = new Computer("Intel Core i7", 3000, 4096, 512);
        compArray[9] = new Computer("Intel Core i5", 2900, 4096, 768);
        for(Computer computer: compArray){
            System.out.println(computer.toString());
        }
        System.out.println();
        System.out.println();
        Comparator comparator=new ComputerPriceComparator();
        System.out.println(Test.max(compArray, comparator));
//********************************************************************************************
        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");

        //==========================================================================================
        System.out.println("========================================================================================");
        Car[] carArray = new Car[10];
        carArray[0] = new Car("Gaz-21", 1953, 700);
        carArray[1] = new Car("Gaz-53", 1973, 1500);
        carArray[2] = new Car("Gazelle", 1998, 4500);
        carArray[3] = new Car("AZLK-2140", 1985, 600);
        carArray[4] = new Car("Niva", 1996, 4500);
        carArray[5] = new Car("Lada 2105", 1993, 1200);
        carArray[6] = new Car("Daewoo", 2003, 1800);
        carArray[7] = new Car("KamAZ", 1998, 7500);
        carArray[8] = new Car("Geely", 2005, 1250);
        carArray[9] = new Car("Ford", 1998, 3500);

        for(Car car: carArray){
            System.out.println(car.toString());
        }
        System.out.println();
        System.out.println();
        Comparator comparator1=new ComputerPriceComparator();
        System.out.println(Test.max(carArray , comparator1));
    }
//------------------------------------------------------------------------------------
    public static Object max(Object[] array, Comparator c) {

        Arrays.sort(array, c);
        int length = array.length;
        return array[length - 1];
    }
}
