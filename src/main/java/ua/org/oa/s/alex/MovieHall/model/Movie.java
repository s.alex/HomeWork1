package ua.org.oa.s.alex.MovieHall.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;


/**
 * Created by A Sosnovsky  on 07.02.2017.
 */
@Getter
@Setter
@EqualsAndHashCode
public class Movie extends Entity<Integer> {
    private String name;
    private String genre; //жанр
    private String producedBy; //производство
    private int duration; //длительность
    private String description; //
    private int rating;
    private String country;
    private String icon;
    private String actors;

    public Movie() {

    }

    @Override
    public String toString() {
        return "Movie{" +
                "title='" + name + '\'' +
                ", description='" + description + '\'' +
                ", duration=" + duration +
                ", genre=" + genre +
                ", produced by=" + producedBy +
                ", rating=" + rating +
                ", actors=" + actors +
                "} ";
    }


    public Movie(String name, String genre, String producedBy, int duration, String description, int rating, String country, String icon, String actors) {
        this.name = name;
        this.genre = genre;
        this.producedBy = producedBy;
        this.duration = duration;
        this.description = description;
        this.rating = rating;
        this.country = country;
        this.icon = icon;
        this.actors = actors;
    }
}
