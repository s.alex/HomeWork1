package ua.org.oa.s.alex.MovieHall.dao.impl;


import ua.org.oa.s.alex.MovieHall.datasource.DataSource;
import ua.org.oa.s.alex.MovieHall.model.Movie;


import java.sql.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Admin on 14.03.2017.
 */
public class MovieDaoImpl extends CrudDao<Movie>{
   private final  String INSERT="Insert into movie (movie_title, movie_ganre, movie_produced_by, movie_duration, movie_description, movie_rating, movie_country, movie_icon, movie_actors) values(?, ?, ?, ?, ?, ?, ?, ?, ?)";
    private final  String UPDATE="Update movie Set movie_title=?, movie_ganre=?, movie_produced_by=?, movie_duration=?, movie_description=?, movie_rating=?, movie_country=?, movie_icon=?, movie_actors=? where id=?";
    private  final String READ_ALL_BY_SEANCE_ID="Select * from movie";
    private static MovieDaoImpl crudDAO;
    private MovieDaoImpl(Class type){super(type);}
    public static  MovieDaoImpl getInstance(){
        if(crudDAO==null){
            crudDAO=new MovieDaoImpl(Movie.class);
        }
        return crudDAO;
    }
   // String name, String genre, String producedBy, int duration, String description, int rating, String country, String icon, String actors
    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Movie entity) throws SQLException {
        PreparedStatement preparedStatement=connection.prepareStatement(UPDATE);
        preparedStatement.setString(1, entity.getName());
        preparedStatement.setString(2, entity.getGenre());
        preparedStatement.setString(3, entity.getProducedBy());
        preparedStatement.setInt(4, entity.getDuration());
        preparedStatement.setString(5, entity.getDescription());
        preparedStatement.setInt(6, entity.getRating());
        preparedStatement.setString(7, entity.getCountry());
        preparedStatement.setString(8, entity.getIcon());
        preparedStatement.setString(9, entity.getActors());
        preparedStatement.setInt(10, entity.getId());

        return  preparedStatement;
    }
// String name, String genre, String producedBy, int duration, String description, int rating, String country, String icon, String actors
    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Movie entity) throws SQLException {
        PreparedStatement preparedStatement=connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1, entity.getName());
        preparedStatement.setString(2, entity.getGenre());
        preparedStatement.setString(3, entity.getProducedBy());
        preparedStatement.setInt(4, entity.getDuration());
        preparedStatement.setString(5, entity.getDescription());
        preparedStatement.setInt(6, entity.getRating());
        preparedStatement.setString(7, entity.getCountry());
        preparedStatement.setString(8, entity.getIcon());
        preparedStatement.setString(9, entity.getActors());
        return  preparedStatement;
    }
// String name, String genre, String producedBy, int duration, String description, int rating, String country, String icon, String actors
    @Override
    protected List<Movie> readAll(ResultSet resultSet) throws SQLException {
        List<Movie> list=new LinkedList<>();
        Movie movie=null;
        if(!resultSet.next()){
            System.out.println("В таблице нет записей");
        }else{
            resultSet.beforeFirst();
        }
        while (resultSet.next()){
            movie=new Movie();
            movie.setId(resultSet.getInt("movie_id"));
            movie.setName(resultSet.getString("movie_title"));
            movie.setGenre(resultSet.getString("movie_ganre"));
            movie.setProducedBy(resultSet.getString("movie_produced_by"));
            movie.setDuration(resultSet.getInt("movie_duration"));
            movie.setDescription(resultSet.getString("movie_description"));
            movie.setRating(resultSet.getInt("movie_rating"));
            movie.setCountry(resultSet.getString("movie_country"));
            movie.setIcon(resultSet.getString("movie_icon"));
            movie.setActors(resultSet.getString("movie_actors"));
            list.add(movie);
        }
        return list;
    }

//    public List<Movie> readAllBySeanceId(int key){
//        List<Movie> movieList=null;
//        try (Connection connection = DataSource.getInstance().getConnection();
//             PreparedStatement preparedStatement = connection.prepareStatement(READ_ALL_BY_SEANCE_ID);
//        ) {
//            preparedStatement.setInt(1, key);
//
//            ResultSet resultSet = preparedStatement.executeQuery();
//
//            movieList = readAll(resultSet);
//        } catch (SQLException e) {
//        }
//        return movieList;
//    }
}
