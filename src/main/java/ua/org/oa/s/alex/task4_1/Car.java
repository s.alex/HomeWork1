package ua.org.oa.s.alex.task4_1;

/**
 * Created by Admin on 13.02.2017.
 */
public class Car {
    private String mark;
    private int dateOfManufacture;
    private int price;

    public Car(String mark, int dateOfManufacture, int price) {
        this.mark = mark;
        this.dateOfManufacture = dateOfManufacture;
        this.price = price;
    }

    @Override
    public String toString() {
        return "Car{" +
                "mark='" + mark + '\'' +
                ", dateOfManufacture=" + dateOfManufacture +
                ", price=" + price +
                '}';
    }
}
