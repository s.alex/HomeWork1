package ua.org.oa.s.alex.task4_2.part3;

import java.util.Iterator;

/**
 * Created by A Sosnovskyi on 17.02.2017.
 */
public interface ListIterator<E> extends Iterator<E> {
    boolean hasPrevious();
    E previous();
    void set(E e);
    void remove();
}
