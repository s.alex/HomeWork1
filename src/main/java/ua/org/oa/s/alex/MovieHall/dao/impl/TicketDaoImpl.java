package ua.org.oa.s.alex.MovieHall.dao.impl;

import ua.org.oa.s.alex.MovieHall.model.Ticket;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Admin on 20.03.2017.
 */
public class TicketDaoImpl extends CrudDao<Ticket> {
    private final  String INSERT="Insert into ticket (ticket_movie_house_name, ticket_hall_name, ticket_time_of_sell," +
            "ticket_user_id, ticket_seance_id, ticket_seat_row, ticket_seat_position, ticket_price) values(?, ?, ?, ?, ?, ?, ?, ?)";

    private final  String UPDATE="Update ticket Set ticket_movie_house_name=?, ticket_hall_name=?," +
            " ticket_time_of_sell=?, ticket_user_id=?, ticket_seance_id=?, ticket_seat_row=?," +
            " ticket_seat_position=?, ticket_price=? where ticket_id=?";
    private static TicketDaoImpl ticketDao;

    private TicketDaoImpl(Class type){super(type);}

    public static TicketDaoImpl getInstance(){
        if(ticketDao==null){
            ticketDao=new TicketDaoImpl(Ticket.class);
        }
        return ticketDao;
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Ticket entity) throws SQLException {
        PreparedStatement preparedStatement=connection.prepareStatement(UPDATE);
        preparedStatement.setInt(1, entity.getMovieHouseName() );
        preparedStatement.setInt(2, entity.getHall() );
        preparedStatement.setTime(3,  new Time(entity.getTimeOfSell().getTime()));
        preparedStatement.setInt(4, entity.getUserId());
        preparedStatement.setInt(5, entity.getSeanceId());
        preparedStatement.setInt(6, entity.getSeatRow());
        preparedStatement.setInt(7, entity.getSeatPosition());
        preparedStatement.setInt(8, entity.getPrice());
        preparedStatement.setInt(9, entity.getId());
        return preparedStatement;
    }

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Ticket entity) throws SQLException {
        PreparedStatement preparedStatement=connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setInt(1, entity.getMovieHouseName() );
        preparedStatement.setInt(2, entity.getHall() );
        preparedStatement.setTime(3,  new Time(entity.getTimeOfSell().getTime()));
        preparedStatement.setInt(4, entity.getUserId());
        preparedStatement.setInt(5, entity.getSeanceId());
        preparedStatement.setInt(6, entity.getSeatRow());
        preparedStatement.setInt(7, entity.getSeatPosition());
        preparedStatement.setInt(8, entity.getPrice());
        return preparedStatement;
    }

    @Override
    protected List<Ticket> readAll(ResultSet resultSet) throws SQLException {
        List<Ticket> ticketList=new LinkedList<>();
        Ticket ticketTmp=null;
        if(!resultSet.next()){
            System.out.println("Table Ticket is Emty");
        }else{resultSet.beforeFirst();}
        //int movieHouseName,  Date timeOfSell,int userId, int hall, int seanceId, int seatRow, int seatPosition, int price
       //ticket_movie_house_name, ticket_hall_name, ticket_time_of_sell, ticket_user_id, ticket_seance_id, ticket_seat_row, ticket_seat_position, ticket_price
        while(resultSet.next()){
            ticketTmp=new Ticket(resultSet.getInt("ticket_movie_house_name"), resultSet.getTime("ticket_time_of_sell"),
                    resultSet.getInt("ticket_user_id"), resultSet.getInt("ticket_hall_name"), resultSet.getInt("ticket_seance_id"),
                    resultSet.getInt("ticket_seat_row"), resultSet.getInt("ticket_seat_position"), resultSet.getInt("ticket_price"));
            ticketList.add(ticketTmp);
        }
        return ticketList;
    }
}
