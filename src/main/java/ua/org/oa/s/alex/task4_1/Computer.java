package ua.org.oa.s.alex.task4_1;

import java.util.Objects;

/**
 * Created by Admin on 13.02.2017.
 */
public class Computer implements Comparable {
    private String markOfProsessor;
    private int memoryRAM;
    private int memoryROM;
    private int price;

    public Computer(String markOfProsessor, int price, int memoryRAM, int memoryROM ) {
        this.price = price;
        this.memoryRAM = memoryRAM;
        this.memoryROM = memoryROM;
        this.markOfProsessor = markOfProsessor;
    }

    @Override
    public int compareTo(Object o) {

        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Computer computer = (Computer) o;
        return memoryRAM == computer.memoryRAM &&
                memoryROM == computer.memoryROM &&
                price == computer.price &&
                Objects.equals(markOfProsessor, computer.markOfProsessor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(markOfProsessor, memoryRAM, memoryROM, price);
    }

    @Override
    public String toString() {
        return "Computer{" +
                "markOfProsessor='" + markOfProsessor + '\'' +
                ", memoryRAM=" + memoryRAM +
                ", memoryROM=" + memoryROM +
                ", price=" + price +
                '}';
    }

    public String getMarkOfProsessor() {
        return markOfProsessor;
    }

    public int getPrice() {
        return price;
    }

    public int getMemoryROM() {
        return memoryROM;
    }

    public int getMemoryRAM() {
        return memoryRAM;
    }
}
