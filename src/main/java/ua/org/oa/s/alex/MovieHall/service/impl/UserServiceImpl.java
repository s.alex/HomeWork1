package ua.org.oa.s.alex.MovieHall.service.impl;

import ua.org.oa.s.alex.MovieHall.dao.DaoFactory;
import ua.org.oa.s.alex.MovieHall.dao.api.Dao;
import ua.org.oa.s.alex.MovieHall.dto.UserDTO;
import ua.org.oa.s.alex.MovieHall.mapper.BeanMapper;
import ua.org.oa.s.alex.MovieHall.model.User;
import ua.org.oa.s.alex.MovieHall.service.api.Service;

import java.util.List;

/**
 * Created by Admin on 21.03.2017.
 */
public class UserServiceImpl implements Service<Integer, UserDTO> {
    private static UserServiceImpl service;

    private Dao<Integer, User> userDao;
    private BeanMapper beanMapper;

    private UserServiceImpl() {
        userDao = DaoFactory.getInstance().getUserDao();
        beanMapper = BeanMapper.getInstance();
    }

    public static synchronized UserServiceImpl getInstance() {
        if (service == null) {
            service = new UserServiceImpl();
        }

        return service;
    }

    @Override
    public List<UserDTO> getAll() {
        List<User> userList = userDao.getAll();
        List<UserDTO> userDTOList = beanMapper.listMapToList(userList, UserDTO.class);
        return userDTOList;
    }

    @Override
    public UserDTO getById(Integer key) {
        User user = userDao.getById(key);
        UserDTO userDTO = beanMapper.singleMapper(user, UserDTO.class);
        return userDTO;
    }

    @Override
    public void save(UserDTO entity) {
        User user = beanMapper.singleMapper(entity, User.class);
        userDao.save(user);
    }

    @Override
    public void delete(Integer key) {
        userDao.delete(key);

    }

    @Override
    public void update(UserDTO entity) {
        User user = beanMapper.singleMapper(entity, User.class);
        userDao.update(user);
    }
}
