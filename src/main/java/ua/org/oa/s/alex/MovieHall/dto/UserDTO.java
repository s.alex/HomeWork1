package ua.org.oa.s.alex.MovieHall.dto;

import lombok.Getter;
import lombok.Setter;
import ua.org.oa.s.alex.MovieHall.model.Entity;
import ua.org.oa.s.alex.MovieHall.model.Ticket;

import java.util.List;

/**
 * Created by Admin on 21.03.2017.
 */
@Getter
@Setter
public class UserDTO extends Entity<Integer> {
    private int userId;
    private String userName; //Имя
    private String userSurname; //Фамилия
    private String userLogin; //Login
    private String userPassword; // Password
    private String userEmail; // Email
    private int userDiscount;
    private List<Ticket> purchasedTickets; //купленный билет
    private int permissions;  //права доступа

    public UserDTO(String usernName, String userSurname, String userLogin, String userPassword, String userEmail) {

        this.userName = usernName;
        this.userSurname = userSurname;
        this.userLogin = userLogin;
        this.userPassword = userPassword;
        this.userEmail = userEmail;
        this.permissions = 1;
    }

    public UserDTO() {
    }


}
