package ua.org.oa.s.alex.MovieHall.model;

/**
 * Created by A Sosnovsky on 07.02.2017.
 */
public class Entity<T> {
    private T id;

    public void setId(T id) {
        this.id = id;
    }

    public T getId() {

        return id;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj==null){
            return false;
        }
        if(!(obj instanceof Entity)){
            return false;
        }
        if(this==obj){
            return true;
        }
        Entity<?> entity=(Entity<?>)obj;
        return this.getId().equals(entity.getId());
    }
}
