package ua.org.oa.s.alex.MovieHall.dao.impl;

import ua.org.oa.s.alex.MovieHall.datasource.DataSource;
import ua.org.oa.s.alex.MovieHall.model.Seat;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 19.03.2017.
 */
public class SeatDaoImpl extends CrudDao<Seat> {
    private final  String INSERT="Insert into seat (seat_row, seat_position, seat_free, seat_hall," +
                                  " seat_movie_house, seat_seance) values(?, ?, ?, ?, ?, ?)";

    private final  String UPDATE="Update seat Set seat_row=?, seat_position=?, seat_free=? where seat_id=?";
    private final  String READ_ALL_BY_HALL_ID="SELECT * FROM seat where seat_hall=?";
    private static SeatDaoImpl seatDao;
    private SeatDaoImpl(Class type){super(type);}

    public static SeatDaoImpl getInstance(){
        if(seatDao==null){
            seatDao=new SeatDaoImpl(Seat.class);
        }
        return seatDao;
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Seat entity) throws SQLException {
        PreparedStatement preparedStatement=connection.prepareStatement(UPDATE);
       // preparedStatement.setInt(1, entity.getSeatRow());
       // preparedStatement.setInt(2, entity.getSeatPosition());
        preparedStatement.setInt(3, (entity.isSeatFree()? 1: 0));
        preparedStatement.setInt(4, entity.getId());
        return preparedStatement;
    }

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Seat entity) throws SQLException {
        PreparedStatement preparedStatement=connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setInt(1, entity.getSeatRow());
        preparedStatement.setInt(2, entity.getSeatPosition());
        preparedStatement.setInt(3, (entity.isSeatFree()? 1: 0));
        preparedStatement.setInt(4, entity.getHall());
        preparedStatement.setInt(5, entity.getMovieHouse());
        preparedStatement.setInt(6, entity.getSeance());
        return preparedStatement;
    }

    protected PreparedStatement createInsertBatchStatement(Connection connection, List<Seat> entity) throws SQLException {
        PreparedStatement preparedStatement=connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        for(Seat seatTmp: entity){
            preparedStatement.setInt(1, seatTmp.getSeatRow());
            preparedStatement.setInt(2, seatTmp.getSeatPosition());
            preparedStatement.setInt(3, (seatTmp.isSeatFree()? 1: 0));
            preparedStatement.setInt(4, seatTmp.getHall());
            preparedStatement.setInt(5, seatTmp.getMovieHouse());
            preparedStatement.setInt(6, seatTmp.getSeance());
            preparedStatement.addBatch();
        }

        return preparedStatement;
    }

    @Override
    protected List<Seat> readAll(ResultSet resultSet) throws SQLException {
        List<Seat> listSeat=new ArrayList<>();
        Seat seatTmp=null;
        if(!resultSet.next()){
            System.out.println("Table of Seats is empty");
        }else{
            resultSet.beforeFirst();
        }
        //int seatRow, int seatPosition, int hall, int movieHouse
        while (resultSet.next()){
            seatTmp=new Seat(resultSet.getInt("seat_row"), resultSet.getInt("seat_position"),
                    resultSet.getInt("seat_hall"), resultSet.getInt("seat_movie_house"));
            seatTmp.setSeatFree(resultSet.getInt("seat_free")==1?true:false);
            seatTmp.setSeance(resultSet.getInt("seat_seance"));
            listSeat.add(seatTmp);
        }
        return listSeat;
    }


    public  List<Seat> readAllByHallId(int key){
        List<Seat> seatList=null;

        try (Connection connection = DataSource.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(READ_ALL_BY_HALL_ID)
        ) {
            preparedStatement.setInt(1, key);
            ResultSet resultSet = preparedStatement.executeQuery();
            seatList = readAll(resultSet);
        } catch (SQLException e) {
        }

        return seatList;
    }
}
