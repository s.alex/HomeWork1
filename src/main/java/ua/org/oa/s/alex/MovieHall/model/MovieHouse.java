package ua.org.oa.s.alex.MovieHall.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;

/**
 * Created by Admin on 16.03.2017.
 */
@Getter
@Setter
public class MovieHouse extends Entity<Integer> {
    private String name;
    private int hallsQuantity;
    private Hall[] movieHalls;


    public MovieHouse() {
    }

    public MovieHouse(String name, int hallsQuantity) {
        this.name = name;
        this.hallsQuantity = hallsQuantity;
        this.movieHalls = new Hall[hallsQuantity];
    }

public void setMovieHalls(Hall[] halls){
    this.movieHalls=halls;
}
    @Override
    public String toString() {
        return "MovieHouse{" +
                "movieHouseId="+this.getId()+
                " name='" + name + '\'' +
                ", halls=" + Arrays.toString(movieHalls) +
                ", hallsQuantity=" + hallsQuantity +
                '}';
    }
}

