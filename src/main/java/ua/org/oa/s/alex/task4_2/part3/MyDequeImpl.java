package ua.org.oa.s.alex.task4_2.part3;

import java.util.Iterator;
import java.util.Objects;

/**
 * Created by A Sosnovskyi on 17.02.2017.
 */
public class MyDequeImpl<E> implements MyDeque<E> {
    public int size;
    private Node<E> head = null;
    private Node<E> tail = null;

    public MyDequeImpl() {
        this.size = 0;
        this.head = null;
        this.tail = null;
    }

    /**
     * addFirst
     *
     * @param e E element
     */
    @Override
    public void addFirst(E e) {
        if (this.head == null && this.tail == null) {
            this.head = new Node<>(e, null, null);
            this.tail = this.head; //первый помещенный элемент есть и начало и конец

        } else {
            Node<E> old = this.head;
            Node<E> newNode = new Node<>(e, old, null);
            this.head = newNode; //elem, next, prev
            old.prev = newNode;

        }
        this.size += 1;
    }

    /**
     * addLast
     *
     * @param e E element
     */
    @Override
    public void addLast(E e) {
        if (this.tail == null && this.head == null) {
            this.tail = new Node<>(e, null, null);
            this.head = this.tail;

        } else {
            Node<E> old = this.tail;
            this.tail = new Node<>(e, null, old); //elem, next, prev
            old.next = this.tail;

        }
        this.size += 1;
    }

    /**
     * removeFirst
     *
     * @return E removed element
     */
    @Override
    public E removeFirst() {
        Node<E> tmpNode = this.head;
        this.head = this.head.next; //Убрать ссылку на первый элемент
        System.out.println("this.head.next" + this.head.next.element);
        return tmpNode.element;
    }

    /**
     * removeLast
     *
     * @return E removed element
     */
    @Override
    public E removeLast() {
        Node<E> tmpNode = this.tail;
        this.tail = this.tail.prev; //Убрать ссылку на последний элемент
        return tmpNode.element;
    }


    /**
     * getFirst
     *
     * @return E element
     */
    @Override
    public E getFirst() {
        if (this.head == null) {
            throw new NullPointerException("В очереди нет элементов");
        }
        return this.head.element;
    }

    /**
     * getLast
     *
     * @return E element
     */
    @Override
    public E getLast() {
        if (this.tail == null) {
            throw new NullPointerException("В очереди нет элементов");
        }
        return this.tail.element;
    }


    /**
     * contains
     *
     * @param o Object
     * @return boolean
     */
    @Override
    public boolean contains(Object o) {
        if (o == null) {
            throw new IllegalArgumentException(o + "=null");
        }

        Node node = this.head;

        int i=0;
        while (i++ < size) {
            if (node.element.equals(o)) {
                return true;
            }
            node = node.next;
        }
        return false;
    }

    /**
     * clear
     */
    @Override
    public void clear() {
        this.head = null;
        this.tail = null;
        this.size = 0;
    }


    /**
     * toArray
     *
     * @return Object[] of elements
     */
    @Override
    public Object[] toArray() {
        Node node = this.head;
        Object[] result = new Object[size];
        int iter = 0;
        int i=0;

        while (i++<size) {
            result[iter++] = node.element;

            node = node.next;

        }
        return result;
    }


    /**
     * size
     *
     * @return int size
     */
    @Override
    public int size() {

        return this.size;
    }


    /**
     * @param deque MyDeque<? extends E>
     * @return boolean
     */
    @Override
    public boolean containsAll(MyDeque<? extends E> deque) {
        if (deque.size() == 0) {
            throw new IllegalArgumentException("не может быть равной " + deque.size());
        }
        for(E element:deque){
           if(!contains(element)){
               return false;
           }
        }
//        Object[] dequeArray = deque.toArray();
//        for (int index = 0; index < dequeArray.length; index++) {
//            if (!this.contains(dequeArray[index])) {
//                return false;
//            }
//        }
        return true;
    }

    /**
     * @return Iterator<E>
     */
    @Override
    public Iterator<E> iterator() {
        return new IteratorImpl();
    }

    /**
     * toString
     *
     * @return String
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        Node node = this.head;

        while (node.next != null) {
            sb.append(node.toString()).append(", ");
            node = node.next;
        }


        return sb.toString();
    }

    /**
     * @return Node<E> head
     */
    public Node<E> getHead() {
        return head;
    }

    /**
     * @return Node<E> tail
     */
    public Node<E> getTail() {
        return tail;
    }

    /**
     * @return ListIteratorImpl()
     */
    @Override
    public ListIterator<E> listIterator() {
        return new ListIteratorImpl();
    }

    /**
     * class Node
     * @param <E> тип хранимого єлемента
     */
    private static class Node<E> {
        E element;
        Node<E> next;
        Node<E> prev;

        /**
         * конструктор
         * @param element E
         * @param next Node<E>
         * @param prev Node<E>
         */
        public Node(E element, Node<E> next, Node<E> prev) {
            this.element = element;
            this.next = next;
            this.prev = prev;
        }

        /**
         *  equals
         * @param o Object
         * @return boolean
         */
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Node<?> node = (Node<?>) o;
            return Objects.equals(element, node.element);
        }

        /**
         * hashCode
         * @return int
         */
        @Override
        public int hashCode() {
            return Objects.hash(element);
        }

        /**
         * toString
         * @return String Node
         */
        @Override
        public String toString() {
            if (element == null) {
                return "null";
            }
            return "Node{" +
                    "element=" + element +
                    '}';
        }
    }

    /**
     * IteratorImpl
     */
    private class IteratorImpl implements Iterator<E> {
        Node<E> currentHeadNode;
        int current;
        int flag;

        /**
         * конструктор
         */
        public IteratorImpl() {
            this.currentHeadNode = getHead();
            this.current = 0;
            this.flag = 0;
        }

        /**
         * hasNext определяет существует ли следующий єлемент
         * @return boolean true-да, false-нет
         */
        @Override
        public boolean hasNext() {
//            if (current < size) {
//                return true;
//            }
//            return false;
            return currentHeadNode.next != null;  //наверное так нагляднее
        }

        /**
         * next
         * @return E current element
         */
        @Override
        public E next() {
            Node<E> result = currentHeadNode;
            currentHeadNode = currentHeadNode.next;
            current++;
            flag = 1;
            return result.element;
        }

        /**
         * remove current element
         */
        @Override
        public void remove() {
            if (flag == 0 || size==1) { // не вызывался метод next()
                throw new IllegalStateException("Метод remove() так не используется");
            }
            Node prevNode = currentHeadNode.prev;
            Node nextNode = currentHeadNode.next;
            prevNode.next = nextNode;
            nextNode.prev = prevNode;
            size -= 1; //уменьшить размер коллекции на 1
            this.flag = 0;
        }


    }

    /**
     *ListIteratorImpl
     */
    private class ListIteratorImpl extends IteratorImpl implements ListIterator<E> {

        int currentTail;
        Node<E> currentTailNode;
        int flagTail;

        /**
         * constructor
         */
        public ListIteratorImpl() {
            this.currentTail = size - 1;
            this.currentTailNode = getTail();
            this.flagTail = 0;
        }

        /**
         * hasNext
         * @return boolean
         */
        public boolean hasNext() {
            if (flagTail == 0) {
                return super.hasNext();
            } else {
                current = currentTail + 2;
                currentHeadNode = currentTailNode.next.next;
                return super.hasNext();
            }
//            return false;
        }

        /**
         * next
         * @return E next element
         */
        public E next() {
            if (flagTail == 0) {

            } else {
                flagTail = 0;

            }
            return super.next();
        }

        /**
         * hasPrevious
         *
         * @return boolean
         */
        @Override
        public boolean hasPrevious() {
            if (flag == 0) {
                return (currentTail >= 0) ? true : false;
            } else {

                currentTail = current - 2;
                currentTailNode = currentHeadNode.prev.prev;
                return (currentTail >= 0) ? true : false;
            }

        }

        /**
         * возвращает текущий прочитанный элемент
         * previous
         *
         * @return E element
         */
        @Override
        public E previous() {
            if (flag == 0) {

            } else {
                flag = 0;
            }
            Node<E> result = currentTailNode;
            currentTailNode = currentTailNode.prev;
            currentTail--;
            flagTail = 1;
            return result.element;
        }

        /**
         * set new value to an element
         * @param o Object
         */
        @Override
        public void set(E o) {
            if (flag == 0 && flagTail == 0) {
                throw new IllegalStateException("метод set() так не используется");
            }
            if (flag != 0) {
                currentHeadNode.element =  o;
            }
            if (flagTail != 0) {

                currentTailNode.element = o;
            }
        }

        /**
         * remove element
         */
        @Override
        public void remove() {
            if (flag == 0 && flagTail == 0) {
                throw new IllegalStateException("метод remove() так не используется");
            }
            if (flag != 0) {
                super.remove();
            }
            if (flagTail != 0) {
                Node prevNode = currentTailNode.next;
                Node nextNode = currentTailNode.prev;
                prevNode.prev=nextNode;
                nextNode.next=prevNode;
                size-=1;
                flagTail=0;
            }
        }
    }

}
