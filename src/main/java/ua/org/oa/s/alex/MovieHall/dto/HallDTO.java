package ua.org.oa.s.alex.MovieHall.dto;

import lombok.Getter;
import lombok.Setter;
import ua.org.oa.s.alex.MovieHall.model.Entity;
import ua.org.oa.s.alex.MovieHall.model.Seance;
import ua.org.oa.s.alex.MovieHall.model.Seat;

import java.util.List;

/**
 * Created by Admin on 21.03.2017.
 */
@Getter
@Setter
public class HallDTO extends Entity<Integer> {
    private String name;
    private int seatsQuantity;
    private int rows;
    private int rowSeats;
    private int seance;
    private int movieHouseId;
    private Seat[] seats;
    private List<Seance> seances;


    public HallDTO(String name,  int rows, int rowSeats, int seatsQuantity, int movieHouseId) {
        this.name = name;
        this.seatsQuantity = seatsQuantity;
        this.rows = rows;
        this.rowSeats = rowSeats;
        this.movieHouseId = movieHouseId;
        this.seats=new Seat[seatsQuantity];
    }

    public HallDTO() {
    }
}
