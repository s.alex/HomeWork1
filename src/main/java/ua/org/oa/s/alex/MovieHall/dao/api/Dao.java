package ua.org.oa.s.alex.MovieHall.dao.api;




import ua.org.oa.s.alex.MovieHall.model.Entity;

import java.util.List;


/**
 * Created by Admin on 14.03.2017.
 */
public interface  Dao <K, T extends Entity<K>>{
    List<T> getAll();
    T getById(K key);
    void save(T entity);
    void delete(K key);
    void update(T entity);
}
