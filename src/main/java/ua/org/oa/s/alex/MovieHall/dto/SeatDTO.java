package ua.org.oa.s.alex.MovieHall.dto;

import lombok.Getter;
import lombok.Setter;
import ua.org.oa.s.alex.MovieHall.model.Entity;

/**
 * Created by Admin on 21.03.2017.
 */
@Getter
@Setter
public class SeatDTO extends Entity<Integer> {
private int seatRow; //ряд
private int seatPosition; //место
private boolean seatFree; //свободно
private int hall; // зал
private int movieHouse;
private int seance;

public SeatDTO(int seatRow, int seatPosition, int hall, int movieHouse) {
        this.seatRow = seatRow;
        this.seatPosition = seatPosition;
        this.hall = hall;
        this.movieHouse = movieHouse;
        this.seatFree=true;
        }

        public SeatDTO() {
        }
}
