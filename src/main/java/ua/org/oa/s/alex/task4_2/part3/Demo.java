package ua.org.oa.s.alex.task4_2.part3;


import java.util.Arrays;
import java.util.Iterator;

/**
 * Created by A Sosnovskyi on 16.02.2017.
 */
class Demo {
    public static void main(String[] args) {
        MyDeque<Number> deque = new MyDequeImpl();
        deque.addFirst(433);
        deque.addFirst(455);
        deque.addFirst(555);
        deque.addLast(8.88);
        deque.addLast(9.99);
        deque.addLast(7.77);

        System.out.println(Arrays.toString(deque.toArray()));
        System.out.println("===================for (Number element : deque)==========================================================");
        for (Number element : deque) {
            System.out.println(element);
        }
        System.out.println("===========================deque.iterator=================================================");
        Iterator iterator = deque.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        System.out.println("=============================================================================");
        Iterator iterator1 = deque.iterator();
        iterator1.next();
        iterator1.remove();
//        //iterator1.remove();
        Iterator iterator2 = deque.iterator();
        while (iterator2.hasNext()) {
            System.out.println(iterator2.next());
        }
              System.out.println("=================ListIterator====li.next()========================================================");
        ListIterator li = deque.listIterator();
        while (li.hasNext()) {
            System.out.println(li.next());
        }
        System.out.println("=============================li1.previous()================================================");
        ListIterator li1=deque.listIterator();
        while (li1.hasPrevious()){
            System.out.println(li1.previous());
        }
        System.out.println("===================li2.next()=====li2.previous()=====================================================");
        ListIterator li2=deque.listIterator();
        if(li2.hasNext()){
        System.out.println(li2.next());
        }
        if(li2.hasPrevious()){
            System.out.println(li2.previous());
        }
        if(li2.hasPrevious()){
            System.out.println(li2.previous());
        }
        if(li2.hasPrevious()){
            System.out.println(li2.previous());
        }
        if(li2.hasPrevious()){
            System.out.println(li2.previous());
        }
        if(li2.hasPrevious()){
            System.out.println(li2.previous());
        }
        if(li2.hasPrevious()){ //выход за пределы списка вывод не произойдет
            System.out.println(li2.previous());
        }
        System.out.println("=================li3.previous()=======li3.next()=====================================================");
        ListIterator li3=deque.listIterator();
        if(li3.hasPrevious()){
        System.out.println(li3.previous());
        }
        if(li3.hasPrevious()){
            System.out.println(li3.previous());
        }if(li3.hasPrevious()){
            System.out.println(li3.previous());
        }
        if(li3.hasNext()) {
            System.out.println(li3.next());
        }
        if(li3.hasNext()) {
            System.out.println(li3.next());
        }
        if(li3.hasNext()) { // //выход за пределы списка вывод не произойдет
            System.out.println(li3.next());
        }
        System.out.println("============li4.next()=======li4.set(222)==========================================================");
        ListIterator li4 = deque.listIterator();
//            if(li4.hasNext()){
//            li4.set(222); // ошибка при изменении значения без вызова метода next()
//        }
        if (li4.hasNext()) {
            li4.next();
        }
        li4.set(222);
        for (Number a : deque
                ) {
            System.out.println(a);
        }

        System.out.println("===============li5.previous()====== li5.set(111)========================================================");
        ListIterator li5 = deque.listIterator();
//        if(li5.hasPrevious()){
//            li5.set(111);   // ошибка при изменении значения без вызова метода next()
//        }
        if (li5.hasPrevious()) {
            System.out.println(li5.previous());
            System.out.println("------------------------------------------------------------------------");
        }
        li5.set(111); //изменение предпоследнего в списке
        for (Number a : deque
                ) {
            System.out.println(a);
        }
        System.out.println("======================li6.next()=========li6.remove()=============================================");
        ListIterator li6 = deque.listIterator();
        if (li6.hasNext()) {
            System.out.println(li6.next());
            System.out.println("------------------------------------------------------------------------");
        }
        li6.remove();
        for (Number a : deque
                ) {
            System.out.println(a);
        }
        System.out.println("===============li7.previous()===== li7.remove()=========================================================");
        ListIterator li7 = deque.listIterator();
        if (li7.hasPrevious()){
            System.out.println(li7.previous());
            System.out.println("------------------------------------------------------------------------");
        }
        li7.remove();
        for (Number a : deque
                ) {
            System.out.println(a);
        }


    }
}
