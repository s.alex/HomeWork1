package ua.org.oa.s.alex.task4_2.part3;



/**
 * Created by A Sosnovskyi on 16.02.2017.
 */
 interface MyDeque<E> extends Iterable<E>, ListIterable<E>{

    /**
     * add element to head
     *
     * @param e E element
     */
    void addFirst(E e);

    /**
     * add last element
     *
     * @param e E element
     */
    void addLast(E e);

    /**
     * First element it was deleting
     * @return E element
     */
    E removeFirst();
    /**
     * Last element it was deleting
     * @return
     */
    E removeLast();
    /**
     * First element
     * @return E element
     */
    E getFirst();
    /**
     * First element
     * @return E element
     */
    E getLast();

    /**
     *
     * @param o Object
     * @return boolean
     */
    boolean contains(Object o);

    /**
     *clear
     */
    void clear();

    /**
     * toArray
     * @return Object[]
     */
    Object[] toArray();

   /**
    * size
    * @return int size
    */
    int size();
   /**
    * containsAll
    * @deque MyDeque  deque
    * @return boolean
    */
    boolean containsAll(MyDeque<? extends E> deque);


}
