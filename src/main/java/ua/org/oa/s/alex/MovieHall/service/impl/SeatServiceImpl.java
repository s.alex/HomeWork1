package ua.org.oa.s.alex.MovieHall.service.impl;

import ua.org.oa.s.alex.MovieHall.dao.DaoFactory;
import ua.org.oa.s.alex.MovieHall.dao.api.Dao;
import ua.org.oa.s.alex.MovieHall.dao.impl.SeanceDaoImpl;

import ua.org.oa.s.alex.MovieHall.dao.impl.SeatDaoImpl;
import ua.org.oa.s.alex.MovieHall.dto.SeatDTO;
import ua.org.oa.s.alex.MovieHall.mapper.BeanMapper;
import ua.org.oa.s.alex.MovieHall.model.Seat;
import ua.org.oa.s.alex.MovieHall.service.api.Service;

import java.util.List;

/**
 * Created by Admin on 21.03.2017.
 */
public class SeatServiceImpl implements Service<Integer, SeatDTO> {
    private static SeatServiceImpl service;
    private Dao<Integer, Seat> seatDao;
    private BeanMapper beanMapper;
    private SeanceDaoImpl seanceDao;
    private SeatDaoImpl seatDaoImpl;

    private SeatServiceImpl() {
        seatDao = DaoFactory.getInstance().getSeatDao();
        beanMapper = BeanMapper.getInstance();
        seatDaoImpl = SeatDaoImpl.getInstance();
    }

    public static synchronized SeatServiceImpl getInstance() {
        if (service == null) {
            service = new SeatServiceImpl();
        }

        return service;
    }


    @Override
    public List<SeatDTO> getAll() {
        List<Seat> seatList = seatDao.getAll();
        List<SeatDTO> seatDTOs = beanMapper.listMapToList(seatList, SeatDTO.class);
        return seatDTOs;
    }

    @Override
    public SeatDTO getById(Integer key) {
        Seat seat = seatDao.getById(key);
        SeatDTO seatDTO = beanMapper.singleMapper(seat, SeatDTO.class);
        return seatDTO;
    }

    @Override
    public void save(SeatDTO entity) {
        Seat seat = beanMapper.singleMapper(entity, Seat.class);
        seatDao.save(seat);
    }

    @Override
    public void delete(Integer key) {
        seatDao.delete(key);
    }

    @Override
    public void update(SeatDTO entity) {
        Seat seat = beanMapper.singleMapper(entity, Seat.class);
        seatDao.update(seat);
    }

    public List<SeatDTO> getAllByHallId(int key) {
        System.out.println("getAllByHallId"+key);
        List<Seat> seatList =seatDaoImpl.readAllByHallId(key);
        List<SeatDTO> seatDTOs = beanMapper.listMapToList(seatList, SeatDTO.class);
        return seatDTOs;
    }
}
