package ua.org.oa.s.alex.MovieHall.dao;

import ua.org.oa.s.alex.MovieHall.dao.api.Dao;
import ua.org.oa.s.alex.MovieHall.dao.impl.*;
import ua.org.oa.s.alex.MovieHall.helper.PropertyHolder;
import ua.org.oa.s.alex.MovieHall.model.*;

/**
 * Created by Admin on 14.03.2017.
 */
public class DaoFactory {
    private static DaoFactory instance=null;
    private Dao<Integer, Movie> movieDao;
    private Dao<Integer, MovieHouse> movieHouseDao;
    private Dao<Integer, Hall> hallDao;
    private Dao<Integer, Seat> seatDao;
    private Dao<Integer, Seance> seanceDao;
    private Dao<Integer, Ticket> ticketDao;
    private Dao<Integer, User> userDao;
    private DaoFactory(){
        loadDaos();
    }



    public static DaoFactory getInstance(){
        if(instance == null){
            instance = new DaoFactory();
        }
        return instance;
    }

    private void loadDaos() {
        if(PropertyHolder.getInstance().isInMemory()){

        }else{
            movieDao = MovieDaoImpl.getInstance();
            movieHouseDao= MovieHouseDaoImpl.getInstance();
            hallDao= HallDaoImpl.getInstance();
            seatDao= SeatDaoImpl.getInstance();
            seanceDao= SeanceDaoImpl.getInstance();
            ticketDao=TicketDaoImpl.getInstance();
            userDao=UserDaoImpl.getInstance();
        }
    }

    public Dao<Integer, Movie> getMovieDao() {
        return movieDao;
    }
    public void setMovieDao(Dao<Integer, Movie> movieDao) {
        this.movieDao = movieDao;
    }

    public Dao<Integer, MovieHouse> getMovieHouseDao() {
        return movieHouseDao;
    }
    public void setMovieHouseDao() {
        this.movieHouseDao=movieHouseDao;
    }

    public Dao<Integer, Hall> getHallDao() {
        return hallDao;
    }
    public void setHallDao() {
        this.hallDao=hallDao;
    }

    public Dao<Integer, Seat> getSeatDao(){return seatDao;}
    public void setSeatDao(){this.seatDao=seatDao;}

    public Dao<Integer, Seance> getSeanceDao(){return seanceDao;}
    public void setSeanceDao(){this.seanceDao=seanceDao;}

    public Dao<Integer, Ticket> getTicketDao(){return ticketDao;}
    public void setTicketDao(){this.ticketDao=ticketDao;}

    public Dao<Integer, User> getUserDao(){return  userDao;}
    public void setUserDao(){this.userDao=userDao;}


}
