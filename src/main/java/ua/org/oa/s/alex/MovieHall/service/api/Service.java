package ua.org.oa.s.alex.MovieHall.service.api;

import java.util.List;

/**
 * Created by Admin on 14.03.2017.
 */
public interface Service<K, T> {
    List<T> getAll();
    T getById(K key);
    void save(T entity);
    void delete(K key);
    void update(T entity);
}
