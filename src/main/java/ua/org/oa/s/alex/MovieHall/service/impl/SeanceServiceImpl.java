package ua.org.oa.s.alex.MovieHall.service.impl;

import ua.org.oa.s.alex.MovieHall.dao.DaoFactory;
import ua.org.oa.s.alex.MovieHall.dao.api.Dao;
import ua.org.oa.s.alex.MovieHall.dto.SeanceDTO;
import ua.org.oa.s.alex.MovieHall.mapper.BeanMapper;
import ua.org.oa.s.alex.MovieHall.model.Seance;
import ua.org.oa.s.alex.MovieHall.service.api.Service;

import java.util.List;

/**
 * Created by Admin on 21.03.2017.
 */
public class SeanceServiceImpl implements Service<Integer, SeanceDTO> {
    private static SeanceServiceImpl service;
    private Dao<Integer, Seance> seanceDao;
    private BeanMapper beanMapper;

    private SeanceServiceImpl() {
        seanceDao = DaoFactory.getInstance().getSeanceDao();
        beanMapper = BeanMapper.getInstance();
    }

    public static synchronized SeanceServiceImpl getInstance() {
        if (service == null) {
            service = new SeanceServiceImpl();
        }

        return service;
    }

    @Override
    public List<SeanceDTO> getAll() {
        List<Seance> seanceList = seanceDao.getAll();
        List<SeanceDTO> seanceDTOs = beanMapper.listMapToList(seanceList, SeanceDTO.class);
        return seanceDTOs;
    }

    @Override
    public SeanceDTO getById(Integer key) {
        Seance seance = seanceDao.getById(key);
        SeanceDTO seanceDTO = beanMapper.singleMapper(seance, SeanceDTO.class);
        return seanceDTO;
    }

    @Override
    public void save(SeanceDTO entity) {
        Seance seance = beanMapper.singleMapper(entity, Seance.class);
        seanceDao.save(seance);
    }

    @Override
    public void delete(Integer key) {
        seanceDao.delete(key);
    }

    @Override
    public void update(SeanceDTO entity) {
        Seance seance = beanMapper.singleMapper(entity, Seance.class);
        seanceDao.update(seance);
    }
}
