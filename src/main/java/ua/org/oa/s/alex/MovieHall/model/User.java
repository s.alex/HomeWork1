package ua.org.oa.s.alex.MovieHall.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;


import java.util.List;

/**
 * Created by A Sosnovsky  on 07.02.2017.
 */
@Getter
@Setter
@EqualsAndHashCode
public class User extends Entity<Integer> {
    private int userId;
    private String userName; //Имя
    private String userSurname; //Фамилия
    private String userLogin; //Login
    private String userPassword; // Password
    private String userEmail; // Email
    private int userDiscount;
    private List<Ticket> purchasedTickets; //купленный билет
    private int permissions;  //права доступа

    public User(String usernName, String userSurname, String userLogin, String userPassword, String userEmail) {

        this.userName = usernName;
        this.userSurname = userSurname;
        this.userLogin = userLogin;
        this.userPassword = userPassword;
        this.userEmail = userEmail;
        this.permissions = 1;
    }


}


