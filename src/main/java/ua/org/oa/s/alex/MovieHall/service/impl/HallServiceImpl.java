package ua.org.oa.s.alex.MovieHall.service.impl;

import ua.org.oa.s.alex.MovieHall.dao.DaoFactory;
import ua.org.oa.s.alex.MovieHall.dao.api.Dao;
import ua.org.oa.s.alex.MovieHall.dao.impl.HallDaoImpl;
import ua.org.oa.s.alex.MovieHall.dao.impl.SeanceDaoImpl;
import ua.org.oa.s.alex.MovieHall.dao.impl.SeatDaoImpl;
import ua.org.oa.s.alex.MovieHall.dto.HallDTO;
import ua.org.oa.s.alex.MovieHall.dto.SeatDTO;
import ua.org.oa.s.alex.MovieHall.mapper.BeanMapper;
import ua.org.oa.s.alex.MovieHall.model.Hall;
import ua.org.oa.s.alex.MovieHall.model.Seance;
import ua.org.oa.s.alex.MovieHall.model.Seat;
import ua.org.oa.s.alex.MovieHall.service.api.Service;

import java.util.List;

/**
 * Created by Admin on 21.03.2017.
 */
public class HallServiceImpl implements Service<Integer, HallDTO> {
    private static HallServiceImpl service;
    private Dao<Integer, Hall> hallDao;
    private HallDaoImpl hallHallDao;
    private BeanMapper beanMapper;

    private HallServiceImpl() {
        hallDao = DaoFactory.getInstance().getHallDao();
        beanMapper = BeanMapper.getInstance();
        hallHallDao = HallDaoImpl.getInstance();
    }

    public static synchronized HallServiceImpl getInstance() {
        if (service == null) {
            service = new HallServiceImpl();
        }

        return service;
    }


    @Override
    public List<HallDTO> getAll() {
        List<Hall> halls = hallDao.getAll();
        for (Hall hall : halls) {
            List<Seat> seatList = SeatDaoImpl.getInstance().readAllByHallId(hall.getId());
            Seat[] seats = (Seat[]) seatList.toArray();
            hall.setSeats(seats);
        }
        List<HallDTO> hallsDTO = beanMapper.listMapToList(halls, HallDTO.class);
        return hallsDTO;
    }

    @Override
    public HallDTO getById(Integer key) {
        Hall hall = hallDao.getById(key);
        HallDTO hallDTO = BeanMapper.singleMapper(hall, HallDTO.class);
        return hallDTO;
    }

    @Override
    public void save(HallDTO entity) {
        Hall hall = beanMapper.singleMapper(entity, Hall.class);
        hallDao.save(hall);
    }

    @Override
    public void delete(Integer key) {
        hallDao.delete(key);
    }

    @Override
    public void update(HallDTO entity) {
        Hall hall = beanMapper.singleMapper(entity, Hall.class);
        hallDao.update(hall);
    }

    public List<Hall> getAllByMovieHouseId(int key) {

        List<Hall> halls = hallHallDao.readAllByMovieHouseId(key);
        SeatDaoImpl setDao = SeatDaoImpl.getInstance();
        SeanceDaoImpl seanceDao = SeanceDaoImpl.getInstance();

        for (Hall hall : halls) {
            List<Seat> seatList = setDao.readAllByHallId(hall.getId());
            List<Seance> seanceList = seanceDao.readAllByHallId(hall.getId());
            int i = 0;
            Seat[] seats = new Seat[hall.getSeatsQuantity()];
            for (Seat seat : seatList) {
                seats[i] = seat;
                i++;
            }

            hall.setSeats(seats);
            hall.setSeances(seanceList);
        }

        //-----------------------------------------------------------------------------------------
        // List<HallDTO> hallsDTO = beanMapper.listMapToList(halls, HallDTO.class);
        return halls;
    }
}
