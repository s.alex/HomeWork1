package ua.org.oa.s.alex.task4_2.part2;


import java.util.Iterator;

/**
 * Created by A Sosnovskyi on 16.02.2017.
 */
class Demo {
    public static void main(String[] args) {
        MyDeque<Number> deque = new MyDequeImpl();
        deque.addFirst(433);
        deque.addFirst(455);
        deque.addFirst(555);
        deque.addLast(8.88);
        deque.addLast(9.99);
        deque.addLast(7.77);

        for (Number element : deque) {
            System.out.println(element);
        }
        System.out.println("============================================================================");
        Iterator iterator = deque.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        System.out.println("=============================================================================");
        Iterator iterator1 = deque.iterator();
        iterator1.next();
        iterator1.remove();
        iterator1.remove();
        Iterator iterator2 = deque.iterator();
        while (iterator2.hasNext()) {
            System.out.println(iterator2.next());
        }
    }
}
