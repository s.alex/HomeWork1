package ua.i.pl.sosnovskyi.homework6;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by A Sosnovskyi on 24.02.2017.
 */
public class HW6 {
    private static String pattern = "(?<book>[\\w\\sА-Яа-я.,:?!-]+);(?<author>[\\w\\sА-Яа-я.,-:]+);(?<year>\\d+)";
    private static String charSet = "utf-8";

    public static void main(String[] args) {
        Set<Book> collection = new HashSet<>();
        String str = loadFromFile("TestHW6.txt", charSet);
        Matcher matcher = getMatcher(pattern, str);
        while (matcher.find()) {
            Book book = new Book(matcher.group("book"), matcher.group("author"), Integer.valueOf(matcher.group("year")));
            collection.add(book);
        }
        for (Book book : collection) {
            System.out.println(book.getTitle() + " " + book.getAuthor() + " " + book.getYear());
        }
        //===========================================================
        //Исскусство программирования на Java", "Г. Шилдт/Д. Холмс", 2005
        // Шаблоны проектирования в Java", "Гранд", 2004
        Book bookToAdd = new Book("Исскусство программирования на Java", "Г. Шилдт/Д. Холмс", 2005);
        if (!collection.contains(bookToAdd)) {
            writeToFile(bookToAdd);
        }


        System.out.println();
        str = loadFromFile("TestHW6.txt", charSet);
        Matcher matcher1 = getMatcher(pattern, str);
        while (matcher1.find()) {
            Book book1 = new Book(matcher1.group("book"), matcher1.group("author"), Integer.valueOf(matcher1.group("year")));
            collection.add(book1);
        }
        for (Book book : collection) {
            System.out.println(book.getTitle() + " " + book.getAuthor() + " " + book.getYear());
        }

    }

    /**
     *
     * @param fileName
     * @param charSet
     * @return
     */
    private static String loadFromFile(String fileName, String charSet) {
        String result = null;
        StringBuilder sb = new StringBuilder();
        try (BufferedReader buf = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(fileName), charSet))) {
            while (true) {
                String tmp = buf.readLine();
                if (tmp == null) {
                    break;
                }
                sb.append(tmp).append("\n");
            }
            return sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     *
     * @param book
     */
    private static void writeToFile(Book book) {

        String tmp = "\n".concat(book.getTitle()).concat(";").concat(book.getAuthor()).concat(";").concat(String.valueOf(book.getYear()));
        try (BufferedWriter br = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("TestHW6.txt", true)))) {
            br.write(tmp);
            br.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param book
     */
    private static void serializableBook(Book book) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("Storage.txt"))) {
            oos.writeObject(book);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @return
     */
    private static Book deSerializableBook() {
        Book result = null;
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("Storage.txt"))) {
            try {
                result = (Book) ois.readObject();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     *
     * @param book
     * @return
     */
    private static String toJson(Book book) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(book);
    }

    /**
     *
     * @param rawData
     * @return
     */
    private static Book fromJson(String rawData) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.fromJson(rawData, Book.class);
    }

    /**
     *
     * @param pattern String
     * @param str String
     * @return Matcher
     */
    private static Matcher getMatcher(String pattern, String str) {
        return Pattern.compile(pattern).matcher(str);
    }
}
