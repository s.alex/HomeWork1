package ua.i.pl.sosnovskyi.homework6;

import java.io.*;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by A Sosnovskyi on 24.02.2017.
 */
public class FileManager {

    private String homePath = "D:\\Documents\\Java_Projects\\HomeworkI\\src\\main\\resources";
    private Set<String> fileName;
    private File fileDir;
    private File newFile;

    /**
     * конструктор
     */
    public FileManager() {
        this.fileDir = new File(homePath);
        String[] files = getFiles(fileDir);

    }

    /**
     * setHomePath
     * Устанавливает домашний каталог
     * @param homePath String выбранный каталог(домашний каталог)
     */
    public void setHomePath(String homePath) {
        this.fileDir = new File(homePath);
        System.out.println("Directory: ".concat(homePath));
        getFiles(fileDir);
    }

    /**
     * getFiles
     * Возвращает массив имен файлов и каталогов в выбранном каталоге и выводит их на экран
     * @param fileDir File выбранный каталог(домашний каталог)
     * @return String[] массив имен файлов
     */
    public String[] getFiles(File fileDir) {
        String[] files = null;
        if (fileDir.isDirectory()) {
            files = fileDir.list();
        }
        System.out.println("Directory: ".concat(homePath));
        for (String file : files) {
            System.out.println(file);
        }
        return files;
    }
    //==========================================================================
//    private static void exists(String fileName) throws FileNotFoundException {
//        File file = new File(fileName);
//        if (!file.exists()){
//            throw new FileNotFoundException(file.getName());
//        }
//    }
    //==============================================================================

    /**
     * run
     * Запускает файлловый редактор
     * проверяет флаг завершения программы
     * принимает команды пользователя и имена файлов
     */

    public void run() {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            int opertation = 1;
            while (opertation == 1) {
                String str = br.readLine();
                opertation = runProsess(str.split(" "));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //============================================================================

//    public static void delete(String nameFile) throws FileNotFoundException {
//
//       File f= new File(nameFile);
//        if(f.exists()){
//            f.delete();
//        }else{
//            throw new FileNotFoundException("А такого файла и нет!!!");
//        }
//    }


//====================================================================================

    /**
     * runProsess обрабатывает данные введенные пользователем
     * @param args String... команда(из списка) пробел имя файла(?) пробел новое имя файла(?)
     * @return int флаг для определения завершения программы 0-завершить
     *
     *  "create"-создает файл в домашней директории
     *  "delete"-удаляет файл
     *  "rename"-переименовывает файл
     *  "dir"-изменяет директорию (проверку на корректность пути не делал)
     *  "quit"-выход из программы
     *  "help"-помощь
     */

    private int runProsess(String... args) {

        switch (args[0]) {
            case "create": {
                System.out.println("create " + args[0] + " " + args[1]);
                newFile = new File(homePath, args[1]);
                try {
                    newFile.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (newFile.exists()) {
                    System.out.println("Done");
                } else {
                    System.out.println("Что-то не работает");
                }
                getFiles(fileDir);
                return 1;
            }
            case "delete": {

                File fileToDelete = new File(fileDir, args[1]);
                fileToDelete.delete();
                //   System.out.println("delete "+args[0]+" "+args[1]);
                System.out.println("Done");
                getFiles(fileDir);
                return 1;
            }
            case "rename": {
                System.out.println("rename " + args[0] + " " + args[1] + " " + args[2]);
                File oldName = new File(fileDir, args[1]), newName = new File(fileDir, args[2]);
                oldName.renameTo(newName);
                getFiles(fileDir);
                return 1;
            }
            case "dir": {
                System.out.println("dir " + args[0] + " " + args[1]);
                setHomePath(args[1]);
                return 1;
            }
            case "quit": {

                return 0;
            }
            case "help": {
                System.out.println("/**\n" +
                        " * Created by A Sosnovskyi on 24.02.2017.\n" +
                        " * Запускает файлловый редактор\n" +
                        " *  \"create\"-создает файл в домашней директории\n" +
                        " *  \"delete\"-удаляет файл\n" +
                        " *  \"rename\"-переименовывает файл\n" +
                        " *  \"dir\"-изменяет директорию (проверку на корректность пути не делал)\n" +
                        " *  \"quit\"-выход из программы\n" +
                        " */");
                return 1;
            }
            default: {
                System.out.println("Не понял что нужно делать");
                System.out.println("Проверьте имя команды!!!");
                return 1;
            }
        }

    }


}
