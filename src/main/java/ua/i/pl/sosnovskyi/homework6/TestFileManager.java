package ua.i.pl.sosnovskyi.homework6;

/**
 * Created by A Sosnovskyi on 24.02.2017.
 * Запускает файлловый редактор
 *  "create"-создает файл в домашней директории
 *  "delete"-удаляет файл
 *  "rename"-переименовывает файл
 *  "dir"-изменяет директорию (проверку на корректность пути не делал)
 *  "quit"-выход из программы
 *  "help"-помощь
 */
public class TestFileManager {
    public static void main(String[] args) {
        FileManager fileManager=new FileManager();
        fileManager.run();
    }
}
