package ua.i.pl.sosnovskyi.homework5;

import java.util.Map;

/**
 * Created by A Sosnovskyi on 20.02.2017.
 */
public interface Translateble {
    void loadDictionaries();

    String translate(String path, Map<String, String> dictionary);

}
