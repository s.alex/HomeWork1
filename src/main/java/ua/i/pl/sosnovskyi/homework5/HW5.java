package ua.i.pl.sosnovskyi.homework5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by A Sosnovskyi on 21.02.2017.
 */
public class HW5 {
    public static void main(String[] args) {
        Translator translator = new Translator();
        translator.loadDictionaries();
        System.out.println("Укажите файл для перевода:");
        System.out.println("Наберите полный путь к файлу, название файла и нажмите \"Ввод\":");
        BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
        String path = null;
        try {
            path = buf.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        translator.setFilepath(path);
       // System.out.println(path);
        System.out.println();
        System.out.println("Выберите направление перевода:");
        System.out.println();
        for (int count = 0; count < translator.availableDictionaries.length; count++) {
            System.out.println("Нажмите " + (count + 1) + " для перевода " +
                    translator.availableDictionaries[count].substring(0, translator.availableDictionaries[count].indexOf('.')));
        }
        System.out.println();
        System.out.println("Сделайте свой выбор и нажмите \"Ввод\":");
        System.out.println();
        int choise = -1;
        try {
            while (choise < 1 || choise > translator.availableDictionaries.length) {
                choise = Integer.parseInt(buf.readLine());
                translator.setDictionary(choise);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(translator.translate(translator.filePath, translator.dictionaryToUse));
    }
}
