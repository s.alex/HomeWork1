package ua.i.pl.sosnovskyi.homework5;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by A Sosnovskyi on 20.02.2017.
 * В данной реализации все словари загружаются в Map<String, Map<String, String>>
 *     перед испольлзоавнием методом translator.loadDictionaries() после создания объекта
 *     класса Translator.
 */
public class Translator implements Translateble {

    private Map<String, Map<String, String>> dictionaries = new HashMap<>();
    private String pattern = "(?<key>[а-яА-Я .,ё!\\w\\(\\)-]+);-;(?<value>[а-яА-Я .,ё!\\w\\(\\)-]+)";
    private String charSet = "windows-1251";
    private String dictionaryPath = "D:\\Documents\\Java_Projects\\HomeworkI\\src\\main\\resources\\dictionaries"; //\src\main\resources\dictionaries
    public String[] availableDictionaries = null;
    public Map<String, String> dictionaryToUse = new HashMap<>();
    public String filePath;

    /**
     *setCharSet метод устанавливает кодировку для считываемых файлов отличную от кодировки по умолчанию
     * @param charSet String кодировка
     */
    public void setCharSet(String charSet) {
        if(charSet!=null||charSet.length()!=0){
            this.charSet = charSet;}

    }

    /**
     * setDictionary устанавливает словарь для перевода
     * @param choise int выбранное направление перевода.
     *               значение, которое ввел пользователь
     */
    public void setDictionary(int choise) {
        dictionaryToUse = dictionaries.get(availableDictionaries[choise - 1]);
        // //-----------------------------test---------------------------------------------
        // System.out.println("availableDictionaries[choise]=" + availableDictionaries[choise - 1]);
//        for (Map.Entry<String, Map<String, String>> entry : dictionaries.entrySet()) {
//            System.out.println(entry.getKey() + " " + entry.getValue());
//        }
        //   //--------------------------------------------------------------------------

        // //-------------------------test-----------------------------------------------
        // System.out.println(dictionaryToUse);
//        for (Map.Entry<String, String> entry : dictionaryToUse.entrySet()) {
//            System.out.println(entry.getKey() + " " + entry.getValue());
//        }
    }

    /**
     * setFilepath устанавливает полный путь к файлу
     *
     * @param filepath String полный путь к файлу(название файла если файл
     *                 в корневом каталоге)
     */
    public void setFilepath(String filepath) {
        this.filePath = filepath;
    }

    /**
     * Возвращает строку с переведенным текстом
     * @param path String строка для перевода
     * @param dictionary Map<String, String> выбранный словарь
     * @return String переведенная строка
     */
    @Override
    public String translate(String path, Map<String, String> dictionary) {
        String strPattern = "[\\wА-Яа-я]+";
        String fileToTranslate = loadFromFile(filePath, charSet);
        ////----------------test вывод оригинала текста---------------------------------------------
        // System.out.println(fileToTranslate);
        ////------------------------------------------------------------------
        Matcher matcher = getMatcher(strPattern, fileToTranslate);
        Set<String> setRawWords = new HashSet<>();
        while (matcher.find()) {
            setRawWords.add(matcher.group());
        }
        //      //-----------------------test--------------------------------------------
//        for(Map.Entry<String, String> entry:dictionary.entrySet()){
//            System.out.println(entry.getKey()+" "+ entry.getValue());
//        }
        //    //--------------------------------------------------------------------
        for (String wordToBeTranslated : setRawWords) {
            String tmpLower = dictionary.get(wordToBeTranslated);
            String tmpUpper=dictionary.get(wordToBeTranslated.toLowerCase());
            if (tmpLower != null) {
                fileToTranslate = fileToTranslate.replaceAll("\\b".concat(wordToBeTranslated).concat("\\b"), tmpLower);

            }
            if (tmpUpper != null) {
                tmpUpper=tmpUpper.toUpperCase();
                tmpUpper=tmpUpper.substring(0,1).concat(tmpUpper.substring(1).toLowerCase());
                fileToTranslate = fileToTranslate.replaceAll("\\b".concat(wordToBeTranslated).concat("\\b"), tmpUpper);

            }
            // //-------------------test--------------------------------------------
            // System.out.println(wordToBeTranslated);
            // System.out.println(dictionary.containsKey(wordToBeTranslated));
            // //--------------------------------------------------------------------

            // //--------------------test--------------------------------------------
            //System.out.println(tmp);
            // //-------------------------------------------------------------------
        }
        return fileToTranslate;
    }

    /**
     * loadDictionaries загружает доступные словари в память
     */
    @Override
    public void loadDictionaries() {
        File dir = new File(dictionaryPath);
        if (dir.isDirectory()) {
            availableDictionaries = dir.list();
        }
        if (availableDictionaries.length != 0) {
            for (int i = 0; i < availableDictionaries.length; i++) {
                dictionaries.put(availableDictionaries[i], loadDictionary(dir.getAbsolutePath().concat("\\").concat(availableDictionaries[i]), charSet));
            }
        }
//-------------------------------------test--------------------------------------------
//        for (Map.Entry<String, Map<String, String>> entry : dictionaries.entrySet()) {
//            System.out.println(entry.getKey()+" "+entry.getValue());
//        }
// ----------------------------------------------------------------------------------
    }

     /**
     * loadFromFile считывает файл с диска и возвращает в виде строки
     *
     * @param fileName String имя файла(полный путь к файлу)
     * @param charSet  String кодировка для считывания
     * @return String строка с данными из файла
     */
    private String loadFromFile(String fileName, String charSet) {
        String result = null;
        StringBuilder sb = new StringBuilder();
        try (BufferedReader buf = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), charSet))) {
            while (true) {
                String tmp = buf.readLine();
                if (tmp == null) {
                    break;
                }
                sb.append(tmp).append("\n");
            }
            return sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * getMatcher
     * @param pattern String
     * @param str String
     * @return Matcher
     */
    private Matcher getMatcher(String pattern, String str) {
        return Pattern.compile(pattern).matcher(str);
    }

    /**
     * loadDictionary активирует словарь для текущего перевода текста
     *
     * @param fileName String имя файла(полный путь к файлу)
     * @param charSet  String кодировка для считывания
     * @return dictionary Map<String, String> словарь в виде карты
     */
    private Map<String, String> loadDictionary(String fileName, String charSet) {
        String tmp = loadFromFile(fileName, charSet);
        Matcher matcher = getMatcher(pattern, tmp);
        Map<String, String> dictionary = new HashMap<>();
        while (matcher.find()) {
            dictionary.put(matcher.group("key"), matcher.group("value"));
        }
        return dictionary;
    }
}
