package ua.i.pl.sosnovskyi.homework2;


import java.util.List;

/**
 * Created by Admin on 05.02.2017.
 */
public class HW2Parser {
    public static void main(String[] args) {
        long timeStart1=System.nanoTime();
        List<String> list1=MarkdownParserImpl.parse("example1.txt", "UTF-8");
        long timeStop1=System.nanoTime();

        for(String html: list1){
            System.out.println(html);
        }
        System.out.println();
        System.out.println();
        System.out.println();
        long timeStart=System.nanoTime();
        List<String> list=MarkdownParser.parse("example1.txt", "UTF-8");
        long timeStop=System.nanoTime();


        for(String html: list){
            System.out.println(html);
        }
        System.out.println("MarkdownParserImpl time="+(timeStop1-timeStart1));
        System.out.println("MarkdownParser time="+(timeStop-timeStart));
        System.out.println("MarkdownParser time-MarkdownParserImpl time="+((timeStop - timeStart) - (timeStop1 - timeStart1)));
    }
}
