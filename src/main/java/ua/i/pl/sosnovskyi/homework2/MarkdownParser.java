package ua.i.pl.sosnovskyi.homework2;



import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by A Sosnovskyi on 04.02.2017.
 */
public class MarkdownParser {
    /**
     * парсит текст файла в HTML использует отдельные паттерны для поиска вхождений
     * @param fileName String имя файла с текстом для парсинга
     * @param charSet  String кодировка
     * @return List<String> listOutputData
     */
    static List<String> parse(String fileName, String charSet) {


        // ^#[^#]+ парсер для заголовков
        List<String> listInputData = dataSplitter(fileName, charSet);
        List<String> listOutputData = new ArrayList<>();
        listOutputData.add("<html>");
        listOutputData.add("<body>");
        for (String data : listInputData) {
            data = convertHeaders(data);
            //----------------------------------------------------------
            data = convertLinks(data);
            data = convertEmTags(data);
            data = convertStrongTags(data);
            //------------------------------------------------------------
            listOutputData.add(data);
        }
        listOutputData.add("</body>");
        listOutputData.add("</html>");
        return listOutputData;
    }


//===========================================================================

    /**
     * @param data String of row Data
     * @return result String of converted Headers and paragraph
     */
    static String convertHeaders(String data) {
        String result;
        int dataLength = data.length();
        if (dataLength == 0) {
            throw new NullPointerException("Empty String in convertHeaders method");
        }
        if (data.charAt(0) != '#') {
            String tmp = data.substring(0, dataLength - 1);
            tmp = tmp.concat("</p>");
            result = "<p>".concat(tmp);
        } else {
            int counter = 1;
            for (int index = 1; index < 7; index++) {
                if (data.charAt(index) == '#') {
                    counter++;
                }
            }

            result = String.format("<h%1$s>%2$s</h%1$s>", counter, data.substring(counter, dataLength - 1)); //fixed
//            System.out.println(result);
//            switch (counter) {
//                case 1: {
//                    String tmp = data.substring(1, dataLength - 1);
//                    tmp = tmp.concat("</h1>");
//                    result = "<h1>".concat(tmp);
//                    break;
//                }
//                case 2: {
//                    String tmp = data.substring(2, dataLength - 1);
//                    tmp = tmp.concat("</h2>");
//                    result = "<h2>".concat(tmp);
//                    break;
//                }
//                case 3: {
//                    String tmp = data.substring(3, dataLength - 1);
//                    tmp = tmp.concat("</h3>");
//                    result = "<h3>".concat(tmp);
//                    break;
//                }
//                case 4: {
//                    String tmp = data.substring(4, dataLength - 1);
//                    tmp = tmp.concat("</h4>");
//                    result = "<h4>".concat(tmp);
//                    break;
//                }
//                case 5: {
//                    String tmp = data.substring(5, dataLength - 1);
//                    tmp = tmp.concat("</h5>");
//                    result = "<h5>".concat(tmp);
//                    break;
//                }
//                case 6: {
//                    String tmp = data.substring(6, dataLength - 1);
//                    tmp = tmp.concat("</h6>");
//                    result = "<h6>".concat(tmp);
//                    break;
//                }
//                case 7: {
//                    String tmp = data.substring(7, dataLength - 1);
//                    tmp = tmp.concat("</h7>");
//                    result = "<h7>".concat(tmp);
//                    break;
//                }
//            }
        }
        return result;
    }


    /**
     * @param data String row data
     * @return result String with Links
     */
    static String convertLinks(String data) {

        // (<anchor> \[[^#\[\]]+\])(?<href>\([^#\(\)]+\)) парсер для ссылок
        String result = data;
        int dataLength = data.length();
        if (dataLength == 0) {
            throw new NullPointerException("Empty String in convertHeaders method");
        }
        Pattern pattern = Pattern.compile("(?<anchor> \\[[^#\\[\\]]+\\])(?<href>\\([^#\\(\\)]+\\))");
        Matcher matcher = pattern.matcher(data);

        while (matcher.find()) {
            int matchGroupStartAnchor;
            int matchGroupEndAnchor;
            int matchGroupStartHref;
            int matchGroupEndHref;
            if (matcher.group("anchor") != null) {
                matchGroupStartAnchor = matcher.start("anchor");
                matchGroupEndAnchor = matcher.end("anchor");
                matchGroupStartHref = matcher.start("href");
                matchGroupEndHref = matcher.end("href");
                String tmpStr1 = data.substring(0, matchGroupStartAnchor + 1);
                String tmpStr2 = data.substring(matchGroupStartAnchor + 2, matchGroupEndAnchor - 1);
                String tmpStr3 = data.substring(matchGroupStartHref + 1, matchGroupEndHref - 1);
                String tmpStr4 = data.substring(matchGroupEndHref, dataLength);
                String modyfy = "<a href=\"".concat(tmpStr3).concat("\">").concat(tmpStr2).concat("</a>");
                result = tmpStr1.concat(modyfy).concat(tmpStr4);
                result = convertLinks(result);
            }
        }
        return result;
    }
//=========================================================================================

    /**
     * @param data String row data
     * @return result String whith Em tagth
     */
    private static String convertEmTags(String data) {
        //(?<em> \*{2}[^#*]+\*{2})|(?<strong> \*{3}[^#*]+\*{3}) парсер для strong и em
        String result = data;
        int dataLength = data.length();
        if (dataLength == 0) {
            throw new NullPointerException("Empty String in convertHeaders method");
        }
        Pattern pattern = Pattern.compile("(?<em> \\*{2}[^#*]+\\*{2})");
        Matcher matcher = pattern.matcher(data);
        while (matcher.find()) {
            int matchGroupStartEm;
            int matchGroupEndEm;

            if (matcher.group("em") != null) {
                matchGroupStartEm = matcher.start("em");
                matchGroupEndEm = matcher.end("em");
                String tmp1 = data.substring(0, matchGroupStartEm + 1);
                String tmp2 = data.substring(matchGroupStartEm, matchGroupEndEm);
                String tmp3 = data.substring(matchGroupEndEm, dataLength);
                String tmp2Modify = "<em>".concat(tmp2.substring(3, matchGroupEndEm - matchGroupStartEm - 2));
                result = tmp1.concat(tmp2Modify).concat("</em>").concat(tmp3);
                result = convertEmTags(result);

            }


        }
        return result;
    }

    /**
     * @param data String row data
     * @return result String whith Strong tagth
     */
    private static String convertStrongTags(String data) {
        //(?<em> \*{2}[^#*]+\*{2})|(?<strong> \*{3}[^#*]+\*{3}) парсер для strong и em
        String result = data;
        int dataLength = data.length();
        if (dataLength == 0) {
            throw new NullPointerException("Empty String in convertHeaders method");
        }
        Pattern pattern = Pattern.compile("(?<strong> \\*{3}[^#*]+\\*{3})");
        Matcher matcher = pattern.matcher(data);
        while (matcher.find()) {
            int matchGroupStart;
            int matchGroupEnd;

            if (matcher.group("strong") != null) {

                matchGroupStart = matcher.start("strong");
                matchGroupEnd = matcher.end("strong");
                String tmp1 = data.substring(0, matchGroupStart + 1);
                String tmp2 = data.substring(matchGroupStart, matchGroupEnd);
                String tmp3 = data.substring(matchGroupEnd, dataLength);
                String tmp2Modify = "<strong>".concat(tmp2.substring(4, matchGroupEnd - matchGroupStart - 3));
                result = tmp1.concat(tmp2Modify).concat("</strong>").concat(tmp3);
                result = convertStrongTags(result);
            }
        }
        return result;
    }
//---------------------------------------------------------------------------------------------

    /**
     * @param fileName String имя файла с текстом для парсинга
     * @param charSet  String кодировка
     * @return String данные из файла
     */
    static String loadFromFile(String fileName, String charSet) {
        if (fileName == null | fileName.length() == 0) {
            throw new IllegalArgumentException("Неверное имя файла");
        }
        StringBuilder stringBuilder = new StringBuilder();

        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), charSet))) {

            while (true) {
                String tmp = bufferedReader.readLine();

                if (tmp == null) {
                    break;
                } else {
                    stringBuilder.append(tmp).append("\n");

                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }

    /**
     * @param fileName String имя файла с текстом для парсинга
     * @param charSet  String кодировка
     * @return List<String> данные из файла
     */
     static List<String> dataSplitter(String fileName, String charSet) {
        //[\S\s\w]+?\n(?=[A-Z#])|[\S\s\w]+$   регулярное для сплитера
        List<String> list = new ArrayList<>();
        String data = loadFromFile(fileName, charSet);
        Pattern pattern = Pattern.compile("[\\S\\s\\w]+?\\n(?=[A-Z#])|[\\S\\s\\w]+$");
        Matcher matcher = pattern.matcher(data);
        while (matcher.find()) {
            //  System.out.println(matcher.group());
            list.add(matcher.group());
        }
        return list;
    }
}
