package ua.i.pl.sosnovskyi.homework2;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by A Sosnovskyi on 08.02.2017.
 */
public class MarkdownParserImpl extends MarkdownParser {

    /**
     * парсит текст файла в HTML использует общий паттерн для поиска вхождений
     * @param fileName String имя файла с текстом для парсинга
     * @param charSet  String кодировка
     * @return List<String> listOutputData
     */
    static List<String> parse(String fileName, String charSet) {
        //(?<headers>^#[^#]+)|(?<anchor> \[[^#\[\]]+\])(?<href>\([^#\(\)]+\))|(?<em> \*{2}[^#*]+\*{2})|(?<strong> \*{3}[^#*]+\*{3})|(?<paragraph>^[A-Z0-9][^#]+)
        List<String> listInputData = dataSplitter(fileName, charSet);
        List<String> listOutputData = new ArrayList<>();
        listOutputData.add("<html>");
        listOutputData.add("<body>");
        Pattern pattern = Pattern.compile("(?<anchor> \\[[^#\\[\\]]+\\])(?<href>\\([^#\\(\\)]+\\))|" +
                "(?<em> \\*{2}[^#*]+\\*{2})|" +
                "(?<strong> \\*{3}[^#*]+\\*{3})");

        for (String data : listInputData) {
            Matcher matcher = pattern.matcher(data);
            while (matcher.find()) {
                if (matcher.group("em") != null) {

                    data = convertTags(data, matcher.group("em"), 2, " <em>", "</em>");
                }
                if (matcher.group("strong") != null) {
                    data = convertTags(data, matcher.group("strong"), 3, " <strong>", "</strong>");
                }
                if (matcher.group("anchor") != null) {
                    data = linkBuilder(data, matcher.group("anchor"), matcher.group("href"));

                 //   data= convertLinks(data);  // с использованием другой реализации
                }


            }
            data = convertHeaders(data);
            listOutputData.add(data);
        }
        listOutputData.add("</body>");
        listOutputData.add("</html>");
        return listOutputData;
    }

    /**
     * Возвращает конвертировинные ссылки
     * @param data String row data
     * @param anchor String
     * @param href String
     * @return String
     */
    private static String linkBuilder(String data, String anchor, String href) {
       // String result;
//        String tmpAnchor = anchor.substring(2, anchor.length() - 1);
//        String tmpHref = href.substring(1, href.length() - 1);
      //  String tmpLink = " <a href=\"".concat(tmpHref).concat("\">").concat(tmpAnchor).concat("</a>");
      //  result = data.replace(anchor.concat(href), tmpLink);
//        return result;
        //ниже тоже самое но в одну строку
        return data.replace(anchor.concat(href), " <a href=\"".concat(href.substring(1, href.length() - 1)).concat("\">").concat(anchor.substring(2, anchor.length() - 1)).concat("</a>"));
    }

    /**
     * Возвращает конвертированные теги Ем и Стронг
     * @param data String row data
     * @return result String whith  tagth
     */
    private static String convertTags(String data, String replacement, int n, String tagStart, String tagEnd) {
        String result;
        int replacementString = replacement.length();
        String tmp = replacement.substring(n + 1, replacementString - n);
        tmp = tagStart.concat(tmp).concat(tagEnd);
        result = data.replace(replacement, tmp);
        return result;
    }

//=================================================================================================================
    /**
     *  возвращает конвертированные ссылки другая реализация
     * @param data String row data
     * @return result String with Links
     */
//    private static String convertLinks(String data) {
//
//        // (<anchor> \[[^#\[\]]+\])(?<href>\([^#\(\)]+\)) парсер для ссылок
//        String result = data;
//        int dataLength = data.length();
//        if (dataLength == 0) {
//            throw new NullPointerException("Empty String in convertHeaders method");
//        }
//        Pattern pattern = Pattern.compile("(?<anchor> \\[[^#\\[\\]]+\\])(?<href>\\([^#\\(\\)]+\\))");
//        Matcher matcher = pattern.matcher(data);
//
//        while (matcher.find()) {
//            int matchGroupStartAnchor;
//            int matchGroupEndAnchor;
//            int matchGroupStartHref;
//            int matchGroupEndHref;
//            if (matcher.group("anchor") != null) {
//                matchGroupStartAnchor = matcher.start("anchor");
//                matchGroupEndAnchor = matcher.end("anchor");
//                matchGroupStartHref = matcher.start("href");
//                matchGroupEndHref = matcher.end("href");
//                String tmpStr1 = data.substring(0, matchGroupStartAnchor + 1);
//                String tmpStr2 = data.substring(matchGroupStartAnchor + 2, matchGroupEndAnchor - 1);
//                String tmpStr3 = data.substring(matchGroupStartHref + 1, matchGroupEndHref - 1);
//                String tmpStr4 = data.substring(matchGroupEndHref, dataLength);
//                String modyfy = "<a href=\"".concat(tmpStr3).concat("\">").concat(tmpStr2).concat("</a>");
//                result = tmpStr1.concat(modyfy).concat(tmpStr4);
//                result = convertLinks(result);
//            }
//        }
//        return result;
//    }

    /**
     * Конвертирует заголовки и абзацы
     * @param data String of row Data
     * @return result String of converted Headers and paragraph
     */
//    private static String convertHeaders(String data) {
//        String result;
//        int dataLength = data.length();
//        if (dataLength == 0) {
//            throw new NullPointerException("Empty String in convertHeaders method");
//        }
//        if (data.charAt(0) != '#') {
//            String tmp = data.substring(0, dataLength - 1);
//            tmp = tmp.concat("</p>");
//            result = "<p>".concat(tmp);
//        } else {
//            int counter = 1;
//            for (int index = 1; index < 7; index++) {
//                if (data.charAt(index) == '#') {
//                    counter++;
//                }
//            }
//
//            result = String.format("<h%1$s>%2$s</h%1$s>", counter, data.substring(counter, dataLength - 1)); //fixed
////            System.out.println(result);
////            switch (counter) {
////                case 1: {
////                    String tmp = data.substring(1, dataLength - 1);
////                    tmp = tmp.concat("</h1>");
////                    result = "<h1>".concat(tmp);
////                    break;
////                }
////                case 2: {
////                    String tmp = data.substring(2, dataLength - 1);
////                    tmp = tmp.concat("</h2>");
////                    result = "<h2>".concat(tmp);
////                    break;
////                }
////                case 3: {
////                    String tmp = data.substring(3, dataLength - 1);
////                    tmp = tmp.concat("</h3>");
////                    result = "<h3>".concat(tmp);
////                    break;
////                }
////                case 4: {
////                    String tmp = data.substring(4, dataLength - 1);
////                    tmp = tmp.concat("</h4>");
////                    result = "<h4>".concat(tmp);
////                    break;
////                }
////                case 5: {
////                    String tmp = data.substring(5, dataLength - 1);
////                    tmp = tmp.concat("</h5>");
////                    result = "<h5>".concat(tmp);
////                    break;
////                }
////                case 6: {
////                    String tmp = data.substring(6, dataLength - 1);
////                    tmp = tmp.concat("</h6>");
////                    result = "<h6>".concat(tmp);
////                    break;
////                }
////                case 7: {
////                    String tmp = data.substring(7, dataLength - 1);
////                    tmp = tmp.concat("</h7>");
////                    result = "<h7>".concat(tmp);
////                    break;
////                }
////            }
//        }
//        return result;
//    }

    /**
     * читает данные из файла
     * @param fileName String имя файла с текстом для парсинга
     * @param charSet  String кодировка
     * @return String данные из файла
     */
//    private static String loadFromFile(String fileName, String charSet) {
//        if (fileName == null | fileName.length() == 0) {
//            throw new IllegalArgumentException("Неверное имя файла");
//        }
//        StringBuilder stringBuilder = new StringBuilder();
//
//        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), charSet))) {
//
//            while (true) {
//                String tmp = bufferedReader.readLine();
//
//                if (tmp == null) {
//                    break;
//                } else {
//                    stringBuilder.append(tmp).append("\n");
//
//                }
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return stringBuilder.toString();
//    }

    /**
     * разбивает на строки
     * @param fileName String имя файла с текстом для парсинга
     * @param charSet  String кодировка
     * @return List<String> данные из файла
     */
//    private static List<String> dataSplitter(String fileName, String charSet) {
//        //[\S\s\w]+?\n(?=[A-Z#])|[\S\s\w]+$   регулярное для сплитера
//        List<String> list = new ArrayList<>();
//        String data = loadFromFile(fileName, charSet);
//        Pattern pattern = Pattern.compile("[\\S\\s\\w]+?\\n(?=[A-Z#])|[\\S\\s\\w]+$");
//        Matcher matcher = pattern.matcher(data);
//        while (matcher.find()) {
//            //  System.out.println(matcher.group());
//            list.add(matcher.group());
//        }
//        return list;
//    }


}
