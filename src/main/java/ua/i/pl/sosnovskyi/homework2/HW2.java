package ua.i.pl.sosnovskyi.homework2;

import java.util.Arrays;

/**
 * Created by A Sosnovskyi on 30.01.2017.
 */
public class HW2 {
    public static void main(String[] args) {
        String str1 = "А роза упала на лапу Азора";
        String str10="Каждый веб-разработчик знает, что такое текст-«рыба».";
        StringUtils str2 = new StringUtils("String");
        StringUtils str3 = new StringUtils("Stringnfos");
        StringUtils str4 = new StringUtils("aaabbbccccc");
        StringUtils str5 = new StringUtils("Danny Doo,  Flat no 502,  Big Apartment,  Wide Road, Near Huge Hilestone,   "
                + "Hugo-city 56010,  Ph:  +8(765)-432-10,  Email:  danny@myworld.com.   "
                + "Haggi Hyer,  Post bag no 52,  Big bank post office,   "
                + "Big bank city 56000,  ph:  +3(765)-012-34,  Email: maggi07@myuniverse.com.");
        StringUtils str6=new StringUtils("02.02.2017");
        StringUtils str7=new StringUtils("Каждый веб-разработчик знает, что такое текст-«рыба». Текст этот, несмотря на название," +
                " не имеет никакого отношения к обитателям водоемов. Используется он веб-дизайнерами для вставки на интернет-страницы и демонстрации" +
                " внешнего вида контента, просмотра шрифтов, абзацев, отступов и т.д. Так как цель применения такого текста исключительно" +
                " демонстрационная, то и смысловую нагрузку ему нести совсем необязательно. Более того, нечитабельность текста сыграет на " +
                "руку при оценке качества восприятия макета.\n" +

                "Самым известным «рыбным» текстом является знаменитый Lorem ipsum. Считается, что впервые его применили в книгопечатании" +
                " еще в XVI веке. Своим появлением Lorem ipsum обязан древнеримскому философу Цицерону, ведь именно из его трактата" +
                " «О пределах добра и зла» средневековый книгопечатник вырвал отдельные фразы и слова, получив текст-«рыбу», широко " +
                "используемый и по сей день. Конечно, возникают некоторые вопросы, связанные с использованием Lorem ipsum на сайтах и" +
                " проектах, ориентированных на кириллический контент – написание символов на латыни и на кириллице значительно различается.\n" +
                "\n" +
                "И даже с языками, использующими латинский алфавит, могут возникнуть небольшие проблемы: в различных языках те или иные " +
                "буквы встречаются с разной частотой, имеется разница в длине наиболее распространенных слов. Отсюда напрашивается вывод, " +
                "что все же лучше использовать в качестве «рыбы» текст на том языке, который планируется использовать при запуске проекта. " +
                "Сегодня существует несколько вариантов Lorem ipsum, кроме того, есть специальные генераторы, создающие собственные варианты " +
                "текста на основе оригинального трактата, благодаря чему появляется возможность получить более длинный неповторяющийся набор слов.");

        StringUtils str8=new StringUtils("The main purpose of my invention is to create a model of the well\n" +
                "known instrument 76.5 in most countries, violin. It looks very alike, yet creating it is far cheaper than a real violin. It can be used as a great piece of creativedecor. It’s very simple to use. Just a decorative piece with no more than one\n" +
                "type of mechanisms. Bright purple and white pieces of paper and cardboard, the 3D shape, and pieces of glittery tape on nails for tuning the bright purple strings make the violin brighter, but still a lot alike with my example, my own\n" +
                "violin. Even the size is real, since violins come in many sizes. The rubber bands are the strings. You cannot bow the notes, though. As\n" +
                "a decorative piece, it uses bright purple rubber bands for strings that you can\n" +
                "even technically tune with the “pegs” and “fine tuners” made out of nails.\n" +
                "\n" +
                "My violin, as well as a piece of decor, can be used as a teaching model\n" +
                "for the parts of a violin, since it has most of them. The only things it doesn’t\n" +
                "have are a chinrest, a bridge, and the wonderful sound of the real, yet far\n" +
                "more expensive violin.");
        System.out.println(new StringUtils(str1).polindromString());

        System.out.println(str2.polindromString());
        System.out.println();
        System.out.println();
        System.out.println(new StringUtils(str1).modifyString());
        System.out.println(str2.modifyString());
        System.out.println(str3.modifyString());
        System.out.println();
        System.out.println();
        System.out.println(StringUtils.reverseWordsInString(str1));
        System.out.println(StringUtils.reverseWordsInString(str10));
        System.out.println("str3.isChars=" + str3.isChars('a', 'b', 'c'));
        System.out.println("str4.isChars=" + str4.isChars('a', 'b', 'c'));
        System.out.println(str5.value);
        System.out.println(Arrays.toString(str5.phoneNumbers()));
        System.out.println();
        System.out.println("str5.isPostAdres()="+str5.isPostAdres());
        System.out.println();
        System.out.println("str6.isDate()="+str6.isDate());

        System.out.println(str7.reverseWordsInSentences());
        System.out.println();
        System.out.println(str8.reverseWordsInSentences());
    }
}
