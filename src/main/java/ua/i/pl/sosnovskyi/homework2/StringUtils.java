package ua.i.pl.sosnovskyi.homework2;


import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by A Sosnovskyi  on 30.01.2017.
 */
public class StringUtils {
    public final String value;

    /**
     * Constructor
     *
     * @param value String
     */
    public StringUtils(String value) {
        this.value = value;
    }

    /**
     * Инвертирует строку
     *
     * @return возвращает инвертированную строку
     */
    public String reverseString() {
        String result;
        char[] data = value.toCharArray();
        for (int i = 0; i < value.length() / 2; i++) {
            char tmp = data[i];
            data[i] = data[data.length - 1 - i];
            data[data.length - 1 - i] = tmp;
        }
        result = new String(data);
        return result;
    }

    /**
     * Проверяет является ли строка полиндромом
     *
     * @return boolean "да" - true "нет" - false
     */
    public boolean polindromString() {
        boolean result;
        String tmp = "";
        String[] tokens = value.split(" ");
        for (String word : tokens) {
            tmp = tmp.concat(word);
        }

        return tmp.equalsIgnoreCase(new StringUtils(tmp).reverseString());
    }

    //---------------------------------------------------------------------------------------------------------

    /**
     * меняет первое и последнее слово в строке учитывает точку,
     *  один пробел после точки или один пробел после границы слова
     *
     * @return String
     */
    public static String reverseWordsInString(String value) {
        StringBuilder sb = new StringBuilder();
        int firstSpaceNumber = value.indexOf(" ");
        Pattern pattern;
        Matcher matcher;
        if(value.lastIndexOf('.')<0){
            pattern = Pattern.compile("(?<end>[\\wА-Яа-я0-9\\S]+(?=$| \\n| $))");
            matcher = pattern.matcher(value);
            int lastWordStartIndex = 0;
            int lastWordEndIndex = 0;
            while (matcher.find()) {
                lastWordStartIndex = matcher.start();
                lastWordEndIndex = matcher.end();
            }
            String tmp1 = value.substring(0, firstSpaceNumber);
            String tmp2 = value.substring(firstSpaceNumber, lastWordStartIndex);
            String tmp3 = value.substring(lastWordStartIndex);
            String tmp4 = value.substring(lastWordEndIndex);
            sb.append(tmp3);
            sb.append(tmp2);
            sb.append(tmp1);
            sb.append(tmp4);
        }else{
             pattern = Pattern.compile("(?<end>[\\wА-Яа-я0-9\\S]+(?=\\. |\\.$|\\.\\n))");
             matcher = pattern.matcher(value);
            int lastWordStartIndex = 0;
            int lastWordEndIndex = 0;
            while (matcher.find()) {
                lastWordStartIndex = matcher.start();
                lastWordEndIndex = matcher.end();
            }
            String tmp1 = value.substring(0, firstSpaceNumber);
            String tmp2 = value.substring(firstSpaceNumber, lastWordStartIndex);
            String tmp3 = value.substring(lastWordStartIndex, lastWordEndIndex);
            String tmp4 = value.substring(lastWordEndIndex);

            sb.append(tmp3);
            sb.append(tmp2);
            sb.append(tmp1);
            sb.append(tmp4);
        }
        return sb.toString();
    }
//--------------------------------------------------------------------------------------------------------

    /**
     * проверяет длинну строки  и если ее длина больше 10,
     * то оставить в строке только первые 6 символов, иначе дополнить строку символами 'o' до длины 12
     *
     * @return String
     */
    public String modifyString() {
        String result;
        int length = value.length();
        if (length > 10) {
            result = value.substring(0, 6);
        } else {
            char[] data = new char[12 - length];
            for (int i = 0; i < data.length; i++) {
                data[i] = 'o';
            }
            result = value.concat(new String(data));
        }
        return result;
    }
//=======================================================================================================

    /**
     * функция, которая меняет местами первое и последнее слово в каждом предложении.
     * (предложения могут разделятся ТОЛЬКО знаком точки)
     *
     * @return result String
     */
    public String reverseWordsInSentences() {

        //(?<begin>(?<=^|\. |\.\n|. \n|\.\n\n)(?<! \n)[\w0-9А-Яа-я\S]+)|(?<end>[\wА-Яа-я0-9\S]+(?=\. |\.$|\.\n)) последняя моя редакция
        //(?<=\w)\.(?!\S) точка
        // [\w \S\n]+?\.(?!\S) предложения
        //[\w \S\n]+?\.(?!\S) ? предложения с пробелом после запятой если есть
        List<String> list = new ArrayList<>();
        Pattern pattern = Pattern.compile("[\\w \\S\\n]+?\\.(?!\\S) ?");
        Matcher matcher = pattern.matcher(value);
        while (matcher.find()) {
            list.add(matcher.group());
        }
        String result="";
        for (String next : list) {
          //  String tmpStr = next;
            result = result.concat(StringUtils.reverseWordsInString(next));

        }


        return result;
    }
//=======================================================================================================

    /**
     * функция, которая определят является ли строчка датой формата MM.DD.YYYY
     *
     * @return boolean true - является  false -нет
     */
    public boolean isDate() {
        if(value==null){
            throw new IllegalArgumentException("String is NULL");
        }
        // Pattern pattern=Pattern.compile("(?<= )[0-31]{2}\\.[0-12]{2}\\.[0-9]{4}(?= )"); // паттерн находит часть строки
        Pattern pattern = Pattern.compile("(?<=^)[0-31]{2}\\.[0-12]{2}\\.[0-9]{4}(?=$)"); //// паттерн находит всю строку
        Matcher matcher = pattern.matcher(value);
       // boolean result = matcher.find();
        return  matcher.find();
    }

    /**
     * функция, которая определяет есть ли в данной строке почтовый адрес
     *
     * @return boolean
     */
    public boolean isPostAdres() {
        Pattern pattern = Pattern.compile("((city \\d{5}\\. )|(city \\d{5}, )|((postal code \\d{5})|Postal code \\d{5}))");
        Matcher matcher = pattern.matcher(value);
        return matcher.find();
    }

    /**
     * функция, которая находит все телефоны в переданном тексте формата +Х(ХХХ)ХХХ-ХХ-ХХ,
     * и возвращает их в виде массива
     *
     * @return result result String[] телефоны формата +Х(ХХХ)ХХХ-ХХ-ХХ,
     */
    public String[] phoneNumbers() {
        Pattern pattern = Pattern.compile("\\+\\d\\(\\d{3}\\)-\\d{3}-\\d{2}");
        Matcher matcher = pattern.matcher(value);

        int counter = 0;
        while (matcher.find()) {
            counter++;
        }
        String[] result = new String[counter];
        matcher = pattern.matcher(value);
        counter = 0;
        while (matcher.find()) {
            result[counter] = matcher.group();
            counter++;
        }


        return result;
    }

    /**
     * функция, которая проверяет содержит ли строка только символы 'a', 'b', 'c' или нет.
     *
     * @return boolean true содержит ли строка только символы 'a', 'b', 'c' false - нет
     */
    public boolean isChars(char... args) {
        Pattern pattern = Pattern.compile("^[" + args[0] + "-" + args[1] + "-" + args[2] + "]+$");
        Matcher matcher = pattern.matcher(value);

        return matcher.matches();
    }
}
