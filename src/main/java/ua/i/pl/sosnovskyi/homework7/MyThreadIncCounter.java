package ua.i.pl.sosnovskyi.homework7;

/**
 * Created by A Sosnovskyi on 02.03.2017.
 * @see DoubleCounter
 * Сравнивает значения count1 и count2 класса DoubleCounter
 * Печатает результат сравнения
 * Увеличивает значения счетчика count1
 * Засыпает на 10 мсек
 * Увеличивает значения счетчика count2
 *
 */
public class MyThreadIncCounter extends Thread {
    @Override
    public void run() {
        while (true) {
            
            ////------------------------Для получения результатов с синхронизацией раскомментировать----------------------------------------
          //  synchronized (DoubleCounter.class) {
            ////----------------------------------------------------------------------------------------------------------------------------
            // int a получает текущее значение count1
                int a = DoubleCounter.getCount1();
            // int b получает текущее значение count2
                int b = DoubleCounter.getCount2();
                System.out.println((a - b < 0) ? "Count1 < Count2 ".concat(String.valueOf(a)).concat(" ").concat(String.valueOf(b)) :
                        //"Count1 - Count2 ".concat(String.valueOf(a)).concat(" ").concat(String.valueOf(b)))
                        (a-b==0)? "Count1 = Count2 ".concat(String.valueOf(a)).concat(" ").concat(String.valueOf(b)):
                                "Count1 > Count2 ".concat(String.valueOf(a)).concat(" ").concat(String.valueOf(b)));
                DoubleCounter.setCount1(a + 1);
                try {
                    sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                DoubleCounter.setCount2(b + 1);
            ////-------------------------Для получения результатов с синхронизацией раскомментировать----------------------------------------------
            //}
            ////----------------------------------------------------------------------------------------------------------------------------------
        }
    }
}

