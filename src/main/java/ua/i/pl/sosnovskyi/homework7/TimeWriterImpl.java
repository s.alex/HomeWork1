package ua.i.pl.sosnovskyi.homework7;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;


/**
 * Created by A Sosnovskyi on 28.02.2017.
 */
public class TimeWriterImpl implements Runnable {
    SimpleDateFormat format;

    public TimeWriterImpl() {
        this.format = new SimpleDateFormat("Время hh:mm:ss");
    }

    @Override
    public void run() {
        //  System.out.println(format.format(new GregorianCalendar().getTime()));

        try {
            while (true) {
                System.out.println(format.format(new GregorianCalendar().getTime()));
                Thread.sleep(1000);
            }

        } catch (InterruptedException e) {
            return;
            //e.printStackTrace();
        }
    }
}
