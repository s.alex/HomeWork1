package ua.i.pl.sosnovskyi.homework7;

/**
 * Created by A Sosnovskyi on 01.03.2017.
 */
public class Toy {
    private boolean taken=false;

    /**
     * взять игрушку
      */
    public synchronized void take(){
        while(taken){}
        taken=true;
    }

    /**
     * бросить игрушку
     */
    public synchronized void drop(){
        taken=false;
    }
}
