package ua.i.pl.sosnovskyi.homework7;

import java.io.*;

/**
 * Created by A Sosnovskyi on 27.02.2017.
 */
public class HW7 {
    public static void main(String[] args) {
        Thread myThread = new TimeWriter();
        Runnable myRunnable=new TimeWriterImpl();
        Thread myThreadRunnable=new Thread(myRunnable);
        myThreadRunnable.start();
       // myThread.start();

 //=================================================================================
        // myThread.setDaemon(true);
//        try {
//            myThread.join();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
    //==============================================================================
        try (BufferedReader buf = new BufferedReader(new InputStreamReader(System.in))) {
            String str = buf.readLine();
            if (str != null) {
                myThreadRunnable.interrupt();
              //  myThread.interrupt();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
