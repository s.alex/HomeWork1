package ua.i.pl.sosnovskyi.homework7;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by A Sosnovskyi on 27.02.2017.
 */
public class TimeWriter extends Thread {

    SimpleDateFormat format;

    public TimeWriter() {
        //format = new SimpleDateFormat("День dd Месяц MM Год yyyy Время hh:mm:ss");
        format = new SimpleDateFormat("Время hh:mm:ss");
    }

    @Override
    public void run() {
        // int i=0;

        try {
            while (true) {
                //System.out.println(new Date());
                System.out.println(format.format(new GregorianCalendar().getTime()));
                // i++;
                sleep(1000);
            }

        } catch (InterruptedException e) {
            return;
            // e.printStackTrace();
        }
    }
}
