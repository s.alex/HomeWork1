package ua.i.pl.sosnovskyi.homework7;

/**
 * App7_3
 * Created by A Sosnovskyi on 01.03.2017.
 * @see MyThreadIncCounter();
 * создает два объекта потока MyThreadIncCounter() и запускает их на выполнение
 */
public class App7_3 {
    public static void main(String[] args) {
        DoubleCounter doubleCounter = new DoubleCounter();
        Thread my1 = new MyThreadIncCounter();
        Thread my2 = new MyThreadIncCounter();

        my2.start();
        my1.start();
    }


}


/*Output
Synchronized:
Count1 - Count2 0 0
Count1 - Count2 1 1
Count1 - Count2 2 2
Count1 - Count2 3 3
Count1 - Count2 4 4
Count1 - Count2 5 5
Count1 - Count2 6 6
Count1 - Count2 7 7
Count1 - Count2 8 8
Count1 - Count2 9 9
Count1 - Count2 10 10
Count1 - Count2 11 11
Count1 - Count2 12 12
Count1 - Count2 13 13
Count1 - Count2 14 14
Count1 - Count2 15 15
Count1 - Count2 16 16
Count1 - Count2 17 17
Count1 - Count2 18 18
Count1 - Count2 19 19
Count1 - Count2 20 20
Count1 - Count2 21 21
Count1 - Count2 22 22
Count1 - Count2 23 23
Count1 - Count2 24 24
Count1 - Count2 25 25
Count1 - Count2 26 26
Count1 - Count2 27 27
Count1 - Count2 28 28
Count1 - Count2 29 29
Count1 - Count2 30 30
Count1 - Count2 31 31
Count1 - Count2 32 32
Count1 - Count2 33 33
.
.
.
 */

/*Output
NonSynchronized:
Count1 - Count2 0 0
Count1 - Count2 1 0
Count1 - Count2 2 1
Count1 - Count2 3 1
Count1 - Count2 4 2
Count1 - Count2 5 2
Count1 - Count2 6 3
Count1 - Count2 7 3
Count1 - Count2 8 4
Count1 - Count2 9 4
Count1 - Count2 10 5
Count1 - Count2 11 6
Count1 - Count2 11 5
Count1 - Count2 12 6
Count1 - Count2 13 7
Count1 - Count2 14 7
Count1 - Count2 15 8
Count1 - Count2 16 9
Count1 - Count2 17 8
Count1 - Count2 18 9
Count1 - Count2 19 10
Count1 - Count2 20 11
Count1 - Count2 21 10
.
.
.
*/