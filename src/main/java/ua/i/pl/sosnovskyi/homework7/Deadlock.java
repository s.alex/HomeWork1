package ua.i.pl.sosnovskyi.homework7;

import java.util.Objects;

/**
 * Created by A Sosnovskyi on 28.02.2017.
 */
public class Deadlock {
    public static void main(String[] args) {
        // на площадке две игрушки
        Toy firstToy = new Toy();
        Toy secondToy = new Toy();
        /**
         * Первый Ребенок берет первую игрушку в одну руку, потом берет вторую игрушку во вторую руку.
         * После чего бросает одну из игрушек(можно сделать псевдослучайный выбор какую из игрушек бросить)
         */
        Thread firstChild = new Thread(() -> {
            while(true) {
                firstToy.take();
                System.out.println("First child takes first toy by right hand");
                secondToy.take();
                System.out.println("First child takes second toy by left hand");

                System.out.println("First child takes first and second toys");
                firstToy.drop();
                System.out.println("First child drop first  toy");
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        /**
         * Второй Ребенок берет первую игрушку в одну руку, потом берет вторую игрушку во вторую руку.
         * После чего бросает одну из игрушек(можно сделать псевдослучайный выбор какую из игрушек бросить)
         */
        Thread secondChild = new Thread(() -> {
            while(true) {
                secondToy.take();
                System.out.println("Second child take second toy by left hand");
                firstToy.take();
                System.out.println("Second child take first toy by right hand");

                System.out.println("Second child takes first and second toys");
                secondToy.drop();
                System.out.println("Second child drop second toy");
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        // можно поменять порядок вызова запуска потоков
        secondChild.start();
        firstChild.start();

    }


}

