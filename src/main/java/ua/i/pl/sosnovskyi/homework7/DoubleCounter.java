package ua.i.pl.sosnovskyi.homework7;

/**
 * Created by A Sosnovskyi on 01.03.2017.
 * класс содержит два независимых счетчика
 */
public  class DoubleCounter {
    private  static int count1;
    private static int count2;

/**
конструктор
 */
    public DoubleCounter() {
        this.count1=0;
        this.count2=0;
    }

    /**
     * getCount1
     * @return int возвращает значение count1;
     */
    public static int getCount1() {
        return count1;
    }

    /**
     * setCount1
     * @param  count1 int устанавливает новое значение count1;
     */
    public static  void setCount1(int count1) {
        DoubleCounter.count1 = count1;
    }

    /**
     * getCount2
     * @return int возвращает значение count2;
     */
    public static  int getCount2() {
        return count2;
    }

    /**
     *setCount2
     * @param  count2 int устанавливает новое значение count2;
     */
    public static  void setCount2(int count2) {
        DoubleCounter.count2 = count2;
    }
}
