package ua.i.pl.sosnovskyi.homework3;

import java.util.Arrays;

/**
 * Created by A Sosnovskyi on 10.02.2017.
 */
public class GenericStorageImpl<T> implements GenericStorage<T> {
    private T[] storage;
    private int fullIndex;
    private final double FULLLIMIT = 0.25;

    /**
     * constructor размер хранилища устанавливается 10
     */
    public GenericStorageImpl() {
        this.storage = (T[]) new Object[10];
        fullIndex = 0;
    }

    /**
     * @param size int размер хранилища
     */
    public GenericStorageImpl(int size) {
        if (size <= 0) {
            throw new IllegalArgumentException("Size of storage <=0");
        }
        this.storage = (T[]) new Object[size];
        fullIndex = 0;
    }

    /**
     * возвращает размер заполненного хранилища
     */
    public int getLength() {
        return fullIndex;
    }

    /**
     * добавляет элемент в конец
     *
     * @param obj элемент для вставки в хранилище
     * @return int индекс вставленного элемента
     */

    @Override
    public int add(T obj) {

        if (fullIndex != 0) {
            if ((1.0 * (storage.length - fullIndex) / storage.length) <= FULLLIMIT) {

                T[] tmp = (T[]) new Object[storage.length * 2];

                System.arraycopy(storage, 0, tmp, 0, fullIndex);
                storage = tmp;


            }
        }
        storage[fullIndex] = obj;
        return fullIndex++;
    }

    /**
     * возвращает элемент с индексом
     *
     * @param index int индекс в хранилище
     * @return Т obj элемент
     */
    @Override
    public T get(int index) {
        if (index > fullIndex || index < 0) {
            throw new IndexOutOfBoundsException("index out of Bounds-"+index);
        }
        return storage[index];
    }

    /**
     * возвращает хранилище
     *
     * @return storage T[] возвращает весь масстив
     */
    @Override
    public T[] getAll() {
        return storage; // можно вернуть не все хранилище а его часть до  fullIndex
//        T[] utility_storage = (T[]) new Object[fullIndex];
//        System.arraycopy(storage, 0, utility_storage, 0, fullIndex);
//        return utility_storage;

    }

    /**
     * обновляет значение элемента с индексом index
     *
     * @param index int идекс элемента для изменения
     * @param obj   T новый элемент
     * @return T старое значение измененного элемента
     */
    @Override
    public T update(int index, T obj) {
        if (index > fullIndex || index < 0) {
            throw new IndexOutOfBoundsException("Index Out Of Bounds");
        }
        T tmp = storage[index];
        storage[index] = obj;
        return tmp;
    }

    /**
     * удаляет элемент по индексу
     *
     * @param index индекс удаляемого элемента
     * @return Т удаленный элемент
     */
    @Override
    public T delete(int index) {
        if (index > (fullIndex-1) || index < 0) {
            throw new IndexOutOfBoundsException("Index>fullIndex");
        }
        System.arraycopy(storage, index + 1, storage, index, fullIndex);
        if(fullIndex>0){
        fullIndex -= 1;
        }
        return storage[index];
    }

    /**
     * удаляет первый найденный элемент
     *
     * @param obj элемент для удаления
     * @return Т удаленный элемент
     */
    @Override
    public T delete(T obj) {
        T tmp = null;
        int i;
        for (i = 0; i < fullIndex; i++) {
            if (storage[i].equals( obj)) {   // сравнивает ссылки на объект
                tmp = storage[i];
                break;
            }
        }
        this.delete(i);
        return tmp;
    }

    /**
     * @return String
     */
    @Override
    public String toString() { //переделать циклом до fullIndex
        return "GenericStorageImpl{" +
                "storage=" + Arrays.toString(storage) +

                '}';
    }

    /**
     * @return storage T[] возвращает хранилище
     */
    public T[] getStorage() {
        return storage;
    }


}
