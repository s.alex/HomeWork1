package ua.i.pl.sosnovskyi.homework3;

import lombok.Getter;

import java.util.Objects;

/**
 * Created by A Sosnovskyi on 10.02.2017.
 */
@Getter
public class Human implements Comparable {
    private String name;
    private boolean man;
    private int age;

    public Human(String name, boolean man, int age) {
        this.name = name;
        this.man = man;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", man=" + man +
                ", age=" + age +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return isMan() == human.isMan() &&
                getAge() == human.getAge() &&
                Objects.equals(getName(), human.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), isMan(), getAge());
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }
}
