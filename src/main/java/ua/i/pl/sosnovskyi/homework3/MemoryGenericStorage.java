package ua.i.pl.sosnovskyi.homework3;

import java.util.*;

/**
 * Created by Admin on 11.02.2017.
 */
public class MemoryGenericStorage<T> implements GenericStorage<T> {
    // Node[] storage; так в методах не работает storage[index].getNodeValue() тип возвращаемего значения java.lang.Object зато работает  (T)storage[index];
   private Node<T>[] storage;
    private int fullIndex;
    private final double FULLLIMIT = 0.25;

    /**
     * constructor размер хранилища устанавливается 10
     */
    public MemoryGenericStorage() {
        this.storage = new Node[10];
        this.fullIndex = 0;
    }

    /**
     * @param size int размер хранилища
     */
    public MemoryGenericStorage(int size) {
        if (size <= 0) {
            throw new IllegalArgumentException("Size of storage <=0");
        }
        this.storage = new Node[size];
        this.fullIndex = 0;
    }

    /**
     * возвращает размер заполненного хранилища
     * @return int размер заполненного хранилища
     */
    public int getLength() {
        return fullIndex;
    }

    /**
     * добавляет элемент в конец
     *
     * @param obj элемент для вставки в хранилище
     * @return int индекс вставленного элемента
     */
    @Override
    public int add(T obj) {
        if (fullIndex != 0) {
            if ((1.0 * (storage.length - fullIndex) / storage.length) <= FULLLIMIT) {
                Node<T>[] tmp = new Node[storage.length * 2];
                System.arraycopy(storage, 0, tmp, 0, fullIndex);
                storage = tmp;

            }
        }
        storage[fullIndex] = new Node<>(obj);
        return fullIndex++;
    }

    /**
     * возвращает элемент с индексом
     *
     * @param index int индекс в хранилище
     * @return Т obj элемент
     */
    @Override
    public T get(int index) {
        if (index > fullIndex || index < 0) {
            throw new IndexOutOfBoundsException("index out of Bounds-"+ index);
        }
        return storage[index].getNodeValue();

    }

    /**
     * возвращает хранилище
     *
     * @return storage T[] возвращает весь масстив
     */
    @Override
    public T[] getAll() {
        List<T> list = new ArrayList<>();
        for (int i = 0; i < fullIndex; i++) {
            list.add(storage[i].getNodeValue());
        }
        T[] tmp = (T[]) list.toArray();   //не нравится приведение типов специально в локальной переменной
        return tmp;
    }

    /**
     * возвращает хранилище
     *
     * @return storage List<T> возвращает весь масстив
     */
    public List<T> getAllAsList() {
        List<T> list = new ArrayList<>();

        for (int i = 0; i < fullIndex; i++) {

            list.add(storage[i].getNodeValue());
        }
        return list;
    }

    /**
     * обновляет значение элемента с индексом index
     *
     * @param index int идекс элемента для изменения
     * @param obj   T новый элемент
     * @return T старое значение измененного элемента
     */
    @Override
    public T update(int index, T obj) {
        if (index > fullIndex || index < 0) {
            throw new IndexOutOfBoundsException("Index Out Of Bounds");
        }
        T tmp = storage[index].getNodeValue();
        storage[index] = new Node<>(obj);
        return tmp;
    }

    /**
     * удаляет элемент по индексу
     *
     * @param index индекс удаляемого элемента
     * @return Т удаленный элемент
     */
    @Override
    public T delete(int index) {
        if (index > (fullIndex-1) || index < 0) {
            throw new IndexOutOfBoundsException("Index>fullIndex");
        }
        System.arraycopy(storage, index + 1, storage, index, fullIndex);
        if(fullIndex>0) {
            fullIndex -= 1; //после удаления элемента уменьшить счетчик
        }
        return storage[index].getNodeValue();
    }

    /**
     * удаляет первый найденный элемент
     *
     * @param obj элемент для удаления
     * @return Т удаленный элемент
     */
    @Override
    public T delete(T obj) {
        T tmp = null;

        int i;
        for (i = 0; i < fullIndex; i++) {
            if (storage[i].getNodeValue().equals( obj)) { // сравнивает ссылки на объект
                tmp = storage[i].getNodeValue();
                break;
            }
        }
        this.delete(i);
        return tmp;
    }

    @Override
    public String toString() {
        return "MemoryGenericStorage{" +
                "storage=" + Arrays.toString(storage) +

                '}';
    }

    /**
     * @param <T> type of object
     */
    class Node<T> {
        private T value;

         T getNodeValue() {
            return this.value;
        }

         Node(T value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return "Node{" +
                    "value=" + value +
                    '}';
        }


    }
}
