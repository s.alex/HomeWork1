package ua.i.pl.sosnovskyi.homework3;

import java.util.Arrays;

/**
 * Created by A Sosnovskyi on 10.02.2017.
 */
public class HW3 {
    public static void main(String[] args) {
        Human first = new Human("Vasya", true, 21);
        Human second = new Human("Petya", true, 29);
        Human third = new Human("Tolya", true, 25);
        Human fourth = new Human("Slava", true, 27);
        Human fifth = new Human("Jenya", true, 31);
        Human six = new Human("Pasha", true, 39);
        Human seventh = new Human("Kolya", true, 45);
        Human eight = new Human("Misha", true, 28);
        Human nine = new Human("Roman", true, 38);
        Human ten = new Human("Andrey", true, 22);
        Human eleven = new Human("Alexey", true, 26);

        GenericStorageImpl<Human> genericStorage = new GenericStorageImpl<Human>();

        genericStorage.add(first);
        genericStorage.add(second);
        genericStorage.add(third);
        genericStorage.add(fourth);
        genericStorage.add(fifth);
        genericStorage.add(six);
        genericStorage.add(seventh);
        genericStorage.add(eight);
        genericStorage.add(nine);
        genericStorage.add(ten);
        genericStorage.add(eleven);
        System.out.println(genericStorage);

        System.out.println();
        System.out.println(genericStorage.get(1));
        genericStorage.update(1, new Human("Tolya", true, 25));
        System.out.println(genericStorage.get(1));
        System.out.println();
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//        genericStorage.delete(1);
//        genericStorage.delete(1);
//        genericStorage.delete(1);
//        genericStorage.delete(1);
//        genericStorage.delete(1);
//        genericStorage.delete(1);
//        genericStorage.delete(1);
//        genericStorage.delete(1);
//        genericStorage.delete(1);
//        genericStorage.delete(1);
//        genericStorage.delete(0); //удалит корректно 0-й элемент хранилище будет пустым
//        genericStorage.delete(1); // бросит исключение
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        System.out.println();
       genericStorage.delete(third);
        System.out.println();
        System.out.println(Arrays.toString(genericStorage.getAll()));
        System.out.println(genericStorage.getLength());
        //==========================================================================================================
//        Human[] human=genericStorage.getAll();  //вызовет Exception in thread "main" java.lang.ClassCastException: [Ljava.lang.Object; cannot be cast to [Lua.i.pl.sosnovskyi.homework3.Human;
//        for(int i=0; i<human.length; i++){
//            System.out.println(human[i]);
//        }


        System.out.println("==========================================================================================");
        MemoryGenericStorage<Human> genericStorage1 = new MemoryGenericStorage<>();
        genericStorage1.add(first);
        genericStorage1.add(second);
        genericStorage1.add(third);
        genericStorage1.add(fourth);
        genericStorage1.add(fifth);
        genericStorage1.add(six);
        genericStorage1.add(seventh);
        genericStorage1.add(eight);
        genericStorage1.add(nine);
        genericStorage1.add(ten);
        genericStorage1.add(eleven);
        System.out.println(genericStorage1.get(0));
        System.out.println();
        System.out.println(genericStorage1);
        genericStorage1.update(1, new Human("Ira", false, 25));
        System.out.println();
        System.out.println(genericStorage1.get(1));
        System.out.println();
        genericStorage1.delete(1);
        System.out.println();
        System.out.println(genericStorage1.get(1));
        System.out.println();
        System.out.println("delete(third)" + genericStorage1.delete(third));
        System.out.println();
        System.out.println();
        System.out.println(Arrays.toString(genericStorage1.getAllAsList().toArray()));
        System.out.println(Arrays.toString(genericStorage1.getAll()));
        System.out.println(genericStorage1.getLength());
    }
}
