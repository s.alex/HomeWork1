package ua.i.pl.sosnovskyi.homework3;

/**
 * Created by A Sosnovskyi on 10.02.2017.
 */
public interface GenericStorage<T> {
    int add(T obj);

    T get(int index);

    T[] getAll();

    T update(int index, T obj);

    T delete(int index);

    T delete(T obj);

}
