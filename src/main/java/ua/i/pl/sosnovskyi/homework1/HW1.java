package ua.i.pl.sosnovskyi.homework1;



/**
 * Created by Admin on 24.01.2017.
 */
public class HW1 {
    public static void main(String[] args) {

        Date date1 = new Date(1, 1, 0);
        Date date2 = new Date(1, 1, 20017); // не тестировал правильность
        System.out.println("date1: "+date1);
       System.out.println("date2: "+date2);
       System.out.println("день года: "+date1.getDayOfYear());
        System.out.println("день года: " + date2.getDayOfYear());
        System.out.println("колличество дней между: " + date1 + " и " + date2 + " = " + date1.daysBeetween(date2) + " daysBeetween");
        System.out.println("колличество дней между: " + date1 + " и " + date2 + " = " + date1.daysBeetweenOptimized(date2) + " daysBeetweenOptimized");






        System.out.println();
        System.out.println();
        System.out.println();

        Car car1 = new Car("BMW", "x5", 2005, "white", false) {
            @Override
            public String toString() {
                return "Sell: " + this.getMark() + " model " + this.getModel() + " year " + this.getYear() + " details on phone";
            }

            @Override
            public boolean equals(Object obj) {
//
                Car that = (Car) obj;
                 if(this.getMark().equals(that.getMark())){
                     return true;
                 }else{return false;}

            }
        };

        Car car2 = new Car("Toyota", "Corolla", 2011, "white", true) {
            @Override
            public String toString() {
                if (this.isNew()) {
                    return "Sell: " + this.getMark() + " model-" + this.getModel() + " year-" + this.getYear() + " color-"
                            + this.getColor() + " new";
                } else {
                    return "Sell: " + this.getMark() + " model-" + this.getModel() + " year-" + this.getYear() + " color-"
                            + this.getColor() + " old";
                }

            }

            @Override
            public boolean equals(Object obj) {
//
                Car that = (Car) obj;
                if(this.getColor().equals(that.getColor())){
                    return true;
                }else{return false;}
            }
        };

        System.out.println(car1);
        System.out.println(car2);
        System.out.println();
        System.out.println("car1.getMark().equals(car2.getMark())  ---  "+car1.getMark().equals(car2.getMark()));
        System.out.println("car1.equals(car2)  ---  "+car1.equals(car2));
        System.out.println();
        System.out.println("car2.getColor().equals(car1.getColor())   ---   "+car2.getColor().equals(car1.getColor()));
        System.out.println("car2.equals(car1)   ---   "+car2.equals(car1));


        System.out.println();
        System.out.println();
        System.out.println();




    }
}