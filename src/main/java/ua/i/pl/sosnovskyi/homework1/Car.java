package ua.i.pl.sosnovskyi.homework1;

import lombok.Getter;



/**
 * Created by Admin on 26.01.2017.
 */
@Getter
public class Car {
    private String mark;
    private String model;
    private int year;
    private String color;
    private boolean isNew;

    public Car(String mark, String model, int year, String color, boolean isNew) {
        this.mark = mark;
        this.model = model;
        this.year = year;
        this.color = color;
        this.isNew = isNew;
    }


}
