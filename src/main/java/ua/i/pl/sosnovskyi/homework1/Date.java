package ua.i.pl.sosnovskyi.homework1;

import lombok.Getter;
import lombok.Setter;

import java.util.Comparator;

/**
 * Created by Sosnovskyi Aleksandr on 23.01.2017.
 */
@Getter

public class Date {
    private Year year;
    private Month month;
    private Day day;
    private DayOfWeek dayOfWeek;
    protected final int controlDay = 1;
    protected final int controlMonth = 1;
    protected final int controlYear = 2017;
    protected final int controlYearDays = 365;

    /**
     * конструктор класса
     *
     * @param day
     * @param month
     * @param year
     */
    public Date(int day, int month, int year) {
        this.year = new Year(year);
        this.month = new Month(month);
        this.day = new Day(day);
        this.dayOfWeek = getDayOfWeek();

    }

    /**
     * Возвращает день недели
     *
     * @return
     */
    public DayOfWeek getDayOfWeek() {

     //   DayOfWeek dayOfWeek = DayOfWeek.valueOf(day.dayOfWeekIndex);
        DayOfWeek dayOfWeek = DayOfWeek.valueOf(day.getDayOfWeekIndex());
        return dayOfWeek;
    }

    /**
     * Возвращает колличество дней в году
     *
     * @return колличество дней в году
     */
    public int getYearDays() {

        return year.getDaysOfYear();
    }

    public int getYearDays(int year) {

        return new Year(year).getDaysOfYear();
    }

    /**
     * Возвращает колличество дней в месяце
     *
     * @param index
     * @return
     */
    public int getDaysInMonthis(int index) {
        int[] arr = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        if (year.isLeapYear()) {
            arr[1] = 29;
        }
        return arr[index];
    }

    /**
     * возвращает числовое значение дня в году
     *
     * @return числовое значение дня в году
     */

    public int getDayOfYear() {

        int result = 0;
        int previosMonthisDeys = 0;
//        for (int index = 0; index < month.monthNumber - 1; index++) {
//            previosMonthisDeys = previosMonthisDeys + getDaysInMonthis(index);
//        }
        int monthNumber=month.getMonthNumber(); //fixed
        for (int index = 0; index < monthNumber - 1; index++) {
            previosMonthisDeys += getDaysInMonthis(index); // fixed
        }
//        result = this.day.getDay() + previosMonthisDeys;
//        return result;
        return this.day.getDay() + previosMonthisDeys;
    }

    //========================================================================================

    /**
     * возвращает колличество дней в заданном интервале интервал рассчитывается от значения
     * находящегося в this.year
     *
     * @param date Дата до которой расчитывается интервал
     * @return колличество дней в заданном интервале
     */
    public int daysBeetween(Date date) {
        int year1 = this.year.getYear();
        int year2 = date.year.getYear();
        int day1 = this.getDayOfYear();
        int day2 = date.getDayOfYear();
        int days1 = this.getYearDays();
        int days2 = date.getYearDays();
        int deltaYears = year1 - year2;

        if (deltaYears == 0) {
            return Math.abs(day1 - day2);
        } else {
            if (deltaYears < 0) {
                if (deltaYears != -1) {
                    int result = 0;

                    for (int index = year1 + 1; index < year2; index++) {
                        int tmpDays = new Year(index).daysOfYear;
                        result = result + tmpDays;
                    }

                    return result + (days1 - day1) + day2;
                } else {
                    return (days1 - day1) + day2;
                }

            } else {
                if (deltaYears != 1) {
                    int result = 0;
                    for (int index = year1 - 1; index > year2; index--) {
                        int tmpDays = new Year(index).daysOfYear;
                        result = result + tmpDays;
                    }

                    return day1 + result + (days2 - day2);
                } else {
                    return day1 + (days2 - day2);
                }

            }
        }

    }

    /**
     * возвращает колличество дней в заданном интервале интервал рассчитывается от значения
     * находящегося в this.year
     * оптимизирован подсчет интервала
     *
     * @param date Дата до которой расчитывается интервал
     * @return колличество дней в заданном интервале
     */

    public int daysBeetweenOptimized(Date date) {
        int deltaDays = deltaDays(this.year.getYear(), this.getDayOfYear(), date.year.getYear(), date.getDayOfYear());

        //System.out.println("deltaDays"+deltaDays);
//        int deltaYears = this.year.getYear() - date.year.getYear();
//        int result = 0;
//        if (deltaYears == 0) {
//            return Math.abs(this.getDayOfYear() - date.getDayOfYear());
//
//        } else {
//            Date startDate, finishDate;
//            if (deltaYears < 0) {
//                startDate = this;
//                finishDate = date;
//            } else {
//                startDate = date;
//                finishDate = this;
//            }
//            result = startDate.getYearDays() - startDate.getDayOfYear() + finishDate.getDayOfYear();
//            int yearNumberStart = startDate.year.getYear() + 1;
//            int yearNumberEnd = finishDate.year.getYear() - 1;
//            int firstLeap = yearNumberStart;
//            if ((yearNumberStart - yearNumberEnd) == 0) {
//                result = result + getYearDays(yearNumberStart);
//            } else {
//                if ((yearNumberStart - yearNumberEnd) < 4) {
//                    while (yearNumberStart <= yearNumberEnd) {
//                        result = result + getYearDays(yearNumberStart);
//                        yearNumberStart = yearNumberStart + 1;
//                    }
//                } else {
//                    while (yearNumberStart <= yearNumberEnd) {
//                        if (getYearDays(yearNumberStart) == 365) {
//                            yearNumberStart = yearNumberStart + 1;
//                        } else {
//                            firstLeap = yearNumberStart;
//                            break;
//                        }
//                    }
//                    result = result + ((yearNumberEnd - firstLeap) / 4) * 366 + getYearDays(firstLeap) +
//                            ((yearNumberEnd - yearNumberStart) - ((yearNumberEnd - firstLeap) / 4)) * 365;
//                }
//            }
//
//        }

        return deltaDays;
        // return result;
    }

    /**
     * возвращает число дней в заданном интервале
     *
     * @param year1 год начала интервала
     * @param day1  день года начала интервала
     * @param year2 год конца интервала
     * @param day2  день года конца интервала
     * @return
     */
    private int deltaDays(int year1, int day1, int year2, int day2) {
        int deltaYears = year1 - year2;
        int result = 0;
       // System.out.println("year1=" + year1 + " day1=" + day1 + " year2=" + year2 + " day2=" + day2);
        if (deltaYears == 0) {
            return Math.abs(day1 - day2);

        } else {
            int startDate, finishDate, startDay, finishDay;
            if (deltaYears < 0) {
                startDate = year1;
                startDay = day1;
                finishDate = year2;
                finishDay = day2;
            } else {
                startDate = year2;
                startDay = day2;
                finishDate = year1;
                finishDay = day1;
            }
            result = getYearDays(startDate) - startDay + finishDay;
            int yearNumberStart = startDate + 1;
            int yearNumberEnd = finishDate - 1;
            int firstLeap = yearNumberStart;
            if ((yearNumberStart - yearNumberEnd) == 0) {
                result = result + getYearDays(yearNumberStart);
            } else {
                if ((yearNumberStart - yearNumberEnd) < 4) {
                    while (yearNumberStart <= yearNumberEnd) {
                        result = result + getYearDays(yearNumberStart);
                        yearNumberStart = yearNumberStart + 1;
                    }
                } else {
                    while (yearNumberStart <= yearNumberEnd) {
                        if (getYearDays(yearNumberStart) == 365) {
                            yearNumberStart = yearNumberStart + 1;
                        } else {
                            firstLeap = yearNumberStart;
                            break;
                        }
                    }
                    result = result + ((yearNumberEnd - firstLeap) / 4) * 366 + getYearDays(firstLeap) +
                            ((yearNumberEnd - yearNumberStart) - ((yearNumberEnd - firstLeap) / 4)) * 365;
                }
            }

        }


        return result;

    }

    //=======================================================================================

    /**
     * КОСТЫЛИ !!!!!!!!!!!!!!!!!
     * возвращает индекс для дня недели
     *
     * @param day   день
     * @param month месяц
     * @param year  год
     * @return индекс для дня недели
     */
    public int daysIndex(int day, Month month, Year year) {

        int day1 = controlDay;
        int year1 = controlYear;
        int year2 = year.getYear();
        int days1 = controlYearDays;
        int days2 = year.daysOfYear;
        int day2 = 0;
        int previosMonthisDeys = 0;
        for (int index = 0; index < month.monthNumber - 1; index++) {
            previosMonthisDeys = previosMonthisDeys + getDaysInMonthis(index);
        }
        day2 = previosMonthisDeys + day;
        int deltaYears = year1 - year2;

        if (deltaYears == 0) {

            return (Math.abs(day1 - day2)) % 7;
        } else {
            if (deltaYears < 0) {
                if (deltaYears != -1) {
                    int result = 0;

                    for (int index = year1 + 1; index < year2; index++) {
                        int tmpDays = new Year(index).daysOfYear;
                        result = result + tmpDays;
                    }

                    return (result + (days1 - day1) + day2) % 7;
                } else {
                    return ((days1 - day1) + day2) % 7;
                }

            } else {
                if (deltaYears != 1) {
                    int result = 0;
                    for (int index = year1 - 1; index > year2; index--) {
                        int tmpDays = new Year(index).daysOfYear;
                        result = result + tmpDays;
                    }
                    if (Math.abs(7 - Math.abs((-day1 - result - (days2 - day2)) % 7)) == 7) {
                        return 0;
                    } else {
                        return Math.abs(7 - Math.abs((-day1 - result - (days2 - day2)) % 7));
                    }

                } else {

                    if (Math.abs(7 - Math.abs((-day1 - (days2 - day2)) % 7)) == 7) {
                        return 0;
                    } else {
                        return Math.abs(7 - Math.abs((-day1 - (days2 - day2)) % 7));
                    }

                }

            }
        }

    }

    /**
     * возвращает индекс для дня недели  РЕАЛИЗОВАНА!!!
     *
     * @param day
     * @param month
     * @param year
     * @return
     */
    public int daysIndexOptimized(int day, Month month, Year year) {

        int day1 = controlDay;
        int year1 = controlYear;
        int year2 = year.getYear();
        int day2 = 0;
        int deltaYears = year1 - year2;

        int previosMonthisDeys=0;
        for (int index = 0; index < month.monthNumber - 1; index++) {
            previosMonthisDeys = previosMonthisDeys + getDaysInMonthis(index);
        }
        day2 = previosMonthisDeys + day;
        int deltaDays = deltaDays(year1, day1, year2, day2);

        if (deltaYears <= 0) {

            return deltaDays % 7;
        } else {
//            return deltaDays%7;
            if (Math.abs(7 - deltaDays % 7) == 7) {
                return 0;
            } else {
                return Math.abs(7 - deltaDays % 7);
            }
        }


    }

    //============================================================================================
    @Override
    public String toString() {
        String monthNumberStr = null;
        if (month.monthNumber < 10) {
            monthNumberStr = "0" + month.monthNumber;
        } else {
            monthNumberStr = "" + month.monthNumber;
        }
        if (year.isLeapYear()) {

            return dayOfWeek.name + " " + day.getDay() + " - " + monthNumberStr + " - " + year.year + " высокосный";
        } else {
            return dayOfWeek.name + " " + day.getDay() + " - " + monthNumberStr + " - " + year.year + " не высокосный";
        }
    }


    /**
     * Year Устанавливает значение года и флаг высокосного года
     */
    @Getter
    private class Year {
        private int year;
        private final boolean leapYear;
        private int lastInterclary = 2016;
        private int daysOfYear;

        /**
         * конструктор
         *
         * @param year
         */
        public Year(int year) {
            this.year = year;
          //  this.leapYear = ((Math.abs(year - lastInterclary)) % 4 == 0);  //fixed next
//            if (this.leapYear) {
//                this.daysOfYear = 366;
//            } else {
//                this.daysOfYear = 365;
//            }
            this.leapYear =year%4==0;  //fixed
            this.daysOfYear =this.leapYear? 366:365;  //fixed
        }
    }

    /**
     * Month устанавливает номер месяца в году
     */
    @Getter
    private class Month {
        private int daysOfMonth;
        private int monthNumber;

        public Month(int month) {
            if (month > 12) {
                throw new IllegalArgumentException("В году 12 месяцев значение " + month + " больше допустимого");
            } else {
                if (month < 1) {
                    throw new IllegalArgumentException("В году 12 месяцев значение " + month + " меньше допустимого");
                }
                this.monthNumber = month;
                setDays(month);
            }
        }

        /**
         * вычисляет колличество дней в месяце
         *
         * @param monthNumber номер месяца в году
         */
        public void setDays(int monthNumber) {
            if (monthNumber == 2 && year.leapYear) {
                this.daysOfMonth = 29;
            } else {
                int[] arr = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
                int result = arr[monthNumber - 1];
                this.daysOfMonth = result;
            }
        }

        /**
         * Возвращает колличество дней в месяце
         * @return
         */
//        public int getDays(){
//            return daysOfMonth;
//        }
    }

    /**
     * Day
     */
    @Getter

    private class Day {
        private int day;
        private int dayOfWeekIndex;



        public Day(int day) {

            if (day < 1 || day > month.getDaysOfMonth()) {
                throw new IllegalArgumentException("В месяце " + month.monthNumber + " не больше " + month.getDaysOfMonth() + " дней");
            } else {

                this.day = day;
               // this.dayOfWeekIndex = daysIndex(day, month, year);
                this.dayOfWeekIndex = daysIndexOptimized(day, month, year);
             //   System.out.println("this.dayOfWeekIndex=" + this.dayOfWeekIndex);
             // System.out.println("daysIndexOptimized="+daysIndexOptimized(day, month, year));

            }
        }


    }

    /**
     * DayOfWeek
     */
    private enum DayOfWeek {
        SU("Воскресенье", 0),
        MON("Понедельник", 1),
        TU("Вторник", 2),
        WE("Среда", 3),
        TH("Четверг", 4),
        FR("Пятница", 5),
        SA("Суббота", 6);

        private final int num;
        private final String name;

        DayOfWeek(String name, int num) {
            this.num = num;
            this.name = name;
        }

        public static DayOfWeek valueOf(int index) {
//            DayOfWeek result = null;  // не зависит он порядка элементов Enum
//            for (DayOfWeek dayOfWeek : DayOfWeek.values()) {
//                if (dayOfWeek.num == index) {
//                    result = dayOfWeek;
//                }
//            }
//            return result;
            return DayOfWeek.values()[index]; // зависит он порядка элементов Enum


        }
    }

}
