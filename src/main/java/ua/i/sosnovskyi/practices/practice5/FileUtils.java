package ua.i.sosnovskyi.practices.practice5;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by A Sosnovsky on 05.03.2017.
 * Для работы с файлами
 */
public class FileUtils {
    /**
     * @param quantity
     * @return
     */
    public static List<Integer> randomIntegerGenerator(int quantity) {
        List<Integer> list = new ArrayList<>();
        Random random = new Random();
        for (int count = 0; count < quantity; count++) {
            list.add(random.nextInt());

        }
        return list;
    }

    /**
     * @param list
     * @param fileName
     */
    public static void fileSaver(List<Integer> list, String fileName) {
//        try ( BufferedWriter bufferedWriter=new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName)))){
//for(Integer value: list){
//    bufferedWriter.write(String.valueOf(value).concat("\\n"));
//}
//            bufferedWriter.flush();

        try (DataOutputStream br = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(fileName)))) {
            for (Integer value : list) {
                br.writeInt(value); //write(value);
            }
            br.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    /**
     * @param fileName
     * @return
     */
    public static List<Integer> readIntFromFile(String fileName) {
        List<Integer> list = new ArrayList<>();

        try (DataInputStream bi = new DataInputStream(new BufferedInputStream(new FileInputStream(fileName)))) {
            int tmp;
            try {
                while (true) {
                    tmp = bi.readInt();
                    list.add(tmp);
                }
            } catch (EOFException e) {

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * @param fileName
     * @param charSet
     * @return
     */
    public static String readStringFromFile(String fileName, String charSet) {

        StringBuffer s2 = new StringBuffer();
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), charSet))) {
            String s;

            while ((s = bufferedReader.readLine()) != null) {
                s2.append(s);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return s2.toString();
    }

    /**
     * @param regEx
     * @param strToMatch
     * @return
     */
    public static Matcher getMatcher(String regEx, String strToMatch) {
        Pattern pattern = Pattern.compile(regEx);
        Matcher matcher = pattern.matcher(strToMatch);
        return matcher;
    }

    /**
     *
     * @param regEx
     * @param str
     * @return
     */
    public static Map<String, List<Integer>> getData(String regEx, String str) {
        Map<String, List<Integer>> data = new HashMap<>();
        Matcher matcher = getMatcher(regEx, str);
        while (matcher.find()) {
            String name = matcher.group("name");
            String marks = matcher.group("marks");
            Matcher markMatcher = getMatcher("[\\d]+", marks);
            List<Integer> marksList = new ArrayList<>();

            while (markMatcher.find()) {
                //Integer.getInteger(markMatcher.group()) не работает!!!!!!!!!!!!!
                marksList.add(Integer.valueOf(markMatcher.group()));
            }
            data.put(name, marksList);

        }

        return data;
    }



    /**
     * @param fileName
     */
    public static void sortIntDataFile(String fileName) {
        List<Integer> list = FileUtils.readIntFromFile(fileName);
        ArrayList<Integer> list1 = new ArrayList<>();
        int[] tmp = new int[list.size()];
        int i = 0;
        for (int val : list) {
            tmp[i] = val;
            i++;
        }
        Arrays.sort(tmp);

        for (i = 0; i < tmp.length; i++) {
            list1.add(tmp[i]);
        }
        FileUtils.fileSaver(list1, fileName);
    }

    public static void bufferedCopyFile(String sourceFileName, String destFileName) {

        try (BufferedInputStream buffIn = new BufferedInputStream(new FileInputStream(sourceFileName));
             BufferedOutputStream buffOut = new BufferedOutputStream(new FileOutputStream(destFileName))) {
            int val;
            while ((val = buffIn.read()) != -1) {
                // byte[] buffer = new byte[buffIn.available()];
                //  buffIn.read(buffer, 0, buffer.length);
                //System.out.println(val);
                buffOut.write(val);

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
