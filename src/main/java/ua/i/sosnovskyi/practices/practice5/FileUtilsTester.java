package ua.i.sosnovskyi.practices.practice5;

import java.util.List;

/**
 * Created by Admin on 06.03.2017.
 */
public class FileUtilsTester {
    public static void main(String[] args) {
        List<Integer> list= FileUtils.randomIntegerGenerator(20);
//        for(int rez: list){
//            System.out.println(rez);
//        }
        System.out.println();
        FileUtils.fileSaver(list, "TestFileUtils.txt");
        List<Integer> list2=FileUtils.readIntFromFile("TestFileUtils.txt");
        for(int rez: list2){
            System.out.println(rez);
        }
        System.out.println();
        FileUtils.sortIntDataFile("TestFileUtils.txt");
        List<Integer> list3=FileUtils.readIntFromFile("TestFileUtils.txt");
        for(int rez: list3){
            System.out.println(rez);
        }
        System.out.println("=====================================================================");
        FileUtils.bufferedCopyFile("TestFileUtils.txt", "TestFileUtils1.txt");
        List<Integer> list4=FileUtils.readIntFromFile("TestFileUtils1.txt");
        for(int rez: list4){
            System.out.println(rez);
        }
    }
}
