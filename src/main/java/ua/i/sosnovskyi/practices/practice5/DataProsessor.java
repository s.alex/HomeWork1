package ua.i.sosnovskyi.practices.practice5;

import java.util.List;

/**
 * Created by A Sosnovsky on 07.03.2017.
 */
public class DataProsessor {
    /**
     * Подсчитывает средний балл
     * @param list  List<Integer> лист из значений для подсчета среднего арифметического
     * @return result/length целочисленное значение величины рифметического
     */
    public static  int average(List<Integer> list){
        int result=0;
        int length=list.size();
        for(int tmp:list){
            result+=tmp;

        }
        return result/length;
    }
}
