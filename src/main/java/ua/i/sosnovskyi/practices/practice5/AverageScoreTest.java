package ua.i.sosnovskyi.practices.practice5;

import java.util.List;
import java.util.Map;


/**
 * Created by A Sosnovsky on 07.03.2017.
 * тестирование работы классов:
 * @see DataProsessor
 * @see FileUtils
 */
public class AverageScoreTest {
    public static void main(String[] args) {
        String regEx = "(?<name>[\\w]+ [A-Za-z]+) (?<marks>[\\d\\s]+)";
        String str = FileUtils.readStringFromFile("AverageScore.txt", "UTF-8");
        System.out.println(str);
        Map<String, List<Integer>> map = FileUtils.getData(regEx, str);
        for (Map.Entry<String, List<Integer>> entry : map.entrySet()) {
            if(DataProsessor.average(entry.getValue())>90){
                System.out.println(entry.getKey() + " " + entry.getValue());
            }

        }
    }


}
