package ua.i.sosnovskyi.practices.practice5;



import ua.i.pl.sosnovskyi.homework6.Book;

import java.io.*;
import java.util.List;

/**
 * Created by A Sosnovskyi on 07.03.2017.
 */
public class StudentGroup implements Serializable{
    public List<Student> students;

    public StudentGroup(List<Student> students) {
        this.students = students;
    }
    private static void serializableGroup(StudentGroup studentGroup, String fileName) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName))) {
            oos.writeObject(studentGroup);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Student deSerializableGroup(String filename) {
        Student result = null;
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filename))) {
            try {
                result = (Student) ois.readObject();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}
