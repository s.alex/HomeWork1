package ua.i.sosnovskyi.practices.practice3;

import java.util.Date;

/**
 * Created by A Sosnovsky  on 07.02.2017.
 */
public class Ticket extends Entity<Integer> {
    private final String movieHallName; //
    private final int ticketId;    //
    private final Date timeOfSell;  //
    private final int userId;   //
    private final int hall;   //
    private final int seanceId;  //
    private final int seatRow; //
    private  final int seatPosition;
private final long price;

    public Ticket(String movieHallName, int ticketId, Date timeOfSell, int userId, int hall, int seanceId, int seatRow, int seatNumber, long price) {
        this.movieHallName = movieHallName;
        this.ticketId = ticketId;
        this.timeOfSell = timeOfSell;
        this.userId = userId;
        this.hall = hall;
        this.seanceId = seanceId;
        this.seatRow = seatRow;
        this.seatPosition = seatNumber;
        this.price=price;
    }
}
