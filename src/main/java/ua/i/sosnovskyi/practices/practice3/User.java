package ua.i.sosnovskyi.practices.practice3;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by A Sosnovsky  on 07.02.2017.
 */
@Getter
@Setter
@EqualsAndHashCode
public class User extends Entity<Integer> {
    private int userId;
    private String usernName; //Имя
    private String userSurname; //Фамилия
    private String userLogin; //Login
    private String userPassword; // Password
    private String userEmail; // Email
    private int userDiscount;
    private List<Ticket> purchasedTickets; //купленный билет
    private String permissions;  //права доступа

    public User(String usernName, String userSurname, String userLogin, String userPassword, String userEmail, Enum permissions) {

        this.usernName = usernName;
        this.userSurname = userSurname;
        this.userLogin = userLogin;
        this.userPassword = userPassword;
        this.userEmail = userEmail;
        this.permissions = permissions.name();
    }

    enum Permissions {
        ADMINISTRATOR, USER
    }
}


