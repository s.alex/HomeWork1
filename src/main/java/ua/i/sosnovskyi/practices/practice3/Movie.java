package ua.i.sosnovskyi.practices.practice3;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by A Sosnovsky  on 07.02.2017.
 */
@Getter
@Setter
@EqualsAndHashCode
public class Movie  extends Entity<Integer>{
    private String name;
    private String genre; //жанр
    private String producedBy; //производство
    private int duration; //длительность

    public Movie(String name, String genre, String producedBy, int duration) {
        this.name = name;
        this.genre = genre;
        this.producedBy = producedBy;
        this.duration = duration;
    }
}
