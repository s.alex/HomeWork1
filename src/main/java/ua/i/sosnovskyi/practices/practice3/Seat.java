package ua.i.sosnovskyi.practices.practice3;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by A Sosnovsky on 07.02.2017.
 */
@Getter
@Setter
@EqualsAndHashCode
public class Seat extends Entity<Integer> {
    private int seatRow; //ряд
    private int seatPosition; //место
    private boolean seatFree = true; //свободно
    private int hall; // зал

    public Seat(int hall, int seatRow, int seatPosition, boolean seatFree) {
        this.seatRow = seatRow;
        this.seatPosition = seatPosition;
        this.seatFree = seatFree;
        this.hall = hall;
    }
}
