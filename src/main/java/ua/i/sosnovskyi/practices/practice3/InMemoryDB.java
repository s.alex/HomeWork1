package ua.i.sosnovskyi.practices.practice3;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

import static javafx.scene.input.KeyCode.K;

/**
 * Created by A Sosnovsky on 05.03.2017.
 */
public class InMemoryDB<Integer, V> implements GenericStorage<Integer, V> {
    private List<Node> storage;

    public InMemoryDB() {
        this.storage = new ArrayList<>();
    }

    @Override
    public int create(Integer key, V val) {
        return 0;
    }

    @Override
    public V read(Integer key) {
        return null;
    }

    @Override
    public boolean update(Integer key, V val) {
        return false;
    }

    @Override
    public V delete(Integer key) {
        return null;
    }

    @Getter
    @Setter

    public class Node{
        private int id;
        private V value;

        public Node(int id, V value) {
            this.id = id;
            this.value = value;
        }
    }
}
