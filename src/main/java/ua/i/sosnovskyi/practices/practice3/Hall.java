package ua.i.sosnovskyi.practices.practice3;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import java.util.Date;

import java.util.List;
import java.util.Map;

/**
 * Created by Admin on 04.03.2017.
 */
@Getter
@Setter
@EqualsAndHashCode
public class Hall  extends Entity<Integer>{
    private List<Seat> seats;
    private Map<Date, Seance> seances;

    public Hall(List<Seat> seats) {
        this.seats = seats;
    }
}
