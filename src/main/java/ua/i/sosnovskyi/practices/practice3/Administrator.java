package ua.i.sosnovskyi.practices.practice3;

/**
 * Created by A Sosnovsky on 05.03.2017.
 */
public class Administrator {
    private int adminId;
    private String adminName; //Имя
    private String adminSurname; //Фамилия
    private String adminLogin; //Login
    private String adminPassword; // Password
    private String adminEmail; // Email

    public Administrator(int adminId, String adminName, String adminSurname, String adminLogin, String adminPassword, String adminEmail) {
        this.adminId = adminId;
        this.adminName = adminName;
        this.adminSurname = adminSurname;
        this.adminLogin = adminLogin;
        this.adminPassword = adminPassword;
        this.adminEmail = adminEmail;
    }
}
