package ua.i.sosnovskyi.practices.practice3;

/**
 * Created by A Sosnovsky on 05.03.2017.
 */
public interface GenericStorage<K, V> {
    int create( K key,  V val);
    V read( K key);
    boolean update(K key, V val);
    V delete(K key);
}
