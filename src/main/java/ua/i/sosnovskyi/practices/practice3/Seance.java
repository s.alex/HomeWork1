package ua.i.sosnovskyi.practices.practice3;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by A Sosnovsky on 04.03.2017.
 */
public class Seance extends Entity<Integer> {
private long price;
private Map<Movie, HourOfShow> movieTimeTable;

    public Seance(long price, Map<Movie, HourOfShow> movieTimeTable) {
        this.price = price;
        this.movieTimeTable = movieTimeTable;
    }

    private class HourOfShow{
        private List<Date> dayTable;
        private List<Date> timeTable;
    }
}
