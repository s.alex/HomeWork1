package ua.i.sosnovskyi.practices.practice2;

/**
 * класс рейтинга имен 2008г
 * Created by A Sosnovskyi on 13.02.2017.
 */
public class NameRating {
    private int position;
    private  String male;
    private String female;

    public NameRating(int position, String male, String female) {
        this.position=position;
        this.male=male;
        this.female=female;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getMaleName() {
        return male;
    }

    public void setMaleName(String name) {
        this.male = name;
    }

    public String getFemaleName() {
        return female;
    }

    public void setFemaleName(String name) {
        this.female = name;
    }

    @Override
    public String toString() {
        return "NameRating{" +
                "position=" + position +
                ", male='" + male + '\'' +
                ", female='" + female + '\'' +
                '}';
    }
}
