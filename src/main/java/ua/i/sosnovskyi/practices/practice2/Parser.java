package ua.i.sosnovskyi.practices.practice2;

import java.io.*;


import java.util.List;


/**
 * Created by A Sosnovskyi on 13.02.2017.
 */
interface Parser<T> {

//    "(?<rankBefore><tr align=\"right\"><td>(?<rank>\\d+)<\\/td>)<td>(?<male>\\w+)<\\/td><td>(?<female>\\w+)<\\/td><\\/tr>"; RatingNames

    /**
     *
     * @param fileName String путь к файлу(имя файла)
     * @param charSet String кодировка
     * @return String файл прочитанный в виде строки
     */
     static String readFromFile(String fileName, String charSet) {

        if (fileName == null | fileName.length() == 0) {
            throw new IllegalArgumentException("Неверное имя файла");
        }
        StringBuilder sb = new StringBuilder();
        try (BufferedReader buff = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), charSet))) {
            while (true) {
                String tmp = buff.readLine();
                if (tmp == null) {
                    break;
                } else {
                    sb = sb.append(tmp);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return sb.toString();
    }

    /**
     *
     * @param fileName String путь к файлу(имя файла)
     * @param charSet String кодировка
     * @param regEx  String регулярное выражение
     * @return   List<T> лист объектов
     */
      List<T> parse(String fileName, String charSet, String regEx);

}
