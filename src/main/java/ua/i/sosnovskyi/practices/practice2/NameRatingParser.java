package ua.i.sosnovskyi.practices.practice2;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.ArrayList;

/**
 * реализация парсера для class NameRating
 * Created by A Sosnovskyi on 13.02.2017.
 */
public class NameRatingParser implements Parser<NameRating> {
    //      "(?<rankBefore><tr align=\"right\"><td>(?<rank>\\d+)<\\/td>)<td>(?<male>\\w+)<\\/td><td>(?<female>\\w+)<\\/td><\\/tr>";
    @Override
    /**
     *
     * @param fileName String путь к файлу(имя файла)
     * @param charSet String кодировка
     * @param regEx  String регулярное выражение
     * @return   List<T> лист объектов
     */
    public List<NameRating> parse(String fileName, String charSet, String regEx) {
        String inputData = Parser.readFromFile(fileName, charSet);
        Pattern pattern = Pattern.compile(regEx);
        Matcher matcher = pattern.matcher(inputData);
        List<NameRating> outputlist = new ArrayList<>();
        while (matcher.find()) {

            int rank=Integer.valueOf(matcher.group("rank"));
            String male=matcher.group("male");
            String female=matcher.group("female");
            NameRating obj=new NameRating(rank, male, female);
            outputlist.add(obj);
        }

        return outputlist;
    }
}
