package ua.i.sosnovskyi.practices.practice2;

import java.util.List;

/**
 * тестирование парсера
 * Created by A Sosnovskyi on 13.02.2017.
 */
public class TestParser {
    public static void main(String[] args) {
        String regEx=  "(?<rankBefore><tr align=\"right\"><td>(?<rank>\\d+)<\\/td>)<td>(?<male>\\w+)<\\/td><td>(?<female>\\w+)<\\/td><\\/tr>";
    NameRatingParser nameRatingParser=new NameRatingParser();
        List<NameRating> list= nameRatingParser.parse("baby2008.html", "utf-8", regEx);

        for(NameRating nameRating: list){
            System.out.println(nameRating);
        }
    }
}
