package ua.i.sosnovskyi.practices.practice4;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by A Sosnovskyi on 14.02.2017.
 * Задан текст на английском языке. Выделить все различные слова.
 * Для каждого слова подсчитать частоту его встречаемости.
 * Слова, отличающиеся регистром букв, считать различными.
 * Использовать класс HashMap. Текст считать из файла.
 * <p>
 * Перегрузить метод с возвратом отсортированного Map,
 * по чем сортировать и в каком направлении указывать через enum константы.
 */
public class WordsCounter implements Counter {
    //--------------------------------------не реализовано-----------------------------------------
    @Override
    public Map<String, Integer> count(Set<String> strings, String source) {
        Map<String, Integer> map = new HashMap<>();
        for (String str : strings) {
            Pattern pattern = Pattern.compile(str);
            Matcher matcher = pattern.matcher(source);
            int counter = 0;
            while (matcher.find()) {
                counter++;
            }
            map.put(str, counter);
        }

        return map;
    }

    @Override
    public Map<String, Integer> count(Set<String> strings, String source, Enum compareType) {
        if(compareType.name()=="LEXICOGRAPHICALLY"){
            Map<String, Integer> map = new TreeMap<>(new Comparator<String>() {
                @Override
                public int compare(String o1, String o2) {
                    return o1.compareTo(o2);
                }
            });
            Map<String, Integer> tmp=count(strings, source);
            map.putAll(tmp);
            return map;
        }else{
            Map<String, Integer> map = new TreeMap<>(new Comparator<String>() {
                @Override
                public int compare(String o1, String o2) {
                    int comp= o1.length()-o2.length();
                    if(comp==0){
                        comp=o1.compareTo(o2);
                    }
                    return comp;
                }
            });
            Map<String, Integer> tmp=count(strings, source);
            map.putAll(tmp);
            return map;
        }
    }
//---------------------------------------------------------------------------------------

    /**
     * @param sourse
     * @param charSet
     * @return
     */
    @Override
    public String read(String sourse, String charSet) {
        if (sourse == null || sourse.length() == 0) {
            throw new IllegalArgumentException("Wrong argument " + sourse);
        }
        StringBuilder sb = new StringBuilder();
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(sourse), charSet))) {
            while (true) {
                String tmp = bufferedReader.readLine();
                if (tmp == null) {
                    break;
                }
                sb = sb.append(tmp);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    /**
     * @param source
     * @param regEx
     * @return
     */
    @Override
    public Set<String> parse(String source, String regEx) {
        // String str= read(sourse, charSet);

        Pattern pattern = Pattern.compile(regEx);
        Matcher matcher = pattern.matcher(source);
        Set<String> strSet = new HashSet<>();
        while (matcher.find()) {
            strSet.add(matcher.group());
        }
        return strSet;
    }

    enum CompareType {
        LEXICOGRAPHICALLY, LENGTH
    }
}
