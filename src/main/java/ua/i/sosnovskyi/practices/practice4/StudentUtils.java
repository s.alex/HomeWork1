package ua.i.sosnovskyi.practices.practice4;

import java.util.*;

/**
 * Created by Admin on 14.02.2017.
 */
public class StudentUtils {
    /**
     *
     * @param students
     * @return
     */
    static Map<String, Student> createMapFromList(List<Student> students){
        Map<String, Student> map=new HashMap<>();
        for(Student student: students){
            map.put(student.getFirstName()+student.getLastName(), student);
        }
        return map;
    }

    /**
     *
     * @param students
     * @param course
     */
    static void printStudents(List<Student> students, int course){
        Iterator<Student> iterator=students.iterator();
        while(iterator.hasNext()){

           Student stud= iterator.next();
            if(stud.getCourse()==course){
                System.out.println(stud);
            }
        }
    }

    /**
     *
     * @param students
     * @return
     */
    static List<Student> sortStudent(List<Student> students){
        Set<Student> set=new TreeSet<>(students);

        List<Student> list=new ArrayList<>(set);
        return list;
    }
}
