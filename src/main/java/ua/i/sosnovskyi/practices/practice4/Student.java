package ua.i.sosnovskyi.practices.practice4;

/**
 * Created by Admin on 14.02.2017.
 */
public class Student implements Comparable {
    private String firstName;
    private String lastName;
    private  int course;

    public Student(String firstName, String lastName,int course ) {
        this.firstName = firstName;
        this.course = course;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public int getCourse() {
        return course;
    }

    public void setCourse(int course) {
        this.course = course;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "Student{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", course=" + course +
                '}';
    }

    @Override
    public int compareTo(Object o) {
        if(o==null){
            throw new IllegalArgumentException("o=null");
        }
        if(o.getClass()!=this.getClass()){
            throw new IllegalArgumentException("o!=this.class");
        }
        Student that=(Student)o;
        return this.getFirstName().compareTo(that.getFirstName());
    }
}
