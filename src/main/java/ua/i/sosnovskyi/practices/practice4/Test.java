package ua.i.sosnovskyi.practices.practice4;

import ua.i.pl.sosnovskyi.homework2.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 14.02.2017.
 */
public class Test {
    public static void main(String[] args) {
        List<Student> list = new ArrayList<>();
        list.add(new Student("Oleg", "Petrov", 1));
        list.add(new Student("Pavel", "Ivanov", 2));
        list.add(new Student("Dmitriy", "Voronov", 2));
        list.add(new Student("Aleksey", "Babich", 1));
        list.add(new Student("Nikolay", "Bolotov", 4));
        list.add(new Student("Oleg", "Shostak", 4));
        list.add(new Student("Evgeniy", "Danin", 3));
        list.add(new Student("Anna", "Ivanova", 3));
        list.add(new Student("Elena", "Petrova", 3));
        list.add(new Student("Inna", "Sidorova", 1));

        for (Student stud : list) {
            System.out.println(stud);
        }
        System.out.println("========================================================================");
        StudentUtils.createMapFromList(list);
        StudentUtils.printStudents(list, 2);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        List<Student> list2 = StudentUtils.sortStudent(list);

        for(Student stud: list2){
            System.out.println(stud);
        }

    }
}
