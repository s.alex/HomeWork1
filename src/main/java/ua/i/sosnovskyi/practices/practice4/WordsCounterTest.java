package ua.i.sosnovskyi.practices.practice4;

import java.util.Map;
import java.util.Set;

/**
 * Created by A Sosnovskyi on 03.03.2017.
 */
public class WordsCounterTest {
    public static void main(String[] args) {
        WordsCounter ws=new WordsCounter();
       String test= ws.read("EnglishText.txt", "UTF-8");
       // System.out.println(test);
       // System.out.println();
        Set set=ws.parse(test, "\\w+");
      //  System.out.println(set);
      //  Map<String , Integer> map=ws.count(set, test);
        Map<String , Integer> map=ws.count(set, test, WordsCounter.CompareType.LENGTH);
        for(Map.Entry<String , Integer> entry: map.entrySet()){
            System.out.println(entry.getKey().concat(" ").concat(String.valueOf(entry.getValue())));
        }
    }
}
