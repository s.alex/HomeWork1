package ua.i.sosnovskyi.practices.practice4;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * Created by A Sosnovskyi on 14.02.2017.
 */
public interface Counter {
    Map<String, Integer> count(Set<String> strings, String source);
    Map<String, Integer> count(Set<String> strings, String source, Enum compareType);
    String read(String sourse, String charSet);
    Set<String> parse(String sourse,  String regEx);
}
