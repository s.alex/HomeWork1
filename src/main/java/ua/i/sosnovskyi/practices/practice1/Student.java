package ua.i.sosnovskyi.practices.practice1;


import java.util.HashSet;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Set;


/**
 * Created by Sosnovskyi on 24.01.2017.
 */
public class Student {
    private final String name;
    private final String lastName;
    private Group group;
    public Set<Exam> examsPassed =new HashSet();

    /**
     * constructor
     * @param name
     * @param lastName
     */
    public Student(String name, String lastName) {
        this.name = name;
        this.lastName = lastName;

    }

    /**
     * constructor
     * @param name
     * @param lastName
     * @param course
     * @param facultet
     */
    public Student( String name, String lastName, int course, String facultet) {
        this.group =new  Group(course, facultet);
        this.name = name;
        this.lastName = lastName;
    }

    /**
     *  set Group
     * @param  course  integer course
     * @param facultet  string facultet
     * @return
     */
    public boolean setGroup(int course, String facultet){
        this.group =new  Group(course, facultet);
        boolean result=false;
        return result;
    }

    /**
     * add Exams Surrender
     * @param nameOfExam   name Of Exam
     * @param mark
     * @param year
     * @param semestr
     */
    public boolean addExamsPassed(String nameOfExam, int mark, int year, int semestr) {
       return this.examsPassed.add(new Exam(nameOfExam, mark, year, semestr));
    }

    /**
     * print Exams
     */

    public void printExams(){
       Set examSet= this.examsPassed;

        Iterator iterator=examSet.iterator();
        while (iterator.hasNext()){
          Exam exam=(Exam)iterator.next();
            System.out.println(exam.nameOfSubject+" date Of Surrender "+exam.dateOfSurrender.semestr+" semestr "
                    +exam.dateOfSurrender.year +" year "+"mark "+exam.mark);
        }
    }
    /**
     *  max Mark Of Exams
     * @return max Mark Of Exams
     */
 public int maxMarkOfExams(){
     int result=0;
     Iterator iterator= examsPassed.iterator();
     while (iterator.hasNext()){
       Object tmp=iterator.next();
         Exam exam=(Exam)tmp;
         if(result<exam.mark){
             result=exam.mark;
         }

     }
     return result;
 }

    //------------------------------------------------------------------------------------------------------
    /**
     * delete Exam Surrender Mark      не работает
     * @return
     */

    public boolean delExamPassedMark(String exam){
        boolean result=false;
        result= isExamPassed(exam);
       // System.out.println(result);

        if(result){
           // Exam tmp=new Exam(name, 0, 0, 0);
           // this.examsPassed.remove(exam);// не удаляет
           Iterator iterator=this.examsPassed.iterator() ;
            if(iterator.hasNext()){
                Exam tmp=(Exam)iterator.next();
                if(tmp.nameOfSubject==exam){
                   // System.out.println(tmp);
                    this.examsPassed.remove(tmp);
                }
            }
        }else{
            throw new NoSuchElementException("Exam "+exam+" not found");
        }
        return result;
    }
//-------------------------------------------------------------------------------------------
    /**
     * is Exam Surrendered
     * @return
     */
    public boolean isExamPassed(String name){
        Exam tmp=new Exam(name, 0, 0, 0);
        return  examsPassed.contains(tmp);
    }


    /**
     * number Of Exams Whith markTested
     * @param markTested
     * @return
     */
    public int numberOfExamsWhithSuchMark(int markTested){
        int result=0;
        Iterator iterator= examsPassed.iterator();

        while (iterator.hasNext()){
            Object tmp=iterator.next();
            Exam exam=(Exam)tmp;
            if(markTested==exam.mark){
                result++;
            }

        }
        return  result;
    }


    /**
     * medium Ball Fore Semestr
     * @param semestr
     * @return
     */
    public float mediumBallForeSemestr(int semestr){
        float result=0;
        int amountOfExams=this.examsPassed.size();
        int marksSum=0;
        Iterator iterator= examsPassed.iterator();
        while (iterator.hasNext()){
            Object tmp=iterator.next();
            Exam exam=(Exam)tmp;
            if(exam.dateOfSurrender.semestr==semestr){
                marksSum=marksSum+exam.mark;
                }

        }
        result=(float)marksSum/amountOfExams;
        return result;

    }

    @Override
    public String toString() {
        return "Student:" +
                "name=" + name +
                " lastName=" + lastName + " facultet="+ group.facultet+" course="+group.course;
    }


    /**
     * Группа
     */
    private class Group{
     private int course;
        private String facultet;

        public Group(int course, String facultet) {
            this.course = course;
            this.facultet = facultet;
        }
    }

    /**
     * Экзамен
     */
    private class Exam{
       private String nameOfSubject ;
        private  int mark;
        private DateSurrender dateOfSurrender;

        public Exam(String nameOfSubject, int mark, int year, int semestr) {
            this.nameOfSubject = nameOfSubject;
            this.mark = mark;
            this.dateOfSurrender= new DateSurrender(year, semestr);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Exam exam = (Exam) o;
            return /*mark == exam.mark &&*/
                    this.nameOfSubject.equals( exam.nameOfSubject);
        }

        @Override
        public int hashCode() {
            int result=0;
            char[] data=nameOfSubject.toCharArray();
            for(int index=0; index<data.length; index++){
                Character tmp=data[index];

                result =31*result+tmp.hashCode();
            }
            return result;
        }
    }

    /**
     * DateSurrender дата сдачи  семестр
     */
    private class DateSurrender{
      private int year;
        private int semestr;

        /**
         *
         * @param year
         * @param semestr
         */
        public DateSurrender(int year, int semestr) {
            this.year = year;
            this.semestr = semestr;
        }
    }
}
