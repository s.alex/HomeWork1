package ua.i.sosnovskyi.practices.practice1;

/**
 * Created by Admin on 24.01.2017.
 */
public class ArrayProd {
    private int[] array;
    public ArrayProd(int[] array) {
        this.array=array;
    }
    public int prod(){
        if(this.array.length==0){
            throw new IllegalArgumentException("array.length is 0");
        }
        int result=1;
        for(int index=0; index<array.length; index++){
            result =result*array[index];
        }
        return result;
    }
    public static int prod(int[] array){
        if(array.length==0){
            throw new IllegalArgumentException("array.length is 0");
        }
        if(array==null){
            throw new NullPointerException("array not found");
        }
        int result=1;
        for(int index=0; index<array.length; index++){
            result =result*array[index];
        }
        return result;
    }
}
