package ua.i.sosnovskyi.practices.practice1;

/**
 * Created by Admin on 24.01.2017.
 */
public class ArraySum {
    private int[] array;
    public ArraySum(int[] array) {
        this.array=array;
    }
    public int sum(){
        if(this.array.length==0){
            throw new IllegalArgumentException("array.length is 0");
        }
        int result=0;

        for(int index=0; index<array.length; index++){
            result =result+array[index];
        }
        return result;
    }
    static int sum(int[] arr){
        if(arr==null){
            throw new NullPointerException("array not found");
        }
        if(arr.length==0){
            throw new IllegalArgumentException("array.length is 0");
        }
        int result=0;
        for(int index=0; index<arr.length; index++){
            result =result+arr[index];
        }
        return result;
    }
}
