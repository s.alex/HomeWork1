package ua.i.sosnovskyi.practices.practice1;

/**
 * Created by Admin on 28.01.2017.
 */
public class PracticeI {
    public static void main(String[] args) {
        Student stud1 = new Student("Vasiliy", "Ivanov");
        Student stud2 = new Student("Vitaliy", "Petrov", 1, "Системные науки и кибернетика");
        stud1.setGroup(2, " Информатика и вычислительная техника");
        System.out.println(stud1);
        System.out.println("stud1.examsPassed.isEmpty Before adding= " + stud1.examsPassed.isEmpty());
        System.out.println("stud1.examsPassed.size Before adding= " + stud1.examsPassed.size());
        System.out.println();
        System.out.println("stud1.addExamsPassed= " + stud1.addExamsPassed("Информационно-коммуникационные технологии", 3, 2016, 1));
        System.out.println("stud1.examsPassed.size After adding= " + stud1.examsPassed.size());
        System.out.println();
        System.out.println("stud1.examsPassed.isEmpty After adding=" + stud1.examsPassed.isEmpty());


        System.out.println("stud1.addExamsPassed= " + stud1.addExamsPassed("Информационные технологии проектирования", 4, 2016, 1));
        System.out.println("stud1.addExamsPassed= " + stud1.addExamsPassed("Информатика", 5, 2016, 1));
        System.out.println("stud1.addExamsPassed= " + stud1.addExamsPassed("Прикладная математика", 3, 2016, 1));
        System.out.println("stud1.examsPassed.size After adding= " + stud1.examsPassed.size());
        System.out.println();
        stud1.printExams();

        System.out.println();
        System.out.println("stud1.delExamPassed(\"Информационно-коммуникационные технологии\")" + stud1.delExamPassedMark("Информационно-коммуникационные технологии"));
        System.out.println("stud1.examsPassed.size After dellete= " + stud1.examsPassed.size());
        // System.out.println(stud1.examsPassed.isEmpty());
        stud1.printExams();
        System.out.println();
        System.out.println("stud1.isExamPassed(\"Прикладная математика\")" + stud1.isExamPassed("Прикладная математика"));
        System.out.println();

        System.out.println("stud1.mediumBallForeSemestr " + stud1.mediumBallForeSemestr(1));
        System.out.println("stud1.maxMarkOfExams= " + stud1.maxMarkOfExams());
        System.out.println("stud1.numberOfExamsWhithSuchMark(5)=" + stud1.numberOfExamsWhithSuchMark(5));
        System.out.println(stud2);
    }
}
