package ua.i.sosnovskyi.practices.practice6;

import java.io.File;

/**
 * Created by A Sosnovskyi on 09.03.2017.
 */
public class FileSeekerApp extends Thread {
    private String beginnerPath;
    private String fileToFind;

    public FileSeekerApp(String beginnerPath, String fileToFind) {
        this.beginnerPath = beginnerPath;
        this.fileToFind = fileToFind;
    }

    @Override
    public void run() {
        File[] files = FileSeeker.find(beginnerPath);

        if (files!=null) {
            for (int i = 1; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    Thread tmp = new FileSeekerApp(files[i].getAbsolutePath(), fileToFind);

                    tmp.start();
                }
            }
            FileSeeker.findInDirectory(files, fileToFind);
        }


    }
}
