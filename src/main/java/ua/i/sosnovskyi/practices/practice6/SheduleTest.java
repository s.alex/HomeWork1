package ua.i.sosnovskyi.practices.practice6;

import ua.i.sosnovskyi.practices.practice5.FileUtils;

import java.util.Map;

/**
 * Created by A Sosnovskyi on 08.03.2017.
 */
public class SheduleTest {


    public static void main(String[] args) {

        String regEx="(?<interval>[0-9]+) (?<message>[^0-9]+)";
        String str=FileUtils.readStringFromFile("SheduleIntervals.txt", "UTF-8");
        Map<Long, String> shedule=MyShedule.getData(regEx, str);
        MyShedule sheduleMessage=new MyShedule(shedule);
        sheduleMessage.printMessage(sheduleMessage.sheduleMessage);
    }
}
