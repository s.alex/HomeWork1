package ua.i.sosnovskyi.practices.practice6;

/**
 * Created by A Sosnovskyi on 07.03.2017.
 */
public class NamePrinterRunnable implements Runnable {
    private int iterationInterval;
    private int sleepInterval;

    public NamePrinterRunnable(int iterationInterval, int sleepInterval) {
        this.iterationInterval = iterationInterval;
        this.sleepInterval = sleepInterval;
    }

    @Override
    public void run() {
        int count=0;
        while(count<iterationInterval){
            System.out.println(Thread.currentThread().getName());

            try {
                Thread.sleep(sleepInterval);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            count+=sleepInterval;
        }
    }
}
