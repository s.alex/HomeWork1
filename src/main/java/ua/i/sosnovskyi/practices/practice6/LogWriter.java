package ua.i.sosnovskyi.practices.practice6;

import java.io.*;

/**
 * Created by A Sosnovskyi on 09.03.2017.
 */
public class LogWriter {
    public synchronized static void write(String strToWrite) {
        System.out.println(strToWrite);
    }

    public synchronized static void writeToFile(String strToWrite) {

        try (BufferedOutputStream buf = new BufferedOutputStream(new FileOutputStream(new File("FileSeekerApp.log"), true))) {
        buf.write(strToWrite.getBytes(), 0, strToWrite.getBytes().length);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
