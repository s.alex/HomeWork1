package ua.i.sosnovskyi.practices.practice6;

import ua.i.sosnovskyi.practices.practice5.FileUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;

/**
 * Created by A Sosnovsky on 07.03.2017.
 */
public class MyShedule {
    Map<Long, String> sheduleMessage;

    public MyShedule(Map<Long, String> sheduleMessage) {
        this.sheduleMessage = sheduleMessage;
    }

    public static Map<Long, String> getData(String regEx, String str) {
        Map<Long, String> data = new HashMap<>();
        Matcher matcher = FileUtils.getMatcher(regEx, str);
        while (matcher.find()) {
            Long interval = Long.valueOf(matcher.group("interval"));
            String message = matcher.group("message");
            data.put(interval, message);
        }
        return data;
    }

    public void printMessage(Map<Long, String> map) {
        for (Map.Entry<Long, String> entry : map.entrySet()) {
            new Thread() {
                @Override
                public void run() {
                    while (true) {
                        System.out.println(entry.getValue().concat(" sleep for ").concat(String.valueOf(entry.getKey())).concat(" ms"));
                        try {
                            sleep(entry.getKey());
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                }
            }.start();
        }
    }
}
