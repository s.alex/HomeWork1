package ua.i.sosnovskyi.practices.practice6;

import java.io.File;

/**
 * Created by A Sosnovskyi on 09.03.2017.
 */
public class FileSeeker {

    public static File[] find(String path){
        File file=new File(path);
        if(file.isDirectory()){

            return    file.listFiles();
        }
        else {
            return new File[0];
        }
    }

    public static void findInDirectory(File[] filesEntry, String fileNameToFind){
        String absolutePath;
        if(filesEntry.length==0){
            return;
        }
        StringBuilder sb=new StringBuilder();
        for(int i=0; i<filesEntry.length; i++){
            if(filesEntry[i].isDirectory()){

            }else{

                absolutePath=filesEntry[i].getAbsolutePath();
                if(detect(absolutePath, fileNameToFind)){
                    sb.append(absolutePath.concat("\n"));
                    synchronized (LogWriter.class){
                        LogWriter.write(absolutePath);
                    }

                }
            }
        }
//        if(sb!=null){
//            synchronized (LogWriter.class) {
//
//                    LogWriter.write(Thread.currentThread().getId() + ": " +sb.toString());
//
//            }
//        }

    }

    public static boolean detect(String fileName, String fileNameToFind){
       boolean result=false;
        if(fileName.contains(fileNameToFind)){
            result=true;
        }

        return result;
    }
}
