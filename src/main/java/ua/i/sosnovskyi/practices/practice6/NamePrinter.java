package ua.i.sosnovskyi.practices.practice6;

/**
 * Created by A Sosnovskyi on 07.03.2017.
 */
public class NamePrinter extends Thread {
    private int iterationInterval;
    private int sleepInterval;

    public NamePrinter(int iterationInterval, int sleepInterval) {
        this.iterationInterval = iterationInterval;
        this.sleepInterval = sleepInterval;
        this.start();
    }

    @Override
    public void run() {
        int count=0;
      while(count<iterationInterval){
          System.out.println(this.getName());

        try {
            sleep(sleepInterval);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
          count+=sleepInterval;
      }
    }
}
