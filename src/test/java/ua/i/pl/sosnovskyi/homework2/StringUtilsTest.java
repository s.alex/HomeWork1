package ua.i.pl.sosnovskyi.homework2;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

/**
 * Created by Admin on 31.01.2017.
 */
public class StringUtilsTest {

    StringUtils strTest1=new StringUtils("Hello World!");
    StringUtils strTest2=new StringUtils("А роза упала на лапу Азора");
    String strTest3="Каждый веб-разработчик знает, что такое текст-«рыба». ";
    StringUtils strTest4=new StringUtils("The main purpose of my invention is to create a model of the well\n" +
            "known instrument 76.5 in most countries, violin. It looks very alike, yet creating it is far cheaper than a real violin. It can be used as a great piece of creativedecor. It’s very simple to use. Just a decorative piece with no more than one\n" +
            "type of mechanisms. Bright purple and white pieces of paper and cardboard, the 3D shape, and pieces of glittery tape on nails for tuning the bright purple strings make the violin brighter, but still a lot alike with my example, my own\n" +
            "violin. Even the size is real, since violins come in many sizes. The rubber bands are the strings. You cannot bow the notes, though. As\n" +
            "a decorative piece, it uses bright purple rubber bands for strings that you can\n" +
            "even technically tune with the “pegs” and “fine tuners” made out of nails.\n" +
            "\n" +
            "My violin, as well as a piece of decor, can be used as a teaching model\n" +
            "for the parts of a violin, since it has most of them. The only things it doesn’t\n" +
            "have are a chinrest, a bridge, and the wonderful sound of the real, yet far\n" +
            "more expensive violin.");

    StringUtils strTest5=new StringUtils("00.02.2017"); // можно менять значения
    StringUtils strTest6=new StringUtils("22.01.1979"); // можно менять значения

    StringUtils strTest7 = new StringUtils("Danny Doo,  Flat no 502,  Big Apartment,  Wide Road, Near Huge Hilestone,   "
            + "Hugo-city 56010,  Ph:  +8(765)-432-10,  Email:  danny@myworld.com.   "
            + "Haggi Hyer,  Post bag no 52,  Big bank post office,   "
            + "Big bank city 56000,  ph:  +3(765)-012-34,  Email: maggi07@myuniverse.com.");
    StringUtils strTest8 = new StringUtils("aaabbbccccc");
    StringUtils str2 = new StringUtils("String");
    StringUtils str3 = new StringUtils("Stringnfos");

    @Test
    public void reverseString() throws Exception {
        String real=strTest1.reverseString();
        String expacted="!dlroW olleH";
        Assert.assertEquals("Error of compare", expacted, real);

    }

    @Test
    public void polindromString() throws Exception {
        boolean real=strTest2.polindromString();
        boolean expacted=true;
        Assert.assertEquals("Error of compare", expacted, real);
    }

    @Test
    public void reverseWordsInString() throws Exception {
        String real=StringUtils.reverseWordsInString(strTest3);
        String expacted="текст-«рыба» веб-разработчик знает, что такое Каждый. ";
        Assert.assertEquals("Error of compare", expacted, real);
    }
//------------------------------------------------------------
    @Test
    public void modifyString() throws Exception {
        String real= str2.modifyString();
        String real1= str3.modifyString();
        String real2=strTest2.modifyString();
        String expacted="Stringoooooo";
        String expacted1="Stringnfosoo";
        String expacted2="А роза";
        Assert.assertEquals("Что то пошло не так!!!", expacted, real);
        Assert.assertEquals("Что то пошло не так!!!", expacted1, real1);
        Assert.assertEquals("Что то пошло не так!!!", expacted2, real2);
    }
//-------------------------------------------------------------
    @Test
    public void reverseWordsInSentences() throws Exception {
        String real=strTest4.reverseWordsInSentences();
        String expacted="violin main purpose of my invention is to create a model of the well\n" +
                "known instrument 76.5 in most countries, The. violin looks very alike, yet creating it is far cheaper than a real It. creativedecor can be used as a great piece of It. use very simple to It’s. mechanisms a decorative piece with no more than one\n" +
                "type of Just. violin purple and white pieces of paper and cardboard, the 3D shape, and pieces of glittery tape on nails for tuning the bright purple strings make the violin brighter, but still a lot alike with my example, my own\n" +
                "Bright. sizes the size is real, since violins come in many Even. strings rubber bands are the The. though cannot bow the notes, You. nails decorative piece, it uses bright purple rubber bands for strings that you can\n" +
                "even technically tune with the “pegs” and “fine tuners” made out of As\n" +
                "a.them violin, as well as a piece of decor, can be used as a teaching model\n" +
                "for the parts of a violin, since it has most of \n" +
                "\n" +
                "My. violin only things it doesn’t\n" +
                "have are a chinrest, a bridge, and the wonderful sound of the real, yet far\n" +
                "more expensive The.";
        Assert.assertEquals("Error of compare", expacted, real);
    }

    @Test
    public void isDate() throws Exception {
        boolean real=strTest5.isDate()&&strTest6.isDate();
        boolean expacted=true;
        Assert.assertEquals("Error of compare", expacted, real);

    }

    @Test
    public void isPostAdress() throws Exception {
        boolean real=strTest7.isPostAdres();
        boolean expacted=true;
        Assert.assertEquals("Error of compare", expacted, real);
    }

    @Test
    public void phoneNumbers() throws Exception {
        boolean real=Arrays.toString(strTest7.phoneNumbers()).equals(Arrays.toString(new String[]{"+8(765)-432-10", "+3(765)-012-34"}));
        boolean expacted=true;
        Assert.assertEquals("Error of compare", expacted, real);
    }

    @Test
    public void isChars() throws Exception {
        boolean real=strTest8.isChars('a', 'b', 'c')&&!strTest8.isChars('d', 'e', 'f');
        boolean expacted=true;
        Assert.assertEquals("Error of compare", expacted, real);
    }

}