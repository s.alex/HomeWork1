package ua.i.sosnovskyi.practices.practice1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Sosnovskyi on 29.01.2017.
 */
public class StudentTest {
    Student stud1 = new Student("Vasiliy", "Ivanov");



    @Before
    public void addExamPassed() {
        stud1.addExamsPassed("Прикладная математика", 3, 2016, 1);
        stud1.addExamsPassed("Информатика", 5, 2016, 1);
        stud1.addExamsPassed("Информационно-коммуникационные технологии", 5, 2016, 1);
        stud1.addExamsPassed("Информационные технологии проектирования", 4, 2016, 1);

    }

    @Test
    public void addExamsPassed() throws Exception {
        boolean real = stud1.examsPassed.isEmpty();
        boolean expacted = false;
        Assert.assertEquals("Error adding Exams Passed", expacted, real);
    }


    @Test
    public void maxMarkOfExams() throws Exception {
        int real = stud1.maxMarkOfExams();
        int expacted = 5;
        Assert.assertEquals("Error max value", expacted, real, 0.001);
    }

    @Test
    public void numberOfExamsWhithSuchMark() throws Exception {
        int real = stud1.numberOfExamsWhithSuchMark(5);
        int expacted = 2;
        Assert.assertEquals("Error number Of Exams Whith Such Mark", expacted, real, 0.001);

    }

    @Test
    public void mediumBallForeSemestr() throws Exception {
        float real = stud1.mediumBallForeSemestr(1);
        float expacted = (float) (3 + 5+5 + 4) / 4;
        Assert.assertEquals("Error medium Ball Fore Semestr", expacted, real, 0.001);
    }

    @Test
    public void delExamPassedMark() throws Exception {
        int sizeBeforeDel=stud1.examsPassed.size();
        System.out.println(sizeBeforeDel);
        stud1.delExamPassedMark("Информационно-коммуникационные технологии");
        int sizeAfterDel=stud1.examsPassed.size();
        System.out.println(sizeAfterDel);
        boolean real = ((sizeBeforeDel-sizeAfterDel)>0);
        boolean expacted = true;
        Assert.assertEquals("Error removing Exams Passed", expacted, real);
    }




}