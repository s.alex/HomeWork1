package ua.i.sosnovskyi.practices.practice1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Admin on 24.01.2017.
 */
public class ArrayProdTest {
    int[] array=null;
    @Before
    public void createArray(){
       array=new int[]{3, 5, 2, 1};
    }
    @Test
    public void prod() throws Exception {
        int expected=3*5*2*1;
        int real= ArrayProd.prod(array);
        Assert.assertEquals("Error prod", expected, real, 0.001);
    }

    @Test
    public void prod1() throws Exception {
        int expected=3*5*2*1;
        int real=new ArrayProd(array).prod();
        Assert.assertEquals("Error prod", expected, real, 0.001);
    }

    @Test(expected = NullPointerException.class)
    public void sumExceptionCathed() throws Exception {
        ArrayProd.prod(null);
    }

    @Test
    public void sumException() throws Exception {
        ArrayProd.prod(null);
    }
}