package ua.i.sosnovskyi.practices.practice1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Admin on 24.01.2017.
 */
public class ArraySumTest {
    //int[] array=new int[]{3, 5, 2, 1};
    int[] array=null;
    @Before
    public void createArray(){
        array=new int[]{3, 5, 2, 1};

    }

    @Test
    public void sum() throws Exception {
        int real= ArraySum.sum(array);
        int expacted=11;
        Assert.assertEquals("Error sum", expacted, real, 0.001);
    }

    @Test
    public void sum1() throws Exception {
        int real=new ArraySum(array).sum();
        int expacted=11;
        Assert.assertEquals("Error sum", expacted, real, 0.001);
    }

    @Test(expected = NullPointerException.class)
    public void sumExceptionCathed() throws Exception {
        ArraySum.sum(null);
    }

    @Test
    public void sumException() throws Exception {
        ArraySum.sum(null);
    }
}