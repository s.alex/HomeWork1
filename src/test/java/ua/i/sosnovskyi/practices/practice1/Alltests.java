package ua.i.sosnovskyi.practices.practice1;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by Admin on 24.01.2017.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({ArrayProdTest.class, ArraySumTest.class})
public class Alltests {
}
